<?
$f=$_REQUEST;

/*****************************
 

 ?img=filename&op=resize&max=N
 ?img=filename&op=resize,fill&max=N&cx=N&cy=N
 ?img=filename&op=crop&xmax=N&ymax=N[&fillcolour=N,N,N] | ?img=filename&op=crop&max=N[&fillcolour=N,N,N]
 
 
 ****************************/

function calcsize(&$x, &$y, $max) {
	
	if ($x>$y) {
		$y = $max*$y/$x;
		$x = $max;
	} elseif ($y>$x) {
		$x = $x*$max/$y;
		$y = $max;
	} else {
		$x = $y = $max;
	}
	
}

function calcsize2(&$x, &$y, $max1, $max2, &$swx, &$swy) {

	if ( ( $x/$y ) < ($max1/$max2) ) {
		$y=$y*$max1/$x;
		$x=$max1;
		$swy = round( ($y-$max2)/2 );
		$swx=0;
	} elseif (( $x/$y ) > ($max1/$max2)) {
		$x = $x*$max2/$y;
		$y = $max2;
		$swy=0;
		$swx=round( ($x-$max1)/2 );
	} else {
		$x=$y=($max1>$max2?$max1:$max2);
	}

}

function fext($fn) {
	return strtolower(substr($fn, strrpos($fn,'.')+1));
}


$img = $f['img'];
$ext = fext($img);


if ( !file_exists($img) ) {
	$img='gfx/nofoto.gif';
	$ext='gif';
}

	// $thumb_name='thumbs/'.md5(@filesize($img).$img.$_SERVER['QUERY_STRING']).'.'.$ext;
	// if ( file_exists($thumb_name) ) {
	// 	if ($ext == 'jpg' || $ext == 'jpeg') {
	// 		readfile($thumb_name);
	// 	} elseif ($ext == 'png') {
	// 		readfile($thumb_name);
	// 	} elseif ($ext == 'gif') {
	// 		header('Content-type: image/gif');  
	// 	}
	// 	readfile($thumb_name);
	// 	exit();
	// }


$defaultcolor=array(255,255,255);

$ts = array(
	1=>array(
			'max'=>100,
			 'op'=>array('resize'),
			),
);


if (intval($f['t'])!=0) {

	$c=$op[$f['t']];
	
} else {
	

	$c['op'] = split(',',$f['op']);

	$c['max'] = intval($f['max']);
	$c['xmax'] = intval($f['xmax']);
	$c['ymax'] = intval($f['ymax']);
	$c['cx'] = intval($f['cx']);
	$c['cy'] = intval($f['cy']);
	$c['clevel']= intval($f['clevel']);

	$c['fillcolour'] = split(',',$f['fillcolour']);
	
}

if ( file_exists($img) ) {
	
	$s = getimagesize($img);
	$x=$s[0];
	$y=$s[1];
//	echo "{$x}:{$y}...";
	if ( in_array('resize', $c['op']) ) {
		calcsize($x, $y, $c['max']);
		if ( in_array('fill',$c['op']) ) {
			$mainwidth = $c['cx'];
			$mainheight = $c['cy'];
			
		} else {
			
			$mainwidth = $x;
			$mainheight = $y;
			
		}

		$swx = $swy = 0;
		
	}

	if ( in_array('crop',$c['op']) ) {

		if ($c['max']!=0) {
			$c['xmax']=$c['ymax']=$c['max'];
		} 
		
		
		calcsize2($x, $y, $c['xmax'], $c['ymax'], $swx, $swy);
		
		if ($c['max']!=0) {
			$mainwidth=$mainheight=$c['max'];
		} else {
			$mainwidth=$c['xmax'];
			$mainheight=$c['ymax'];
		}
		
		
	}
	
	if ($ext=='jpg') {
		$tmp = imagecreatefromjpeg($img);
	} elseif ($ext=='gif') {
		$tmp = imagecreatefromgif($img);
	} elseif ($ext=='png') {
		$tmp = imagecreatefrompng($img);
	}
	
	
	$out = imagecreatetruecolor($mainwidth, $mainheight);

	$c['fillcolour']=isset($f['fillcolour'])?split(',',$f['fillcolour']):$defaultcolor;
	$color = imagecolorallocate($out, $c['fillcolour'][0], $c['fillcolour'][1], $c['fillcolour'][2]);

	if ($ext == 'png' || $ext == 'gif' ) {
		imagealphablending($out, false);
	} else {
		if (in_array('fill',$c['op'])) imagefill($out, 1,1,$color);
	}
	
	$startx = round( ($mainwidth/2) - ($x/2) );
	$starty = round( ($mainheight/2) - ($y/2) );

	if ($startx<0) {
		$startx=0;
	}
	if ($starty<0) {
		$starty=0;
	}

/*	

 startx - docelowy x
 starty - 
 swx - zrodlowy x
 swy
 x - docelowy width
 y
 s0 - zrodlowy width
 s1
 
*/	
	
	if ($f['debug']=='on') echo "sx:{$startx} sy:{$starty} swx:{$swx} swy:{$swy} x:{$x} y:{$y} s0:{$s[0]} s1:{$s[1]}";
	imagecopyresampled($out, $tmp, $startx, $starty, $swx, $swy, round($x), round($y), $s[0], $s[1]);
	
	if ($ext == 'png' || $ext == 'gif' ) {
		imagesavealpha($out, true);
	}
	else {
//		if (in_array('fill',$c['op'])) imagefill($out, 1,1,$color);
	}
	
	imagedestroy($tmp);  
	
	if ($ext == 'jpg' || $ext == 'jpeg') {
		
		header('Content-type: image/jpeg');  
		imagejpeg($out,$thumb_name,$c['clevel']!=0?$c['clevel']:75);  
		
	} elseif ($ext == 'png') {
		header('Content-type: image/png');  
		imagepng($out, $thumb_name);  
		
	} elseif ($ext == 'gif') {
		
		header('Content-type: image/gif');  
		imagegif($out, $thumb_name);  
	}
	
	readfile($thumb_name);
	imagedestroy($out);  
	
	
	
	
} else {
	
	echo 'File not found';
	
}


?>