<?php
include "admin/lib/phpmailer/class.phpmailer.php";

function MAILER($mail_do, $mail_od, $title, $tresc, $zalacznik=NULL) {
    
		$mail = new PHPMailer();
    $mail->CharSet = "UTF-8";
    $mail->From = $mail_od;
    $mail->FromName = $mail_od;
    $mail->IsHTML(true);
	  $mail->Subject  =  $title;
	  $mail->Body = $tresc;
	  if ($zalacznik!=NULL)
	  {
	  $mail->AddAttachment($zalacznik); 
	  }
    $mail->AddAddress($mail_do);    
    $mail->Send();
    $mail->ClearAddresses();
  }

?>
