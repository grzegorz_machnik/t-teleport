<?php
  define ("PAGE_NAME_REGEXP","^[^']{3,200}$");
  define ("CITY_NAME_REGEXP","^(.){3,100}$");
  define ("LONG_NAME_REGEXP","^.+$");
  define ("EMAIL_REGEXP","^([0-9a-zA-Z\._-]+)@(([0-9a-zA-Z_-]+)\.){1,5}[a-zA-Z]{2,3}$");
  define ("AREA_NAME_REGEXP","^[a-z0-9_]{3,50}$");
	define ("LANGUAGE_PAGE_NAME_REGEXP","^[^']{3,100}$");
	define ("PAGE_URL_REGEXP","^/(([a-z0-9_\-]+/)*){0,99}$|^#$");
	define ("CONTENT_NAME_REGEXP","^[^']{3,50}$");
	define ("AREA_NAME_REGEXP","^[a-z0-9_]{3,50}$");
	define ("LANGUAGE_NAME_REGEXP","^[^']{3,50}$");
	define ("LANGUAGE_SYMBOL_REGEXP","^[a-z]{2,3}$");
	define ("CONTENT_REGEXP","^.+$");
	define ("MODULE_NAME_REGEXP","^[^']{3,50}$");
	define ("DESCRIPTION_REGEXP","^[^']*$");
	define ("KEYWORDS_REGEXP","^.+$");
	define ("FILE_NAME_REGEXP","^[^'\"\\/]+\.[a-zA-Z]{3}$");
	define ("FILE_TITLE_REGEXP","^[^']{3,255}$");
	define ("FILE_EXTENTION_REGEXP","(\.[a-zA-Z]{3,4})$");
	define ("DATE_REGEXP","^[0-9]{2,4}-[0-9]{2}-[0-9]{2}");
	define ("DIRECTION_NUMBER_REGEXP","^\+?[\(\)0-9\s]{2,}$");
	define ("SURFACE_REGEXP","^.+$");
	define ("IDENTIFIER_REGEXP","^[a-zA-Z0-9_-]{3,100}$");
	define ("CITY_NAME_REGEXP","^(.){3,100}$");
	define ("STREET_NAME_REGEXP","^(.){3,100}$");
	define ("HOUSE_NUMBER_REGEXP","^[0-9]{1,4}[a-zA-Z]?(/[0-9]{1,4}[a-zA-Z]?)?$");
	define ("DIRECTION_NUMBER_REGEXP","^(\([0-9\s]\))|([0-9\s])$");	
	define ("WWW_REGEXP","^(https?://)?(www\.)?([a-zA-Z0-9_]+\.){1,6}([a-zA-Z]+){2,4}/?$");
	define ("BANK_ACCOUNT_NUMBER_REGEXP","^(.){3,200}$");
	define ("NIP_REGEXP","^[0-9]{3}(\s|-)?[0-9]{2,3}(\s|-)?[0-9]{2,3}(\s|-)?[0-9]{2,3}$");
	define ("POSTCODE_REGEXP","^[0-9]{2}-[0-9]{3}$");
	define ("PHONE_NUMBER_REGEXP","^[0-9\s-]{5,15}$");
	define ("NAME_REGEXP","^[^']{3,50}$");
  define ("USER_NAME_REGEXP","^[a-zA-Z0-9_@\.-]{3,20}$");
  define ("USER_PASSWORD_REGEXP","^[a-zA-Z0-9_]{3,20}$");
 
	 
?>
