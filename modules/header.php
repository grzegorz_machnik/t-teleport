<?php
global $s, $sqlconn, $lng, $cid, $task, $id, $item, $id_section;

//error_reporting(E_ALL);

function clean($str, $replace=array(), $delimiter='-'){
    
     setlocale(LC_ALL, 'pl_PL.UTF8');
      if( !empty($replace) ) {
       $str = str_replace((array)$replace, ' ', $str);
      }
      
      $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
      $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
      $clean = strtolower(trim($clean, '-'));
      $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
      
      return $clean;
    
    }

$sql = "Select * from "._DB_PREFIX."_banners where jdb_active = 'y' order by jdb_orderkey";  
$banners = db_getsqltable($sqlconn, $sql);
$s->assign('banners',$banners);


$sql = "Select *, title_{$lng} as title from "._DB_PREFIX."_menu where  jdb_active = 'y' and id_parent = '0' order by jdb_orderkey";  
$mtop = db_getsqltable($sqlconn, $sql);

foreach($mtop as $k=>$v){

 $mtop[$k]['slug'] = clean($v['title']);
 
}
     //dumpvar($mtop);
$s->assign('mtop',$mtop);

$siteid = $_GET['parent']?$_GET['parent']:$_GET['site'];
$s->assign('siteid',$siteid);


$sql = "Select meta_title_{$lng} as meta_title, meta_description_{$lng} as meta_description, meta_keywords_{$lng} as meta_keywords from "._DB_PREFIX."_menu where id = '{$siteid}' ";
$meta = db_getsinglerow($sqlconn, $sql);

$s->assign('meta',$meta);



$sql = "Select * from "._DB_PREFIX."_menu where mleft = 'y' and jdb_active = 'y' and id_parent = '0'  order by jdb_orderkey";  
$mleft = db_getsqltable($sqlconn, $sql);
  
foreach($mleft as $k=>$v){
  
  $mleft[$k]['slug'] = clean($v['title']);
  
  $sql = "Select * from "._DB_PREFIX."_menu where mleft = 'y' and jdb_active = 'y' and id_parent = '{$v['id']}'  order by jdb_orderkey";  
  
  $mleft[$k]['sub'] = db_getsqltable($sqlconn, $sql);
  
  foreach($mleft[$k]['sub'] as $ke=>$va){

   $mleft[$k]['sub'][$ke]['slug'] = clean($va['title']);

  }
    
}
  
$s->assign('mleft',$mleft);   