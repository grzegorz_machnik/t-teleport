{literal}
 <script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			
      $(".fancybox").fancybox({
        helpers: {
            media: {}
        }
      });
      
  });
  
</script>      
{/literal}
<div class="boxy main">  	
        <div class="group">    
          
          <div class="grid">      
            
            {if $banners.0.link !=''}
            <a href="{$banners.0.link}" class="box">
            {elseif $banners.0.yt != ''}
            <a href="{$banners.0.yt}" class="box fancybox iframe">   
            {else}
            <a href="#" class="box">   
            {/if}   
              
              {if $banners.0.plik !=''}
               <div class="foto" style="background-image: url({$banners.0.plik});">
              </div> 
              {/if}            
              <span class="label {if $banners.0.plik == ''}bezfoto{/if}">{$banners.0.nazwa}
              </span> </a>  	
          </div>
              
          <div class="grid">      
            
            {if $banners.2.link !=''}
            <a href="{$banners.2.link}" class="box">
            {elseif $banners.2.yt != ''}
            <a href="{$banners.2.yt}" class="box fancybox iframe">   
            {else}
            <a href="#" class="box">   
            {/if}   
              
              {if $banners.2.plik !=''}
               <div class="foto" style="background-image: url({$banners.2.plik});">
              </div> 
              {/if}            
              <span class="label {if $banners.2.plik == ''}bezfoto{/if}">{$banners.2.nazwa}
              </span> </a>  	
          </div> 
             
        </div>    
        <div class="grid_big">      
          <div class="box box_black">        
            <video class="movie" loop="loop"  id="video" autoplay="autoplay" controls="controls">        
              <source src="images/movie_1.mp4" type="video/mp4">  		
                <source src="images/movie_1.webm" type="video/webm">  			  			 I'm sorry; your browser doesn't support HTML5 video in WebM with VP8 or MP4 with H.264.   			
                  <!-- You can embed a Flash player here, to play your mp4 video in older browsers --> 		
            </video>                 
          </div>    
        </div>    
        <div class="group">    
          
          <div class="grid">      
            
            {if $banners.1.link !=''}
            <a href="{$banners.1.link}" class="box">
            {elseif $banners.1.yt != ''}
            <a href="{$banners.1.yt}" class="box fancybox iframe">   
            {else}
            <a href="#" class="box">   
            {/if}   
              
              {if $banners.1.plik !=''}
               <div class="foto" style="background-image: url({$banners.1.plik});">
              </div> 
              {/if}            
              <span class="label {if $banners.1.plik == ''}bezfoto{/if}">{$banners.1.nazwa}
              </span> </a>  	
          </div>  
          
          <div class="grid">      
            
            {if $banners.3.link !=''}
            <a href="{$banners.3.link}" class="box">
            {elseif $banners.3.yt != ''}
            <a href="{$banners.3.yt}" class="box fancybox iframe">   
            {else}
            <a href="#" class="box">   
            {/if}   
              
              {if $banners.3.plik !=''}
               <div class="foto" style="background-image: url({$banners.3.plik});">
              </div> 
              {/if}            
              <span class="label {if $banners.3.plik == ''}bezfoto{/if}">{$banners.3.nazwa}
              </span> </a>  	
          </div>
             
        </div>    
        
        {section name=b loop=$banners start=4}
        
         <div class="grid">      
            
            {if $banners[b].link !=''}
            <a href="{$banners[b].link}" class="box">
            {elseif $banners[b].yt != ''}
            <a href="{$banners[b].yt}" class="box fancybox iframe">   
            {else}
            <a href="#" class="box">   
            {/if}   
              
              {if $banners[b].plik !=''}
               <div class="foto" style="background-image: url({$banners[b].plik});">
              </div> 
              {/if}            
              <span class="label {if $banners[b].plik == ''}bezfoto{/if}">{$banners[b].nazwa}
              </span> </a>  	
          </div>
        
        {/section}       
      </div>