{literal}
 <script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			
      $(".fancybox").fancybox();
      
  });
  
</script>      
{/literal}

<div class="grid  newline">
      <div class="box">
        
        <span class="label bezfoto">{$st.title}</span> </div>
  	</div>
    
   
    <div class="grid_vbig">
      <div class="box">
      <div class="spad">
      
       <div class="tresc">
           <ul id="gallery">
           {foreach from=$zdjecia item=v key=k}
            <li>
            <a href="{$_DOMAIN}{$v.zdjecie}" class="fancybox" title="{$v.nazwa}"><img src="{$_DOMAIN}{$v.zdjecie}"  alt="{$v.nazwa}" /></a>
            </li>
           {/foreach}
           </ul>
           <br style="clear:both;">
           
             
            {$paggin}
           
          </div>
        </div>
        </div>
    </div>




                                       