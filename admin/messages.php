<?php

function break_lines($cnt = 8) {
  for ($i=0; $i<$cnt; $i++) $tu.='<br />';
  return $tu;
}

function page_title($title, $breaks=2) {
  $tu="<h1 style='font-size:18px;font-weight:bold;color:#75848F;'>{$title}</h1>";
  for ($i=0; $i<$breaks; $i++) $tu.='<br />';
  return $tu;
}

function msg_warning($msg) {
	return "<table><tr><td style='vertical-align:middle;' width='20'><img src='../grafika/alert.png' width='16' height='16' border='0' /></td><td style='font-size:12px;color:red;'>{$msg}</td></tr></table>";
}

function msg($msg) {
	return "<span style='font-size:11px;margin-left:5px;'>{$msg}</span><br/>";
}

function msg_back($msg) {
	return "<b>{$msg}</b><br><div style='margin-top:20px;'>
	<a href='javascript:history.back()' style='text-decoration:none;'>
	<img src='gfx/wstecz.gif' style='border:none;' /></a></div>";
}

function msg_link($msg, $textlink, $link) {
	return "<b>{$msg}</b><a href='{$link}' class='msg_link'>{$textlink}</a>";
}

function msg_warning_link($msg, $textlink, $link) {
	return "<table><tr><td style='vertical-align:middle;' width='20'><img src='../grafika/alert.png' width='16' height='16' border='0' /></td><td style='font-size:12px;color:red;'>{$msg}<a href='{$link}' style='margin-left:15px;'>[ {$textlink} ]</a></td></tr></table>";
}

function back($margin='20') {
	return "<div style='margin-top: {$margin}px;'><a href='javascript:history.back()' style='text-decoration:none;'>
	       <img src='gfx/wstecz.gif' style='border:none;' /></a></div>";
}

function more() {
	return "<img src='gfx/wiecej.gif' style='border:none;' />";
}

?>