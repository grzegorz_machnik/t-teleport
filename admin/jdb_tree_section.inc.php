<?php

include(_INCLUDE_PATH.'jdbet_tree.class.php');

$f=$_REQUEST;

$desc=array(
'title'=>'Nazwa',
'id_parent'=>'Kategoria nadrzędna',
'url'=>'Przyjazny link',
'reguser'=>'Zarejestrowani',
);

$a = new jdbet_tree($sqlconn, $table_section);

$wyswietlanie = array(
	'n'=>'Normalne [newsy]',
	'c'=>'Wybór przez combobox',
);

$a->set_deep(2);

$a->set_fielddescriptions($desc);

$a->set_fieldtype('reguser','ajax_switch');

if ($_REQUEST['op']!='edit') $a->set_fieldtype('url','hidden');

$tb=new easytable();
$tb->setsimple();
$tb->opentable('width=650');
$tb->row();
$tb->cell($a->operation($f),'align=center');
$tu=$tb->show();

?>