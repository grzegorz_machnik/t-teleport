<?php

include('head.inc.php');
include('common.php');
include('images.php');

$sid='galeria';

$kategorie = db_getbyindex($sqlconn, "SELECT id, nazwa FROM "._DB_PREFIX."_zdjecia_kategorie WHERE jdb_active='y' ORDER BY jdb_orderkey");

function show_($row) {  
  $row['foto'] = "<a href='../{$row['foto']}'>".get_image($row['foto'], 60, 60)."</a>";
  return $row;
}

function content() {
	global $sqlconn,$kategorie;
  
	$a=new jdbet($sqlconn, _DB_PREFIX."_zdjecia");
	$desc['url']='Przyjazny link';
	$desc['tresc']='Treść';
	$desc['foto']='Zdjęcie';
	$desc['is_hidden']='Czy ukryte?';
  $a->set_fielddescriptions($desc);
  
	$a->set_toshow('foto, nazwa, id_category, is_hidden');
	$a->set_fieldtype('is_hidden','ajax_switch');
	
	 $a->set_artforeignkey('id_category', $kategorie);
 	  $a->set_fieldtype('id_category','combo');
  $a->set_filebrowse('foto','filebrowse.php');
	$a->set_orderby('jdb_orderkey');
	//$a->set_desc('DESC');
	$a->set_showfunct('show_');
    		
	return $a->operation($_REQUEST);
}

$content = content();
//$url_info.=ApplyFriendlyUrl(_DB_PREFIX."_news");
$content = $content;
                                                   
include('foot.inc.php');	