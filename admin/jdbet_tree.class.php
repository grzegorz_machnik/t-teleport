<?php
// jdbet_tree;

function get_section_tree($data, $id_parent=0, $deep=0) {
	global $sqlconn;
	$tmp=array();
	foreach ($data as $k=>$v) {
		if ($v['id_parent']==$id_parent) {
			$tmp[]=array('jdb_tree_data'=>get_section_tree($data,$v['id'], $deep+1 ),'jdb_deep_tree'=>$deep) + $v;
		}
	}
	return $tmp;
}

function show_section_tree($data, $pre='', $deep=0) {
	static $start, $bgcolor;
	
	$tmp='';

	if ($pre=='') {
		$tmp="<table cellspacing=0 cellpadding=0 width=500 style=\"border:1px #e0e0e0 solid;\">
			<tr height=16 bgcolor=#6B6B88><td>&nbsp;&nbsp;<b style=\"color:white;\">Nazwa działu</b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td align=right><b style=\"color:white;\">operacje</b>&nbsp;&nbsp;</td>
			</tr>
			<tr height=16 onmouseover=\"this.bgColor='#e0e0e0';\" onmouseout=\"this.bgColor='#ffffff';\"><td></td>
			<td><a href=\"{$_SERVER['PHP_SELF']}?".sqs("op=add&id_parent=0")."\" title=\"Dodaj kategorię główną\"><img src=\"gfx/addsubsect.gif\" border=0></a></td>
			<td></td>
			<td></td>
			<td></td>
			<td align=right></td>
			</tr>
			";
	}
	
	$cnt=count($data);
	for ($i=0; $i<$cnt; $i++) {

		$bgcolor=$bgcolor!='#e0e0d0'?'#e0e0d0':'#ffffff';

		if ($pre=='') {
			$pre_cur=$pre."<img src=\"gfx/tree_root.gif\" border=0 align=middle>";
		} else {
			if ($i+1<$cnt) 
				$pre_cur=$pre."<img src=\"gfx/tree_subnode.gif\" border=0 align=middle>";
			else $pre_cur=$pre."<img src=\"gfx/tree_node.gif\" border=0 align=middle>";
		}
				
		$tmp.="<tr height=14 bgcolor={$bgcolor} onmouseover=\"this.bgColor='#cae8ff';\" onmouseout=\"this.bgColor='{$bgcolor}'\"><Td height=14>";
		$tmp.="{$pre_cur} {$data[$i]['title']}";
// 		{$deep}/{$data[$i]['jdb_deep_tree']} 
		$tmp.="</td>";
		$tmp.="<td>";
		if ($deep==0 || $deep-1>$data[$i]['jdb_deep_tree']) $tmp.="<a href=\"{$_SERVER['PHP_SELF']}?".sqs("op=add&id_parent={$data[$i]['id']}")."\"><img src=\"gfx/addsubsect.gif\" border=0></a>";
		$tmp.="</td>";
		$tmp.="<td width=20 align=center>";
		if ($i>0 && isset($data[$i]['jdb_orderkey'])) $tmp.="<a href=\"{$_SERVER['PHP_SELF']}?".sqs("op=up&id={$data[$i]['id']}")."\"><img src=\"gfx/up.gif\" border=0></a>";
		$tmp.="</td>";
		$tmp.="<td width=20 align=center>";
		if ($i<$cnt-1 && isset($data[$i]['jdb_orderkey'])) $tmp.="<a href=\"{$_SERVER['PHP_SELF']}?".sqs("op=down&id={$data[$i]['id']}")."\"><img src=\"gfx/down.gif\" border=0></a>";
		$tmp.="</td>";
		$tmp.="<td width=30 align=center>";
		if (isset($data[$i]['jdb_active'])) {
			if ($data[$i]['jdb_active']=='y') $tmp.="<a href=\"{$_SERVER['PHP_SELF']}?".sqs("op=deactiv&id={$data[$i]['id']}")."\"><img src=\"gfx/active.gif\" border=0></a>";
			else $tmp.="<a href=\"{$_SERVER['PHP_SELF']}?".sqs("op=activ&id={$data[$i]['id']}")."\"><img src=\"gfx/deactive.gif\" border=0></a>";
		}
		$tmp.="</td>";
		$tmp.="<td width=120><a href=\"{$_SERVER['PHP_SELF']}?".sqs("op=edit&id_parent={$data[$i]['id_parent']}&id={$data[$i]['id']}")."\"><img src=\"gfx/pl/edit.gif\" border=0></a>
			<a href=\"#\" onclick=\"check('{$_SERVER['PHP_SELF']}?".sqs("op=del&id={$data[$i]['id']}")."','Jesteś pewien, że chcesz skasować {$data[$i]['title']}?');\"><img src=\"gfx/pl/del.gif\" border=0></a></td>";
		$tmp.="</tr>";
			
		if (count($data[$i]['jdb_tree_data'])>0) {
			if ($i+1<$cnt) $tmp.=show_section_tree($data[$i]['jdb_tree_data'], $pre."<img src=\"gfx/tree_spacer.gif\" border=0 align=middle>",$deep);
			else $tmp.=show_section_tree($data[$i]['jdb_tree_data'], $pre."<img src=\"gfx/tree_blank.gif\" border=0 align=middle>",$deep);
		}

	}
	if ($pre=='') {
		$tmp.="</table>";
	}
	return $tmp;	
	
}

function get_combo_tree($data, $pre='', $deep=0) {
	$tmp=array();
	$cnt=count($data);
	for ($i=0; $i<$cnt; $i++) {
		$tmp[$data[$i]['id']]=($pre==''?'&raquo; ':$pre).$data[$i]['title'];
		if (count($data[$i]['jdb_tree_data'])>0) {
			if ($deep==0 || $deep-2>$data[$i]['jdb_deep_tree'])$tmp=$tmp+get_combo_tree($data[$i]['jdb_tree_data'], $pre.'&nbsp;&nbsp;&nbsp;', $deep);
		}
	}
	return array('0'=>'--- kategoria główna ---')+$tmp;
}

function get_combo_tree4prod($data, $pre='', $deep=0) {
	$tmp=array();
	$cnt=count($data);
	for ($i=0; $i<$cnt; $i++) {
		$tmp[$data[$i]['id']]=($pre==''?'&raquo; ':$pre).$data[$i]['title'];
		if (count($data[$i]['jdb_tree_data'])>0) {
			if ($deep==0 || $deep-2>$data[$i]['jdb_deep_tree'])$tmp=$tmp+get_combo_tree4prod($data[$i]['jdb_tree_data'], $pre.'&nbsp;&nbsp;&nbsp;', $deep);
		}
	}
	return $tmp;
}

function get_combo_tree4prodall($data, $pre='', $deep=0) {
	$tmp=array();
	$cnt=count($data);
	for ($i=0; $i<$cnt; $i++) {
		$tmp[$data[$i]['id']]=$pre.' &raquo; '.$data[$i]['title'];
		if (count($data[$i]['jdb_tree_data'])>0) {
			if ($deep==0 || $deep-2>$data[$i]['jdb_deep_tree']) $tmp=$tmp+get_combo_tree4prodall($data[$i]['jdb_tree_data'], $pre.' &raquo; '.$data[$i]['title'], $deep);
		}
	}
	return $tmp;
}


function jdb_tree($sql, $deep=0) {
	global $sqlconn;
	$data = db_getsqltable($sqlconn, $sql);
	$data_tree=get_section_tree( $data!==false?$data:array());
	return show_section_tree($data_tree, '',$deep);
}

class jdbet_tree extends jdbet {
	var $deep=0;
	
	function set_deep($nr) {
		$this->deep=$nr;
	}
	
	function jdbet_tree($sqlconn, $table) {
		$this->jdbet($sqlconn, $table);
		$f=$_REQUEST;
	}
	
	function op_show() {
		$where=$this->where!=''? " where {$this->where} ":'';
		
		$sql_count="select count(*) ";
		$sql_curr="select * ";
		$sql_main = " from {$this->table} {$where}";
		$sql_order = " order by {$this->orderby}";
		$sql=$sql_curr.$sql_main.$sql_order.$sql_limit;
	
		return jdb_tree($sql,$this->deep);	
	}
	
	function op_movedown() {
		$tmp_where=$this->where;
		$id_parent=db_getsinglevalue($this->sqlconn, "select id_parent from {$this->table} where id='{$this->fu[$this->idname]}'");
		$this->where= $this->where!=''?"{$this->where} and id_parent='{$id_parent}'":"id_parent='{$id_parent}' ";
		jdbet::op_movedown();
		$this->where=$tmp_where;
	}
	
	function op_moveup() {
		$tmp_where=$this->where;
		$id_parent=db_getsinglevalue($this->sqlconn, "select id_parent from {$this->table} where id='{$this->fu[$this->idname]}'");
		$this->where= $this->where!=''?"{$this->where} and id_parent='{$id_parent}'":'';
		jdbet::op_moveup();
		$this->where=$tmp_where;
	}
	
	function del_children($table, $remote, $id) {
		$res = db_getbyindex($this->sqlconn, "select id, id from {$table} where {$remote}={$id}");
		if ($res!==false && count($res)>0) {
			foreach ($res as $k=>$v) {
				$plik=glob($this->foto_details['upload_dir']."{$table}_{$v}_*.*");
				if ($plik!==false && count($plik)>0) {
					foreach ($plik as $img) {
						@unlink($img);
					}
				}
				$plik=glob($this->foto_details['upload_dir']."mini/{$table}_{$v}_*.*");
				if ($plik!==false && count($plik)>0) {
					foreach ($plik as $img) {
						@unlink($img);
					}
				}
			}
		}
		$this->sqlconn->query("delete from {$table} where {$remote}='{$id}'");
	}
	
	function del_item($data, $id_parent) {
		$cnt=count($data);
		for ($i=0; $i<$cnt; $i++) {
			if ($data[$i]['id_parent']==$id_parent) {
				if (count($data[$i]['jdb_tree_data'])>0) $this->del_item($data[$i]['jdb_tree_data'], $data[$i]['id']);
				if (count($this->deletechild)>0) {
					foreach ($this->deletechild as $v) {
						$this->del_children($v['table'], $v['remoteid'], $data[$i]['id']);
					}
				}
				$this->sqlconn->query("delete from {$this->table} where id='{$data[$i]['id']}'");
			} else {
				if (count($data[$i]['jdb_tree_data'])>0) $this->del_item($data[$i]['jdb_tree_data'], $id_parent);
			}
		}
	}

	function op_delete() {
		$tmp_data=db_getsqltable($this->sqlconn, "select * from {$this->table}");
		$data=get_section_tree( $tmp_data!==false?$tmp_data:array());
		$this->del_item($data, $this->fu[$this->idname]);
		return jdbet::op_delete();
	}

	function operation($f) {
		$where=$this->where!=''? " where {$this->where} ":'';
		$sql_curr="select * ";
		$sql_main = " from {$this->table} {$where}";
		$sql_order = " order by {$this->orderby}";
		$sql=$sql_curr.$sql_main.$sql_order;
		
		$tmp_data=db_getsqltable($this->sqlconn, $sql, $this->deep);
		$data_tree=get_section_tree($tmp_data!==false?$tmp_data:array());
 		
		jdbet::set_artforeignkey('id_parent',	get_combo_tree($data_tree,'',$this->deep));
 		
		return jdbet::operation($f);
	}
	
}

?>
