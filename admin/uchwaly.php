<?php

include('head.inc.php');
include('common.php');

define('JFORM_CONFIRM','Czy na pewno chcesz dodać uchwałę?\nUchwała zostanie automatycznie wysłana do wszystkich użytkowników.\nPrzed potwierdzeniem sprawdź poprawność');

$sid='wspolnoty';

$wspolnoty = db_getbyindex($sqlconn, "SELECT id, nazwa FROM "._DB_PREFIX."_wspolnoty WHERE jdb_active='y' ORDER BY jdb_orderkey");
$wspolnoty_all = array('' => '- Wszystkie -') + $wspolnoty;

if (isset($f['where_wspolnota'])) $_SESSION['admin']['wspolnoty']['uchwaly']['id_wspolnota'] = $f['where_wspolnota'];
if (!isset($_SESSION['admin']['wspolnoty']['uchwaly']['id_wspolnota'])) $_SESSION['admin']['wspolnoty']['uchwaly']['id_wspolnota'] = '';
	
function navi() {
	global $sqlconn, $wspolnoty_all;
	$f=$_REQUEST;
  	
  $tu.="<form method=\"post\" name=\"wspolnoty_form\">";
  $tu.='Wspólnota: '.jform_combo('where_wspolnota', $wspolnoty_all, $_SESSION['admin']['wspolnoty']['uchwaly']['id_wspolnota'], "onchange=\"wspolnoty_form.submit();\"");	
  $tu.="</form>";	

  return $tu;
}

function _show($row){           
   $row['operacje'] .= '
       <a href="uchwaly.php?op=showresults&id='.$row['id'].'" style="color:black">Wyniki</a><br>
       <a href="javascript:if(confirm(\'Czy napewno chcesz ponownie powiadomić osoby, które jeszcze nie zagłosowały?\')) location.href=\'uchwaly.php?op=sendagain&id='.$row['id'].'\'" style="color:black">Wyślij ponownie do osób <br>które nie zagłosowały</a>    
       ';
   if($row['glosowanie']=='y') $row['operacje'].='<br><a href="javascript:if(confirm(\'Czy napewno chcesz zamknąć głosowanie??\')) location.href=\'uchwaly.php?op=closevote&id='.$row['id'].'\'" style="color:red">Zamknij głosowanie</a>';    
   $row['czy_glosowanie'] = $row['glosowanie']=='n'?"Nie":"Tak";   
   return $row; 
}
	
$addleft= group_left('Wybierz wspólnotę:',navi());

function content() {
	global $sqlconn, $wspolnoty;
  
	$a=new jdbet($sqlconn, _DB_PREFIX."_uchwaly");
	$desc['id_wspolnoty']='Wspólnota';	
	$desc['url']='Przyjazny link';
	$desc['tresc']='Treść';
  $a->set_fielddescriptions($desc);
  
	$a->set_toshow('nazwa, data, czy_glosowanie, operacje');
	if ($_SESSION['admin']['wspolnoty']['uchwaly']['id_wspolnota']!='') {
    $a->set_where(" id_wspolnoty='{$_SESSION['admin']['wspolnoty']['uchwaly']['id_wspolnota']}' ");
	  $a->set_fieldvalue('id_wspolnoty',$_SESSION['admin']['wspolnoty']['uchwaly']['id_wspolnota']);	
	  $a->set_fieldtype('id_wspolnoty','hidden');
	}
  else {
 	  $a->set_artforeignkey('id_wspolnoty', $wspolnoty);
 	  $a->set_fieldtype('id_wspolnoty','combo');  
  }
  
  
  
  $a->set_showfunct('_show');
	$a->set_fieldtype('tresc','htmlarea');
	$a->set_orderby('jdb_orderkey');
	$a->set_desc('DESC');
  		
	return $a->operation($_REQUEST);
}
$sql = "Select value from "._DB_PREFIX."_config where sid = 'email_nadawca' limit 1";
  $nadawca = db_getsinglevalue($sqlconn, $sql);
if($_GET['op']=='showresults'){
   
  $glosy =  array(0=>'Przeciw',1=>'Za',2=>'Wstrzyłmał się'); 

  $sql = "Select * from "._DB_PREFIX."_uchwaly where id = '{$_GET['id']}'";
  $uch = db_getsinglerow($sqlconn, $sql);
  
  $sql = "Select u.*, p.imie, p.nazwisko, p.email, p.login, p.udzial, p.nr_lokalu from "._DB_PREFIX."_users_uchwaly u, "._DB_PREFIX."_users p where u.id_uchwaly = '{$_GET['id']}' and u.id_user = p.id ";
  $us = db_getsqltable($sqlconn, $sql);
             
  $g_0 = 0;
  $g_1 = 0;
  $g_2 = 0;
  
  $ud_0 = 0;
  $ud_1 = 0;
  $ud_2 = 0;
  
  $g_sum = 0;
  $ud_sum = 0;
  
  foreach($us as $k=>$v){
   
   if($v[glos] == '0'){
     $ud_0 += $v['udzial'];
     $g_0++;
   } 
   if($v[glos] == '1'){
    $ud_1 += $v['udzial'];
    $g_1++;
   } 
   if($v[glos] == '2'){
    $ud_2 += $v['udzial'];
    $g_2++;
   }
   $ud_sum += $v['udzial'];
   $g_sum++;
  }
    
    
  
   ob_start();
   include('templates/uchwaly_showresults.php');
   $content = ob_get_contents();
  
  ob_end_clean();
  
   
  if($_GET['mode']=='print'){
   echo $content; die();
  }
  if($_GET['mode']=='sendtouser'){
   
   
   $sql = "Select * from "._DB_PREFIX."_users where id_wspolnoty = '{$uch['id_wspolnoty']}' and jdb_active = 'y' ";
   $users = db_getsqltable($sqlconn, $sql);
   foreach($users as $user){
     jmail_html($user['email'], 'Wyniki głosowania nad uchwała: '.$uch['nazwa'], $content, $nadawca);  
   }
   header('location:uchwaly.php?msg=Wyniki uchwały zostały wysłane o użytkowników');
   die();
  }
  
  
  
  $content .= '<table><tr>
   <td>
    <input type="button" value="Wyślij wyniki do użytkowników via email" onClick="if(confirm(\'Czy chcesz wysłać wyniki głosowania na uchwałe użytkownikom\')) location.href=\'uchwaly.php?op=showresults&id='.$_GET['id'].'&mode=sendtouser\'">
    <input type="button" value="Drukuj wyniki" onClick="window.open(\'uchwaly.php?op=showresults&id='.$_GET['id'].'&mode=print\',\'mywindow\',\'width=800, height=600\')">
   </td>
  </tr></table>';
}
else{

$content = content();

}

if ($_REQUEST['op']=='insert'){
    $sql = "Select * from "._DB_PREFIX."_uchwaly order by id desc limit 1";
    $uchwala = db_getsinglerow($sqlconn, $sql);
    $sql = "Select * from "._DB_PREFIX."_users where id_wspolnoty = '{$uchwala['id_wspolnoty']}' and jdb_active = 'y' ";
    $users = db_getsqltable($sqlconn, $sql);
    if($uchwala['glosowanie']!='y')
      $tresc = db_getsinglevalue($sqlconn, "Select value from "._DB_PREFIX."_text where sid = 'email_after_insert_resolution_without_vote' limit 1");
    else
      $tresc = db_getsinglevalue($sqlconn, "Select value from "._DB_PREFIX."_text where sid = 'email_after_insert_resolution' limit 1");
    
    if($uchwala['glosowanie']=='y'){    
    foreach($users as $user){
     jmail_html($user['email'], 'Nowa uchwała: '.$uchwala['nazwa'], $tresc, $nadawca);       
    }
    }
}
if ($_REQUEST['op']=='sendagain'){
    $sql = "Select * from "._DB_PREFIX."_uchwaly where id = '{$_GET['id']}'";
    $uchwala = db_getsinglerow($sqlconn, $sql);
    $sql = "Select * from "._DB_PREFIX."_users where id_wspolnoty = '{$uchwala['id_wspolnoty']}' and jdb_active = 'y' and email != '' and id not in(select id_user from bud_users_uchwaly where id_uchwaly = '{$_GET['id']}')";
    $users = db_getsqltable($sqlconn, $sql);
        
    if($uchwala['glosowanie']!='y')
      $tresc = db_getsinglevalue($sqlconn, "Select value from "._DB_PREFIX."_text where sid = 'email_after_insert_resolution_without_vote' limit 1");
    else
      $tresc = db_getsinglevalue($sqlconn, "Select value from "._DB_PREFIX."_text where sid = 'email_after_insert_resolution' limit 1");
    
    if($uchwala['glosowanie']=='y'){  
    foreach($users as $user){
     jmail_html($user['email'], 'Nowa uchwała: '.$uchwala['nazwa'], $tresc, $nadawca);  
     
    }
    }
   
   header('location:uchwaly.php');
    
}
if($_REQUEST['op']=='closevote'){
  
  $sql = "Update "._DB_PREFIX."_uchwaly  set glosowanie = 'n' where id = '{$_GET['id']}' limit 1";  
  mysql_query($sql);
  
   $sql = "Select * from "._DB_PREFIX."_uchwaly where id = '{$_GET['id']}'";
    $uchwala = db_getsinglerow($sqlconn, $sql);
   
   $sql = "Select * from "._DB_PREFIX."_users where id_wspolnoty = '{$uchwala['id_wspolnoty']}' and jdb_active = 'y' ";
   $users = db_getsqltable($sqlconn, $sql);
   
   $sql = "Select value from "._DB_PREFIX."_config where sid = 'closevote_email' limit 1";
   $tresc = db_getsinglevalue($sqlconn, $sql);
                                                 
   $tresc = str_replace('%UCHWALA%',$uchwala['nazwa'],$tresc);
   $tresc = nl2br($tresc);
                                                
   foreach($users as $user){
     jmail_html($user['email'], 'Głosowanie nad uchwałą: '.$uch['nazwa'].' zakończone ', $tresc, $nadawca);     
   }   
   header('location:uchwaly.php?msg=Głosowanie zostało zamknięte');
   die();
  }

$url_info.=ApplyFriendlyUrl(_DB_PREFIX."_uchwaly");
$content = $url_info.$content;
                                                   
include('foot.inc.php');	