<?php

include('head.inc.php');

$sid='config';

function showfunct($row) {	
	$row['_name']=$row['name'];	
	return $row;
}

function content() {
	global $sqlconn;
	
	$a=new jdbet($sqlconn, _DB_PREFIX."_config");
	$fielddesc['_name']='Nazwa';
	$fielddesc['var']='Wartość';
  $a->set_fielddescriptions($fielddesc);
	$a->set_fieldtype('sid','hidden');
	$a->set_fieldtype('name','hidden');
	$a->set_fieldtype('fieldtype','hidden');
	$a->set_toshow('nazwa, var');
	$a->set_showfunct('showfunct');
  $a->unset_operation('del');

  if($_GET['id']=='5')
  $a->set_filebrowse('var','filebrowse.php');

	return $a->operation($_REQUEST);
}

$content=content();

include('foot.inc.php');

?>