<?php

include('config.inc.php');
include('messages.php');
include('common.php');

function ajaxDeleteComment() {
  global $sqlconn;
  $f=$_REQUEST;

  if (!isset($f['id'])) return 'error';
  if (!isset($f['table_name'])) return 'error';
 
  $result = $sqlconn->query("DELETE FROM {$f['table_name']} WHERE id='{$f['id']}'");
  if ($result==true) return 'ok';
  else return 'error';
}

function ajaxShowOnMain() {
  global $sqlconn;
  $f=$_REQUEST;

  if (!isset($f['id_content']) || !isset($f['value'])) return;

  if ($f['value']=='y') {  
    $on_main = db_getsinglevalue($sqlconn, "SELECT id FROM "._DB_PREFIX."_main_order WHERE id_content={$f['id_content']}");
    if ($on_main != false ) return;
    
    $orderkey = db_getsinglevalue($sqlconn, "SELECT MAX(jdb_orderkey) FROM "._DB_PREFIX."_main_order");    
    $orderkey = $orderkey + 1;

    $result = $sqlconn->query("INSERT INTO `"._DB_PREFIX."_main_order` (`id_content`, `jdb_orderkey`) VALUES ({$f['id_content']}, {$orderkey});");
    if ( $result == false ) return false;
    
    return true;
  }
  else if ($f['value']=='n') {    
    $on_main = db_getsinglevalue($sqlconn, "SELECT id FROM "._DB_PREFIX."_main_order WHERE id_content={$f['id_content']}");
    if ($on_main == false) return false;
    
    $result = $sqlconn->query("DELETE FROM "._DB_PREFIX."_main_order WHERE id_content={$f['id_content']}");
    if ($result== false) return false;
    
    return true;    
  }
  else return false;
}

function ajaxShowOnMainMovie() {
  global $sqlconn;
  $f=$_REQUEST;

  if (!isset($f['id_movie']) || !isset($f['value'])) return;

  if ($f['value']=='y') {  
    $on_main = db_getsinglevalue($sqlconn, "SELECT id FROM "._DB_PREFIX."_main_movies_order WHERE id_movie={$f['id_movie']}");
    if ($on_main != false ) return;
    
    $orderkey = db_getsinglevalue($sqlconn, "SELECT MAX(jdb_orderkey) FROM "._DB_PREFIX."_main_movies_order");    
    $orderkey = $orderkey + 1;

    $result = $sqlconn->query("INSERT INTO `"._DB_PREFIX."_main_movies_order` (`id_movie`, `jdb_orderkey`) VALUES ({$f['id_movie']}, {$orderkey});");
    if ( $result == false ) return false;
    
    return true;
  }
  else if ($f['value']=='n') {    
    $on_main = db_getsinglevalue($sqlconn, "SELECT id FROM "._DB_PREFIX."_main_movies_order WHERE id_movie={$f['id_movie']}");
    if ($on_main == false) return false;
    
    $result = $sqlconn->query("DELETE FROM "._DB_PREFIX."_main_movies_order WHERE id_movie={$f['id_movie']}");
    if ($result== false) return false;
    
    return true;    
  }
  else return false;
}

function ajaxMovieThumbnail() {
  global $sqlconn;
  $f=$_REQUEST;        		

  $record = db_getsinglerow($sqlconn, "SELECT id, url FROM "._DB_PREFIX."_movies WHERE id={$f['id']}");
  if ($record==false) return 'error';
  
  if (file_exists('../thumbs'."/mov".$record['id'].".jpg")) unlink('../thumbs'."/mov".$record['id'].".jpg");
   
  $fpatch = $movie = 'http://'.$_SERVER['HTTP_HOST'].'/'.$record['url'];

  $mov = new ffmpeg_movie($movie);

  $frcount=$mov->getFrameCount()-1;
  $try = 1;
  $fc = 1;

  while(1) {
    $p = rand(1,$frcount);
    $ff_frame= $mov->getFrame($p);
    if($ff_frame==true) {
      $gd_image = $ff_frame->toGDImage();
      $ff='../thumbs'."/mov".$record['id'].".jpg";
      imagejpeg($gd_image, $ff);
      $fc++;
    }
    $try++;
    if($try>10 || $fc==4) break;
  }
  
  return 'ok';
}

$f=$_REQUEST;

switch ($f['op']) {
  case 'movie_thumbnail':
    echo ajaxMovieThumbnail();
  break;
  case 'delete_comment':
    echo ajaxDeleteComment();
  break;
  case 'show_on_main':
    $result = ajaxShowOnMain();   

    if ($f['value']=='y') {
        if ($result == true ) echo on_main_switch_OFF($f['id_content']); 
        else if ($result == false ) echo on_main_switch_ON($f['id_content']);
    }
    else if ($f['value']=='n' ) {
        if ( $result == true ) echo on_main_switch_ON($f['id_content']);
        else if ( $result == false ) echo on_main_switch_OFF($f['id_content']);
    }
  break;
  case 'show_on_main_movie':
    $result = ajaxShowOnMainMovie();   

    if ($f['value']=='y') {
        if ($result == true ) echo on_main_movie_switch_OFF($f['id_movie']); 
        else if ($result == false ) echo on_main_movie_switch_ON($f['id_movie']);
    }
    else if ($f['value']=='n' ) {
        if ( $result == true ) echo on_main_movie_switch_ON($f['id_movie']);
        else if ( $result == false ) echo on_main_movie_switch_OFF($f['id_movie']);
    }
  break;
}

// close mysql connection
$sqlconn->close();
// -->

?>