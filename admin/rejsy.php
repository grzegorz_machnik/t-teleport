<?php
include('head.inc.php');
define('_TMPVAR','stronytmp');
include_once(_INCLUDE_PATH.'jdbet_tree.class.php');
$_REQUEST['id_site']?$_SESSION['id_site']=$_REQUEST['id_site']:0;
$sid='rejsy';

$f=$_REQUEST;

$wspolnoty = db_getbyindex($sqlconn, "SELECT id, nazwa FROM "._DB_PREFIX."_rejsy_kategorie");
$wspolnoty_all = array('' => '- Wszystkie -') + $wspolnoty;


if (isset($f['where_rkategoria'])) $_SESSION['admin']['where_rkategoria'] = $f['where_rkategoria'];
if (!isset($_SESSION['admin']['where_rkategoria'])) $_SESSION['admin']['where_rkategoria'] = '';

function navi() {
	global $sqlconn, $wspolnoty_all;
	$f=$_REQUEST;
  	
  $tu.="<form method=\"post\" name=\"where_rkategoria_form\">";
  $tu.='Kategoria: '.jform_combo('where_rkategoria', $wspolnoty_all, $_SESSION['admin']['where_rkategoria'], "onchange=\"where_rkategoria_form.submit();\"");	
  $tu.="</form>";	

  return $tu;
}
	
$addleft= group_left('Wybierz wspólnotę:',navi());
  

function content() {
	global $sqlconn, $sid, $ssid, $fielddesc, $gals;
	 $fd = array( 
  'title'=>'Tytuł',
  'description'=>'Krótki opis',
  'description2'=>'Rozwinięcie',
  'content'=>'Treść', 
  'jdb_active'=>'Aktywne',
  'date'=>'Data',
  'yacht_id'=>'Jacht',
  'kategoria_id'=>'Kategoria rejsu',
  'content_pl'=>'Zawartość polska', 'content_en'=>'Zawartość angielska',
            'content_de'=>'Zawartość niemiecka', 'content_cz'=>'Zawartość czeska','content_ru'=>'Zawartość rosyjska',
            'description_pl'=>'Opis polski', 'description_en'=>'Opis angielski',
            'description_de'=>'Opis niemiecki', 'description_cz'=>'Opis czeski','description_ru'=>'Opis rosyjski',
            'title_pl'=>'Tytuł polski','title_en'=>'Tytuł angielski',
            'title_de'=>'Tytuł niemiecki','title_cz'=>'Tytuł czeski','title_ru'=>'Tytuł rosyjski',
  'id_gallery'=>'Wybirze nazwę galleri, <br>która m zostać wyświetlona <br>po wejćiu w aktualnośc' );
	$a=new jdbet($sqlconn, _DB_PREFIX."_rejsy");
	

  
	$a->set_fielddescriptions($fd);
	
	$a->set_toshow('nr_rejsu,termin,liczba_dni,cena,czy_dostepny,kategoria_id');
	
	  //$a->set_fieldtype('id_site','hidden');
	  $a->set_fieldtype('jdb_active','hidden');
	  //$a->set_fieldvalue('id_site',$_SESSION['id_site']);
    if(isset($_GET['op']))
    $a->set_fieldtype('czy_dostepny','hidden');
    else
	  $a->set_fieldtype('czy_dostepny','ajax_switch');
    $a->set_artforeignkey('yacht_id', db_getbyindex($sqlconn, "select id, nazwa from "._DB_PREFIX."_yachty"));
 	  $a->set_fieldtype('yacht_id','combo');
    
    
    $a->set_artforeignkey('kategoria_id', db_getbyindex($sqlconn, "select id, nazwa from "._DB_PREFIX."_rejsy_kategorie"));
 	  $a->set_fieldtype('kategoria_id','combo');
    
    
    
    if ($_SESSION['admin']['where_rkategoria']!='') {
      $a->set_where(" kategoria_id = '{$_SESSION['admin']['where_rkategoria']}' ");
      $a->set_fieldvalue('kategoria_id',$_SESSION['admin']['where_rkategoria']);	
  	  $a->set_fieldtype('kategoria_id','hidden');
  	}
    //$a->set_fieldtype('uwagi', 'htmlarea');
    
	
	$tu.=$a->operation($_REQUEST);
	return $tu;
}

$content= content();


include('foot.inc.php');
?>