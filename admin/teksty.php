<?php

include('head.inc.php');

$sid='config';

function showfunct($row) {	
	$row['_name']=$row['name'];	
	return $row;
}

function content() {
	global $sqlconn;
	
	$a=new jdbet($sqlconn, _DB_PREFIX.'_text');
	$fielddesc['_name']='Nazwa';
	$fielddesc['value']='Tekst';
  $a->set_fielddescriptions($fielddesc);
	$a->set_fieldtype('value','htmlarea');
	$a->set_fieldtype('sid', 'hidden');
	$a->set_fieldtype('name', 'hidden');
	$a->set_showfunct('showfunct');
	$a->set_toshow('_name');
  $a->unset_operation('del');

	return $a->operation($_REQUEST);
}

$content=content();

include('foot.inc.php');

?>
