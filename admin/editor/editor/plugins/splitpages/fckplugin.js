// Our method which is called during initialization of the toolbar.
function SplitPages()
{
}

// Disable button toggling.
SplitPages.prototype.GetState = function()
{
	return FCK_TRISTATE_OFF;
}

// Our method which is called on button click.
SplitPages.prototype.Execute = function()
{
  FCK.InsertHtml('%STRONA%');
}

// Register the command.
FCKCommands.RegisterCommand('splitpages', new SplitPages());

// Add the button.
var item = new FCKToolbarButton('splitpages', 'Podziel strone');
item.IconPath = FCKPlugins.Items['splitpages'].Path + 'splitpages.png';
FCKToolbarItems.RegisterItem('splitpages', item);