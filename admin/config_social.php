<?php

include('head.inc.php');

$sid='config';

function showfunct($row) {	
	$row['_name']=$row['name'];	
	return $row;
}

function content() {
	global $sqlconn;
	
	$a=new jdbet($sqlconn, _DB_PREFIX."_social");
	$fielddesc['_name']='Nazwa';
	$fielddesc['value']='Wartość';
  $a->set_fielddescriptions($fielddesc);
	
	$a->set_toshow('nazwa, url');
	$a->set_filebrowse('foto','filebrowse.php');
	
	return $a->operation($_REQUEST);
}

$content=content();

include('foot.inc.php');

?>