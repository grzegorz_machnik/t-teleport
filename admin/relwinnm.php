<?php
session_start();
include('config.inc.php');

$f=$_REQUEST;

function makelink($arr=array()) {
	$tu='';
	if (count($arr)>0) { 
		foreach ($arr as $k=>$v) {
			$tu[]="{$k}={$v}";
		}
		$tu=implode('&amp;',$tu);
	}
	return $tu;
}

function op_showsearchform() {
	$tu.=jform_open($_SERVER['PHP_SELF']);
	$tu.='Wyszukaj: '.jform_text('q',$_SESSION['contener']['query_string']);
	$tu.=jform_submit('ok','Szukaj').'&nbsp;&nbsp;';
	$tu.=jform_redirect('redir','Poka� wszystkie',"{$_SERVER['PHP_SELF']}?q=");
	$tu.=jform_close();
	return $tu;
}

function op_showoffsetlink($offset, $limit, $allrows, $add=array()) {
	$lt=4;
	$offset_link='offs';
	if ($limit!==false) {
		$cnt=ceil($allrows/$limit);
		if ($offset>($cnt*$limit)) {
			$offset=(($cnt*$limit));
			$_SESSION[$offset_link]=$offset;
		}
		$tu=array();
		if ($cnt>1) {

			$link=makelink($add + array("{$offset_link}"=>'0'));
			$tu[]="<a href=\"{$_SERVER['PHP_SELF']}?{$link}\" title=\"Pocz�tek listy\">&laquo;&laquo;</a>&nbsp;&nbsp;";
			if (($offset/$limit-$lt)>0) {
				$start=$offset/$limit-$lt+1;
				$tu[]="...";
			} else {
				$start=1;
			}
			if (($offset/$limit+$lt)>$cnt) {
				$stop=$cnt;
				$end=0;
			} else {
				$stop=$offset/$limit+$lt+1;
				$end=1;
			}

			for ($i=$start; $i<=$stop; $i++) {
				$link=makelink($add+array("{$offset_link}"=>($i-1)*$limit));
				$style=$offset==($i-1)*$limit?' style="font-weight:bold; color:red; font-size:13px;"':'';
				$tu[]=" <a href=\"{$_SERVER['PHP_SELF']}?{$link}\"{$style}>{$i}</a> ";
			}
			
			if ($end==1) $tu[]='...';

			$link=makelink($add+array("{$offset_link}"=>$cnt*$limit-$limit));
			$tu[]="&nbsp;&nbsp;<a href=\"{$_SERVER['PHP_SELF']}?{$link}\" title=\"Koniec listy\">&raquo;&raquo;</a>";
			
		}


		if (count($tu)>0) {
			$link=makelink($add+array("{$offset_link}"=>$offset>0?$offset-$limit:0));
			$prev="<a href=\"{$_SERVER['PHP_SELF']}?{$link}\">&laquo; poprzednia strona</a>";
			$link=makelink($add+array("{$offset_link}"=>$offset+$limit<$cnt*$limit?$offset+$limit:($cnt-1)*$limit));
			$next="<a href=\"{$_SERVER['PHP_SELF']}?{$link}\">nast�pna strona &raquo;</a>";
			$tmp = implode(' | ',$tu);
			$tu = "<table width=\"100%\" cellspacing=1 cellpadding=2 bgcolor=#e0e0e0><tr><Td bgcolor=#f0f0f0 width=150 align=left>{$prev}</td><Td bgcolor=#f0f0f0 align=center><b>Znaleziono {$allrows} wpis�w</b></td><Td bgcolor=#f0f0f0 width=150 align=right>{$next}</td></tr><tr><td bgcolor=#ffffff align=center colspan=3>{$tmp}</td></tr></table>";
			return $tu;
		} else return '';
		
	} return '';
}


function content($contener, $table, $id, $show, $where, $order, $query='') {
	global $sqlconn;
	
	$f=$_REQUEST;
	
	$limit=12;
	
	if ($f['offs']!='') $offs=$f['offs'];
	else $offs=0;
	
	$limit_str=" limit {$offs}, {$limit}";

	if ($query!='') {
		$where = ($where!=''?" ({$where}) and {$show} like '%{$query}%' ":" {$show} like '%{$query}%' ");
	}	
	
	$sql = "from {$table} ".($where!=''?" where {$where}":'').($order!=''?" order by {$order}":'');

	$cnt = db_getsinglevalue($sqlconn, "select count(*) {$sql} ");

	$sql = "select {$id}, {$show} {$sql} {$limit_str}";
	$res = db_getbyindex($sqlconn, $sql);
	if($res!==false) {
		$tu.="<table class=\"relwin1n\" width=\"100%\">";
		foreach ($res as $k=>$v) {
			$tu.="<tr id=\"{$contener}_row_{$k}\"><td onclick=\"javascript:jdbet_updateContener('{$contener}','{$k}','{$v}');\"><input type=\"checkbox\" id=\"jdbet_relwin_{$k}\">{$v}</td></tr>";
		}
		$tu.="</table>";
	}
	return op_showoffsetlink($offs, $limit, $cnt).op_showsearchform().'<div align=left>'.$tu.'</div>'.op_showoffsetlink($offs, $limit, $cnt);
	
}

if (isset($f['contener'])) { 
	foreach ($f as $in=>$out) {
		$f[$in]=rawurldecode($out);
	}
	
	$_SESSION['contener']=$f;
}
if (isset($f['q'])) {
	$_SESSION['contener']['query_string']=$f['q'];
}
$dt=&$_SESSION['contener'];

$content=content($dt['contener'],$dt['rout_table'], $dt['rout_id'], $dt['rout_sqlshow'], $dt['rout_where'], $dt['rout_order'], $_SESSION['contener']['query_string']);
$addbody=" onload=\"updateFromContener('{$dt['contener']}');\"";

$a=new etpl( 'tpl/empty_2.tpl' );
$a->add_item('addbody',$addbody);
$a->add_item( 'content',$content );
echo $a->show();
$sqlconn->close();
?>
