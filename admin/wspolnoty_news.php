<?php

include('head.inc.php');
include('common.php');

$sid='wspolnoty';

$wspolnoty = db_getbyindex($sqlconn, "SELECT id, nazwa FROM "._DB_PREFIX."_wspolnoty WHERE jdb_active='y' ORDER BY jdb_orderkey");
$wspolnoty_all = array('' => '- Wszystkie -') + $wspolnoty;

if (isset($f['where_wspolnota'])) $_SESSION['admin']['wspolnoty']['news']['id_wspolnota'] = $f['where_wspolnota'];
if (!isset($_SESSION['admin']['wspolnoty']['news']['id_wspolnota'])) $_SESSION['admin']['wspolnoty']['news']['id_wspolnota'] = '';
	
function navi() {
	global $sqlconn, $wspolnoty_all;
	$f=$_REQUEST;
  	
  $tu.="<form method=\"post\" name=\"wspolnoty_form\">";
  $tu.='Wspólnota: '.jform_combo('where_wspolnota', $wspolnoty_all, $_SESSION['admin']['wspolnoty']['news']['id_wspolnota'], "onchange=\"wspolnoty_form.submit();\"");	
  $tu.="</form>";	

  return $tu;
}
	
$addleft= group_left('Wybierz wspólnotę:',navi());

function content() {
	global $sqlconn, $wspolnoty;
  
	$a=new jdbet($sqlconn, _DB_PREFIX."_wspolnoty_news");
	$desc['id_wspolnoty']='Wspólnota';	
	$desc['url']='Przyjazny link';
	$desc['tresc']='Treść';
  $a->set_fielddescriptions($desc);
  
	$a->set_toshow('nazwa, url, data');
	if ($_SESSION['admin']['wspolnoty']['news']['id_wspolnota']!='') {
    $a->set_where(" id_wspolnoty='{$_SESSION['admin']['wspolnoty']['news']['id_wspolnota']}' ");
	  $a->set_fieldvalue('id_wspolnoty',$_SESSION['admin']['wspolnoty']['news']['id_wspolnota']);	
	  $a->set_fieldtype('id_wspolnoty','hidden');
	}
  else {
 	  $a->set_artforeignkey('id_wspolnoty', $wspolnoty);
 	  $a->set_fieldtype('id_wspolnoty','combo');  
  }

	$a->set_fieldtype('zajawka','htmlarea');
	$a->set_fieldtype('tresc','htmlarea');
	$a->set_orderby('jdb_orderkey');
	$a->set_desc('DESC');
  		
	return $a->operation($_REQUEST);
}

$content = content();
$url_info.=ApplyFriendlyUrl(_DB_PREFIX."_wspolnoty_news");
$content = $url_info.$content;
                                                   
include('foot.inc.php');	