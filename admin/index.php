<?php

include('head.inc.php');
include('common.php');
include_once(_INCLUDE_PATH.'jdbet_tree.class.php');

$sid='index';




function content() {
	global $sqlconn;
	
   $fd = array( 
  'title_pl'=>'Tytuł polski',
  'title_en'=>'Tytuł angielski',
  'title_fr'=>'Tytuł francuski',
  'id_parent'=>'Kategoria nadrzędna',
  'description'=>'Krótki opis',
  'url'=>'Link alternatywny',
  'img'=>'Zdjęcie boczne',
  'main_bg'=>'Banner główny',
  'typ'=>'Typ strony',
  'mtop'=>'Czy w menu górnym?',
  'mleft'=>'Czy w menu lewym?',
  'content_pl'=>'Treść polska',
  'content_en'=>'Treść angielska',
  'content_fr'=>'Treść francuska', 
  'jdb_active'=>'Aktywne',
  'date'=>'Data',
  );
  
	//$a = new jdbet_tree($sqlconn, _DB_PREFIX."_menu", 0);
  	$a=new jdbet($sqlconn, _DB_PREFIX."_menu");
  //$a->set_deep(2); // glebokosc
  $a->set_fielddescriptions($fd);
	
  
  
  $a->set_fieldtype('content_pl', 'htmlarea');
  $a->set_fieldtype('content_en', 'htmlarea');
  
  //$a->set_fieldtype('content', 'hidden');
  $a->set_fieldtype('skrot', 'hidden');
 // $a->set_fieldtype('url', 'hidden');
  
	$a->set_toshow('title_pl, title_en');
  $a->set_filebrowse('img','filebrowse.php');
  $a->set_filebrowse('main_bg','filebrowse.php');
  //$a->set_fotodetails(100);
  
  $a->set_artforeignkey('typ',array('text'=>'Strona tekstowa','news'=>'Aktualności','gallery'=>'Galeria'));//,'blog'=>'Blog','gallery'=>'Galeria','news'=>'Aktualności'));
  $a->set_fieldtype('typ','combo');
  //$a->set_fieldtype('typ','hidden');
  $a->set_fieldtype('id_parent','hidden');
    
	return $a->operation($_REQUEST);      
}

$content=content();
//$url_info.=ApplyFriendlyUrl(_DB_PREFIX."_oferta");
//$content = $url_info.$content;

include('foot.inc.php');

?>
