<?php

$_PERMISSION = false;

if ($_REQUEST['logout']==1) {
	unset($_SESSION);
	unset($_REQUEST);
	session_destroy();
}

function login_form($kom='') {
	global $lng_cfg;

	$container_open='<table width=100% height=80%><tr><td align=center valign=middle>'.$kom.'<br/><br/>';
	
	$tu.=jform_open($_SERVER['PHP_SELF']);	
	$tu.="<table style='border:1px solid #CDCDCD;'>";
	$tu.="<tr>";
  $tu.="<td>{$lng_cfg['field_login']}:</td>";
  $tu.="<td>".jform_text('login','')."</td>";
  $tu.="</tr>";
	$tu.="<tr>";
  $tu.="<td>{$lng_cfg['field_pass']}:</td>";
  $tu.="<td>".jform_password('pass','')."</td>";
  $tu.="</tr>";
	$tu.="<tr>";
  $tu.="<td colspan='2' align='right'>".jform_submit('ok',$lng_cfg['login_btn'])."</td>";
  $tu.="</tr>";
  $tu.="</table>";
  $tu.=jform_hidden('loginform','ok').jform_close();
  
	$container_close='</td></tr></table>';  
	
	return $container_open.$tu.$container_close;
}

function check_auth() {
	global $sqlconn, $lng_cfg, $_PERMISSION;
	if ($_REQUEST['loginform']=='ok' && $_REQUEST['pass']!='' && $_REQUEST['login']!='') {
		$sql =  "SELECT * FROM "._DB_PREFIX."_admins WHERE login='{$_REQUEST['login']}' AND pass=md5('{$_REQUEST['pass']}') AND jdb_active='y' ";
		$res=db_getsinglerow($sqlconn, $sql);
		if ( is_array($res) && count($res)>0 ) {
			unset($res['pass']);
			$_SESSION['userdata']=$res;
			if ($_SESSION['userdata']['login']=='monadmin') $_PERMISSION = true;
			return true;
		}
		else return login_form($lng_cfg['user_pass_fail']);
	}
	else return login_form('Podaj login i has�o');
}

$check=check_auth();

if (!isset($_SESSION['userdata'])) {
	if ($_REQUEST['loginform']=='ok') {
		$content=$check;
	} else $content=login_form();
	
	$a=new etpl('tpl/empty.tpl');
	$a->add_item('content',$content);
	//$a->add_item('site_title',$lng_cfg['sitetitle_login']);
	echo $a->show();
	$sqlconn->close();
	exit();
}

?>