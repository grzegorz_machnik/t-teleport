<?php

class jdb_relationNM {
	var $sqlconn;
	
	var $ntable, $nid, $nshow, $norder, $nwhere;
	var $rtable, $rmid, $rnid;
	var $mainid, $mainshow;
	var $backlink, $backlinkshow;
	var $inwhere;
	
	function jdb_relationNM($sqlconn) {
		$this->sqlconn=$sqlconn;
	}
	
	function set_main($id, $show) {
		$this->mainid=$id;
		$this->mainshow=$show;
	}
	
	function set_relation($rtable, $rmid, $rnid, $table, $id, $show) {
		$this->rtable=$rtable;
		$this->rmid=$rmid;
		$this->rnid=$rnid;
		$this->ntable=$table;
		$this->nid=$id;
		$this->nshow=$show;
	}
	
	function set_order($order) {
		$this->norder=$order;
	}
	
	function set_where($where) {
		$this->nwhere=$where;
	}

	function set_backlink($link, $show) {
		$this->backlink=$link;
		$this->backlinkshow=$show;
	}

	// ---

	function ajax_selectcheckbox($id) {
		static $fots;
		$fots = db_getbyindex($this->sqlconn, "select {$this->rnid}, {$this->rnid} from {$this->rtable} where {$this->rmid}='{$this->mainid}'"); 
		if (isset($fots[$id])) $check=" checked"; else $check='';
		$tu="<input type=\"checkbox\" name=\"{$this->rtable}_{$id}\" id=\"{$this->rtable}_{$id}\"{$check} onclick=\"javascript:setselection('{$this->rtable}','{$this->rmid}','{$this->rnid}','{$this->mainid}','{$id}');\">";
		return $tu;
	}

	// ---
	
	function showlist() {
	
		$sql = "select {$this->nid} as id,{$this->nshow} as val from {$this->ntable}";
		
		$f=$_REQUEST;
		$where=array();
		if ($this->nwhere!='') $where[] = $this->nwhere;
		
		if ($f['op']!='all' && $f['op']!='') {
			if ($f['op']=='selected') {
				$idki=db_getbyindex($this->sqlconn, "select {$this->rnid}, {$this->rnid} from {$this->rtable} where {$this->rmid}='{$this->mainid}' ");
				if ($idki!='') {
					$where[] = " {$this->nid} in (".implode(',',$idki).")";
				} else return "Nie ma zaznaczonych �adnych pozycji";
			}
			if ($f['op']=='search') {
				$where[]= " {$this->nshow} like '%{$f['q']}%' ";
			}
		}
		
		if (count($where)>0) {
			$sql.= " where ".implode(' and ', $where);
		}
		if ($this->norder!='') $sql.=" order by {$this->norder} ";

		$res = db_getbyindex($this->sqlconn, $sql);

		$tb=new easytable();
		$tb->opentable('width=500');
		if ($res!==false) {
			foreach ($res as $k=>$v) {
				$tb->row();
				$tb->cell( $this->ajax_selectcheckbox($k),'width=20' );
				$tb->cell($v);
			}
		}
		
		$tu.=$tb->show();
		
		return $tu;
		
	}

	function ctrlpanel() {
		$tb=new easytable();
		$tb->setsimple();
		$tb->opentable('width=780 cellspacing=1 cellpadding=3 bgcolor=#a0a0a0');
		$tb->row();
		$tb->cell("EDYTUJESZ: <b style=\"font-size:13px;\">{$this->mainshow}</b><br>",'colspan=3');
		$tb->row('bgcolor=#ffffff');
		$tb->cell('POKA�','colspan=2');
		$tb->cell( jform_open($_SERVER['PHP_SELF'])."Szukaj: ".
			jform_text('q','').
			jform_submit('ok','OK').
			jform_hidden('op','search').
			jform_close() ,'rowspan=2');
		
		$tb->row('bgcolor=#ffffff');
		$tb->cell("<a href=\"?op=selected\">Tylko zaznaczone</a>");
		$tb->cell("<a href=\"?op=all\">Wszystkie</a>");
		$tu.=$tb->show();
		$tu.="<br>";
		return $tu;
	}

	function show() {
		$tu.=$this->ctrlpanel();
		$tu.=$this->showlist();
		$tu.="<a href=\"{$this->backlink}\">{$this->backlinkshow}</a>";
		return $tu;
	}
	
	function get_addbody() {
		return " onload=\"initSelection('{$this->rtable}','{$this->rmid}','{$this->rnid}','{$this->mainid}');\"";
	}
	
}



?>
