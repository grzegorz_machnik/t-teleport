<?php
function dumpVar($pVar) {
	echo '<pre>';
	var_dump($pVar);
	echo '</pre>';
}

function dump($pStr) {
	echo '<pre>';
	print_r($pStr);
	echo '</pre>';
}


/* xml */
function SimpleToXML($xml, $file) {
	$fh = fopen($file, 'w');
	fwrite($fh, $xml);
	fclose($fh);
	}

/* detekcja przegladarki */
function detect_agent() { 
	// USTAWIENIA
	// Wykrywane przegladarki
	$search = array(0 =>"MSIE","Firefox","Opera","Safari","Lynx");
	$agent = $_SERVER['HTTP_USER_AGENT'];
	// Wykrycie wersji Internet Explorera
	function IEversion($header) {
		$detect_version = "Internet Explorer " . 
		substr($header, strpos($header, "MSIE")+5, 1);
		return $detect_version;
	}
	// Wykrywanie przegladarki
	foreach ($search as $key) {
		if (ereg($key, $agent) == 1) { 
			if ($key == 'MSIE') { 
				 return IEversion($agent);
			}
			else
				return $key;
		}
	}
}
/* end detekcja przegladarki */

/* jaka przegladarka */
$browser = detect_agent();

switch ($browser) {
	case 'Internet Explorer 6' :
		$przegladarka = 'ie6';
		break;
	case 'Internet Explorer 7' :
		$przegladarka = 'ie7';
		break;
	case 'Firefox' :
		$przegladarka = 'firefox';
		break;
	case 'Opera' :
		$przegladarka = 'opera';
		break;
	}
/* jaka przegladarka */


  function showDateDM($pDate) {
        $tTab = array();
        $tTabDays = array();
        $res = '';

        $tTab = explode(" ", $pDate);
        $tTabDays = explode("-", $tTab[0]);

        switch ($tTabDays[1]) {
              case '01': $res = 'stycznia'; break;
              case '02': $res = 'lutego'; break;
              case '03': $res = 'marca'; break;
              case '04': $res = 'kwietnia'; break;
              case '05': $res = 'maja'; break;
              case '06': $res = 'czerwca'; break;
              case '07': $res = 'lipca'; break;
              case '08': $res = 'sierpnia'; break;
              case '09': $res = 'wrze�nia'; break;
              case '10': $res = 'pa�dziernika'; break;
              case '11': $res = 'listopada'; break;
              case '12': $res = 'grudnia'; break;
        }

        $res = strval(intval($tTabDays[2])).' '.$res;

        return $res;
  }

  function showDateDMY($pDate) {
      $res = substr($pDate, 8, 2).'/'.substr($pDate, 5, 2).'/'.substr($pDate, 0, 4);
      
      return $res;
  }

  function showDateFull($pDate, $pDay = '') {
        $tTabDays = explode("-", $pDate);

        switch ($tTabDays[1]) {
              case '01': $res = 'stycznia'; break;
              case '02': $res = 'lutego'; break;
              case '03': $res = 'marca'; break;
              case '04': $res = 'kwietnia'; break;
              case '05': $res = 'maja'; break;
              case '06': $res = 'czerwca'; break;
              case '07': $res = 'lipca'; break;
              case '08': $res = 'sierpnia'; break;
              case '09': $res = 'wrze�nia'; break;
              case '10': $res = 'pa�dziernika'; break;
              case '11': $res = 'listopada'; break;
              case '12': $res = 'grudnia'; break;
        }

        switch ($pDay) {
              case '0': $pDay = 'Niedziela, '; break;
              case '1': $pDay = 'Poniedzia�ek, '; break;
              case '2': $pDay = 'Wtorek, '; break;
              case '3': $pDay = '�roda, '; break;
              case '4': $pDay = 'Czwartek, '; break;
              case '5': $pDay = 'Pi�tek, '; break;
              case '6': $pDay = 'Sobota, '; break;
        }
        
        $tmp = $tTabDays[2];
        $tmp[0] == '0' ? $tTabDays[2] = $tmp[1] : '';

        $res = $pDay.$tTabDays[2].' '.$res;

        $res = $res.' '.$tTabDays[0];

        return $res;
  }
  
  function showAge($pDate) {
      $pDate = substr($pDate, 0, 4);
      $now = date('Y');
      
      $res = strval(intval($now) - intval($pDate));
      
      return $res;
  }
  
  function showAgeFull($pDate) {
      $pDate = substr($pDate, 0, 4);
      $now = date('Y');
      
      $res = strval(intval($now) - intval($pDate));
      
      in_array($res[strlen($res) - 1], array('0', '1', '5', '6', '7', '8', '9')) ? $res .= ' lat' : $res .= ' lata';
      
      return $res;
  }

  
  // zwraca ograniczona liczbe slow z podanego tekstu
  function limitWords($pStr, $pCnt) {
      $tab = array();
      $res = '';
      $i = 0;

      $tab = explode(" ", $pStr);
      count($tab) > $pCnt ? $suffix = '...' : $suffix = '';
      $tab = array_slice($tab, 0, $pCnt);
      
      // usuniecie nadmiarowej kropki
      $tmp = end($tab);
      ($tmp[strlen($tmp) - 1] == '.' && $suffix == '...') ? $suffix = '..' : '';
      //
      
      $res = implode(" ", $tab).$suffix;
      
      return $res;
  }
  
  
  function createFilename($pFile, $pStr) {
    $tFrom = "\x61\x41\x63\x43\x65\x45\x6C\x4C\x6E\x4E\x6F\x4F\x73\x53\x7A\x5A\x7A\x5A";
    $tTo = "\xB1\xA1\xE6\xC6\xEA\xCA\xB3\xA3\xF1\xD1\xF3\xD3\xB6\xA6\xBC\xAC\xBF\xAF";

    $pStr = str_replace(" ", "_", $pStr);
    $pStr = str_replace(":", "", $pStr);
    $pStr = str_replace(",", "", $pStr);
    $pStr = str_replace("\"", "", $pStr);
    $pStr = str_replace("/", "-", $pStr);
    $pStr = strtr($pStr, $tTo, $tFrom);
    $pStr = strtolower($pStr);

    $tab = array();
    $tab = explode('.', $pFile);
    $ext = end($tab);

    $tab = explode("_", $pStr);

    if (count($tab) > 4) { // skrocenie nazwy pliku
        $tab = array_slice($tab, 0, 4);
        $pStr = implode("_", $tab);
    }

    return $pStr.'.'.$ext;
}

  function getImagesInfo($pItems) {
    $tab = array();

    foreach ($pItems as $key => $val) {
      $tmp_img = imagecreatefromjpeg($val['file']);
      $img_w= imagesx($tmp_img);
      $img_h= imagesy($tmp_img);
      imagedestroy($tmp_img);

      $tab[$key] = $val;
      $tab[$key]['width'] = $img_w + 15;
      $tab[$key]['height'] = $img_h + 15;
    }

    return $tab;
  }


  function getFilesInfo($pItems) {
    $tab = array();

    foreach ($pItems as $key => $val) {
      $tab[$key] = $val;
      $tab[$key]['size'] = sprintf("%.2f", filesize($val['file']) / 1024 / 1024).' MB';
    }
    
    return $tab;
  }


function loadModule($module, $task = '') {
	global $sqlconn, $modules, $html;
	//if(!in_array($module, $modules)) {
		//return 'You don\'t have persmissions to access this page.';
		//header("Location: index.php");
	//}
	//echo "sdf";
//echo _INCLUDE_MODULE;
	$module = _INCLUDE_MODULE.$module.'.php';
	//echo $module. '<br />';
	if(file_exists($module)) {
		return include($module);
	}
	else {
    return '<font color=red>Module <b>'.$module.'</b> doesn\'t exists.</font>';
		//header("Location: index.php");
		//return false;
	}
}


?>
