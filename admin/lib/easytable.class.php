<?

// easytable class 
// v.3.05
// joker@joker.art.pl
// la_mo_15.09.04

library_exists('etpl.inc.php');

class easytable {

	var $onmouse_rbc = 1;
	var $onmouse = '#f0f0ff';
	var $action_rbc = 1;
	var $action='';
	var $progresscolor = array( '#ffffff', '#f8f8f8', '#f0f0f0', '#e8e8e8', '#e0e0e0', '#e8e8e8', '#f8f8f8');
	var $tableclass = 'et';
	var $trclass = 'et_action';
	var $tdclass = '';

	var $opentable = 0;
	var $openrow = 0;

	var $bgcolor_once = false;
	var $class_once = false;
	var $action_once = false;
	var $res=false;

	var $wynik='';

	function setsimple() {
		$this->progresscolor=array();
		$this->onmouse='';
		$this->tableclass='';
		$this->tdclass='';
		$this->trclass='';
	}

	function easytable( $tableclass='', $trclass='', $tdclass='' ) {

		if ($tableclass!='') $this->tableclass=$tableclass;
		if ($trclass!='') $this->trclass=$trclass;
		if ($tdclass!='') $this->$tdclass=$tdclass;

	}

	function setdata($res) {
		$this->res=$res;
	}

	function setprogresscolor($arrcolor) {
		$this->progresscolor = $arrcolor;
	}

	function setonmouse($color) {
		$this->onmouse = $color;
	}

	function setonmouse_rbc($en) {
		$this->onmouse_rbc = $en;
	}

	function setclass_once($class) {
		$this->class_once = $class;
	}

	function setbgcolor_once($bgcolor) {
		$this->bgcolor_once = $bgcolor;
	}

	function setaction_once( $action ){
		$this->action_once = $action;
	}

	function todefault_once() {
		$this->bgcolor_once=false;
		$this->class_once=false;
		$this->action_once=false;
	}

	function opentable($add='') {
		if (count($this->progresscolor)>0) reset($this->progresscolor);
		$this->wynik.='<table';
		$this->wynik.=$this->tableclass!=''?" class=\"{$this->tableclass}\"":'';
		$this->wynik.=$add!=''?" {$add}":'';
		$this->wynik.='>';
		$this->opentable=1;
	}
	function row($add='') {
		$this->openrow($add);
	}
	function openrow($add='') {

		if ($this->opentable==0) $this->opentable();
		if ($this->openrow==1) $this->closerow();

		$this->wynik.='<tr';
		
		$this->wynik.=$add!=''?" {$add}":'';

		if ($this->bgcolor_once!=false) {
			$bgcolor=$this->bgcolor_once!=''?$this->bgcolor_once:'';
		} else {
			$bgcolor=count($this->progresscolor)>0?current($this->progresscolor):'';
		}
		if (!next($this->progresscolor)) reset($this->progresscolor);
		$this->wynik.=$bgcolor!=''?" bgcolor=\"{$bgcolor}\"":'';

		if ($this->onmouse_rbc==0&&($this->onmouse!=''&&$bgcolor!='')&&($this->onmouse!=$bgcolor)) {
			$this->wynik.=" onmouseover=\"this.bgColor='{$this->onmouse}'\" onmouseout=\"this.bgColor='{$bgcolor}'\"";
		}

		if ($this->action_once!=false) {
			$this->action=$this->action_once;
			$action=$this->action_rbc==0?$this->action:'';
		}
		$this->wynik.=$action!=''?" onclick=\"{$action}\"":'';

		if ($this->class_once!=false) {
			$this->wynik.=$this->class_once!=''?" class=\"{$this->class_once}\"":'';
		} else {
			$this->wynik.=$this->trclass!=''?" class=\"{$this->trclass}\"":'';
		}

		$this->wynik.='>';
		$this->openrow=1;
		$this->todefault_once();
	
	}

	function cell($content, $add='') {

		if ($this->openrow==0) $this->openrow();
		$this->wynik.='<td';
		$this->wynik.=$add!=''?" {$add}":'';

		if ($this->bgcolor_once!==false) {
			$bgcolor=$this->bgcolor_once!=''?$this->bgcolor_once:'';
		} 
		$this->wynik.=$bgcolor!=''?" bgcolor=\"{$bgcolor}\"":'';

		if ($this->onmouse_rbc==1&&($this->onmouse!=''&&$bgcolor!='')&&($this->onmouse!=$bgcolor)) {
			$this->wynik.=" onmouseover=\"this.bgColor='{$this->onmouse}'\" onmouseout=\"this.bgColor='{$bgcolor}'\"";
		}

		if ($this->action_once!==false) {
			$action=$this->action_once!=''?$this->action_once:'';
		} else {
			$action=$this->action!=''&&$this->action_rbc==1?$this->action:'';
		}
		$this->wynik.=$action!=''?" onclick=\"{$action}\"":'';

		if ($this->class_once!==false) {
			$this->wynik.=$this->class_once!=''?" class=\"{$this->class_once}\"":'';
		} else {
			$this->wynik.=$this->tdclass!=''?" class=\"{$this->tdclass}\"":'';
		}

		$this->wynik.='>'.($res!=false?etpl($res, $content):$content).'</td>';
		$this->todefault_once();
	}

	function closerow() {
		if ($this->openrow==1) {
			$this->wynik.='</tr>';
		}
		$this->openrow=0;
	}

	function closetable() {
		if ($this->opentable==1) {
			if ($this->openrow==1) {
				$this->wynik.='</tr>';
			}
			$this->wynik.='</table>';
		}
		$this->openrow=$this->opentable=0;
	}

	function show() {
		if ($this->opentable) $this->closetable();
		return $this->wynik;
	}

}


?>
