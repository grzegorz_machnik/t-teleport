<?php
// multiexec lib

class ax_me {
	var $operation=array();
	var $mename;
	var $id;
	var $backlink='';
	
	function ax_me($mename,$id='') {
		$this->mename=$mename;
		$this->id=$id;
	}

	function add_operation( $opname, $title, $param='', $type='') {
		$cnt = count($this->operation);
		$this->operation[$cnt]['op']=$opname;
		$this->operation[$cnt]['title']=$title;
		$this->operation[$cnt]['param']=$param!=''?$param:'null';
		$this->operation[$cnt]['type']=$type;
	}
	
	function set_backlink($file) {
		$this->backlink=$file;
	}
	
	function progress_html($opname) {
		$tu='
		<div style="position:relative; top:20px; left:140px;" id="progressbarek_'.$opname.'_txt">0%</div>
		<table id="progressbarek_'.$opname.'" style="background-color:#ffffff;width:300px;height:30px; border:1px solid black;">
		<tr><td><div id="progressbarek_'.$opname.'_percent" style="background-color:red; width:1px;"></div>
		</td><td></td>
		</tr>
		</table>
		';
		$tu='
		<table id="progressbarek_'.$opname.'" style="background-color:#ffffff; width:100%; height:30px; border:1px solid black;">
		<tr><td>
		<div id="progressbarek_'.$opname.'_percent" style="display:none; height:100%; background-color:red; width:1px; border:0px red solid;"><div style="position:relative; font-weight:bold; border:0px; top:1px; left:180px; display:none;" id="progressbarek_'.$opname.'_txt">0%</div>
		</div>
		</td>
		</tr>
		</table>
		';
		return $tu;
	}
	
	function show() {
		
		$tb=new easytable();
		$tb->opentable();
		$tb->row();
		$tb->cell("<b>{$this->mename}</b>",'colspan=3');
		$tb->row('style="font-weight:bold;"');
		$tb->cell('OPREACJA','height=20 width=200');
		$tb->cell( "-",'height=22 width=22 align=center valign=middle' );
		$tb->cell('Komunikat zwrotny','height=20 width=400');
		$jsop=array();
		foreach ($this->operation as $k=>$v) {
			$cnt = count($this->operation)-$k;
			$tb->row();
			$tb->cell($v['title'],'height=20');
			$tb->cell( "<img src=\"gfx/roz_ax_before.gif\" id=\"img_{$v['op']}_{$cnt}\">",'height=22 width=22 align=center valign=middle' );
			if ($v['type']=='progress') {
				$progress = $this->progress_html($v['op'].'_'.$cnt);
			}
			else $progress='';
			$tb->cell( "{$progress}<div id=\"reps_{$v['op']}_{$cnt}\"></div>");
		}
		foreach ($this->operation as $v) {
			$jsoparr[]=$v['op'];
			$jsparamarr[]=$v['param'];
		}
		$jsop=implode(',',$jsoparr);		
		$jsparam=implode(',',$jsparamarr);		
		$tb->row();
		$tb->cell( jform_submit('update_btn','update'," style=\"width:100px; display:block;\" onclick=\"javascript:multiexec('{$this->id}','{$jsop}','{$jsparam}', '{$this->backlink}')\""),'colspan=3 align=right');
		$tu=$tb->show();
		return $tu;
	
	}
	
}

?>
