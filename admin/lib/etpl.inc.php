<?

// etpl v.3.6
// joker@joker.art.pl
// la_mo_20050907


class etpl {

	var $tpl;
	var $blocks=array();
	var $items=array();
	var $repl=array();
	var $tpldir='';
	var $auto=1;

	function etpl($tpl, $tpldir='') {
		if ($tpldir!='') {
			$this->tpldir=$tpldir;
		} elseif ( defined('_TPL') && file_exists(_TPL.'/'.$tpl) ) {
			$this->tpldir=_TPL;
		}
		if ($this->tpldir!='') $tpl=$this->tpldir.'/'.$tpl;
		if (file_exists($tpl)) {
			$this->tpl=@join('',@file($tpl));
		} else $this->tpl=$tpl;
		
		$this->loadfiles();
		
		$this->items=etpl_item($this->tpl);

		$this->blocks=etpl_block($this->tpl, 1);
	}

	function loadfiles() {
		$pattern="(\%{2}LOADFILE ([\w\d\\.\_\- ]+)\%{2})";
		$ile=preg_match_all($pattern, $this->tpl, $reg,PREG_SET_ORDER);
		if ($ile>0) {
			for ($i=0; $i<$ile; $i++) {
				$filename=$this->tpldir!=''?$this->tpldir.'/'.$reg[$i][1]:$reg[$i][1];
				$filecontent=@join('',@file($filename));
				$this->tpl=ereg_replace($reg[$i][0], $filecontent, $this->tpl);
			}
		}
	}
	
	function set_autoblock() {
		$this->auto=1;
	}

	function set_item($name, $itemname, $value=array()) {
		$ok=1;
		$itemname=strtoupper($itemname);
		$value=array_change_key_case($value, CASE_UPPER);
		if (isset($this->items[$itemname]['notnull'])) {
			foreach ($this->items[$itemname]['notnull'] as $v) {
				if ($value[$v]=='') {
					$ok=0;
				}
			}
			if ($ok==0) {
				if ($this->items[$itemname]['else']!='') $replace=$this->items[$itemname]['else'];
			} else 
				$replace=$this->items[$itemname]['content'];
		} else $replace=$this->items[$itemname]['content'];

		$this->repl[strtoupper($name)].=etpl($value, $replace);
	}

	function set_manualblock() {
		$this->auto=0;
	}

	function add_item($name, $value='') {
		if (is_array($name)) {
			$this->repl+=$name;
		} else $this->repl[$name]=$value;
		$this->repl=array_change_key_case($this->repl, CASE_UPPER);
	}

	function add_block($name, $value) {
		$ok=1;
		$value=array_change_key_case($value, CASE_UPPER);
		if (isset($this->blocks[$name]['notnull'])) {
			foreach ($this->blocks[$name]['notnull'] as $v) {
				if ($value[$v]=='') {
					if ($this->blocks[$name]['else']!='') $this->blocks[$name]['content']=$this->blocks[$name]['else'];
					else $ok=0;
				}
			}
		}
		if ($ok==1) {
			$this->repl[strtoupper($name)].=etpl($value, $this->blocks[$name]['content']);
		}
	}
	
	function add($name, $value='') {
		$this->add_item($name, $value);
	}

	function show() {

		if ($this->auto==1) {
			if (count($this->blocks)>0) {
				foreach ($this->blocks as $k=>$v) {
					$this->add_block($k, $this->repl);
				}
			}
		}
		if (count($this->repl)>0) {
			$this->tpl=etpl($this->repl, $this->tpl);
		} else $this->tpl=etpl(array(), $this->tpl);

		return $this->tpl;
	}

}


function pre($variable) {
	echo '<pre>';
	print_r($variable);
	echo '</pre>';
}

function etpl($naco, $wczym) {

	$ile=preg_match_all("([%]{1}([\w-]+)[%]{1})", $wczym, $reg);
	$naco=array_change_key_case($naco, CASE_UPPER);
	for ($i=0; $i<$ile; $i++) {
		$wczym=ereg_replace("{$reg[0][$i]}","{$naco[$reg[1][$i]]}",$wczym);
	}	

	return $wczym;

}

function etpl_block(&$txt, $change=0) {

	$preg="([%]{2}BEGIN ([\w\d_\-]+)( IF \(([\w\|&\-_\d,]+)\))?[%]{2}([\s\S]+)[%]{2}END \\1[%]{2})";
	
	$cnt=preg_match_all($preg, $txt, $reg, PREG_SET_ORDER);
	$tu=array();

	if ($cnt>0) {
		foreach ($reg as $v) {
			$tu=$tu+etpl_block($v[4], 1);
			$tu[$v[1]]['content']=$v[4];
			if ($v[3]!='') {
				$pattern="(([\s\S]+)[%]{2}ELSE[%]{2}([\s\S]+))";
				$ile=preg_match_all($pattern, $v[4], $else, PREG_SET_ORDER);
				if ($ile>0) {
					$tu[$v[1]]['content']=$else[0][1];
					$tu[$v[1]]['else']=$else[0][2];
				}
				$tu[$v[1]]['notnull']=preg_split('/[\|&]/',$v[3],-1);
			}
		}
	}
	if ($change==1) {
		$txt=preg_replace($preg,'%\\1%',$txt);
	}

	return $tu;

}
function etpl_item(&$txt) {

	$preg="([%]{2}ITEM ([\w\d_\-]+)( IF \(([\w\|&\-_\d,]+)\))?[%]{2}([\s\S]+)[%]{2}END \\1[%]{2})";
	
	$cnt=preg_match_all($preg, $txt, $reg, PREG_SET_ORDER);
	$tu=array();

	if ($cnt>0) {
		foreach ($reg as $v) {
			$tu=$tu+etpl_block($v[4], 1);
			$tu[$v[1]]['content']=$v[4];
			if ($v[3]!='') {
				$pattern="(([\s\S]+)[%]{2}ELSE[%]{2}([\s\S]+))";
				$ile=preg_match_all($pattern, $v[4], $else, PREG_SET_ORDER);
				if ($ile>0) {
					$tu[$v[1]]['content']=$else[0][1];
					$tu[$v[1]]['else']=$else[0][2];
				}
				$tu[$v[1]]['notnull']=preg_split('/[\|&]/',$v[3],-1);
			}
		}
	}
	$txt=preg_replace($preg,'',$txt);

	return $tu;

}


function easyparse($co, $naco, $wczym) {
	return str_replace('%'.strtoupper($co).'%',$naco,$wczym);
}

function showfromtpl( $tpl , $content ) {

	if (file_exists($tpl)) {
		$tpl=file($tpl);
		if (count($tpl)>0) {
			$res=join('',$tpl);
			$res=etpl($content, $res);
		}
	}
	return $res;

}

?>
