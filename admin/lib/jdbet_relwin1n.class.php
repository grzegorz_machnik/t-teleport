<?

class jdbet_relwin1N {
	var $rname, $fieldshow, $fieldid, $sqlconn, $notnull, $val;
	var $rtable, $rid, $rshow, $rwhere, $rorder;
	var $limit, $allrows, $offset;
	
	function jdbet_relwin1N(&$sqlconn, $fieldid, $fieldshow, $name, $notnull, $setselect) {
		$this->fieldshow=$fieldshow;
		$this->fieldid=$fieldid;
		$this->rname=$name;
		$this->notnull=$notnull;
		$this->sqlconn=$sqlconn;
		$this->setselect=$setselect;
	}

	function makelink($arr=array()) {
		$tu='';
		if (count($arr)>0) { 
			foreach ($arr as $k=>$v) {
				$tu[]="{$k}={$v}";
			}
			$tu=implode('&amp;',$tu);
		}
		return $tu;
	}

	function op_showoffsetlink($offset, $limit, $allrows, $add=array()) {
		$lt=4;
		$offset_link='offs';
		if ($limit!==false) {
			$cnt=ceil($allrows/$limit);
			if ($offset>($cnt*$limit)) {
				$offset=(($cnt*$limit));
				$_SESSION[$offset_link]=$offset;
			}
			$tu=array();
			if ($cnt>1) {

				$link=$this->makelink($add + array("{$offset_link}"=>'0'));
				$tu[]="<a href=\"{$_SERVER['PHP_SELF']}?{$link}\" title=\"Pocz�tek listy\">&laquo;&laquo;</a>&nbsp;&nbsp;";
				if (($offset/$limit-$lt)>0) {
					$start=$offset/$limit-$lt+1;
					$tu[]="...";
				} else {
					$start=1;
				}
				if (($offset/$limit+$lt)>$cnt) {
					$stop=$cnt;
					$end=0;
				} else {
					$stop=$offset/$limit+$lt+1;
					$end=1;
				}

				for ($i=$start; $i<=$stop; $i++) {
					$link=$this->makelink($add+array("{$offset_link}"=>($i-1)*$limit));
					$style=$offset==($i-1)*$limit?' style="font-weight:bold; color:red; font-size:13px;"':'';
					$tu[]=" <a href=\"{$_SERVER['PHP_SELF']}?{$link}\"{$style}>{$i}</a> ";
				}
				
				if ($end==1) $tu[]='...';

				$link=$this->makelink($add+array("{$offset_link}"=>$cnt*$limit-$limit));
				$tu[]="&nbsp;&nbsp;<a href=\"{$_SERVER['PHP_SELF']}?{$link}\" title=\"Koniec listy\">&raquo;&raquo;</a>";
				
			}


			if (count($tu)>0) {
				$link=$this->makelink($add+array("{$offset_link}"=>$offset>0?$offset-$limit:0));
				$prev="<a href=\"{$_SERVER['PHP_SELF']}?{$link}\">&laquo; poprzednia strona</a>";
				$link=$this->makelink($add+array("{$offset_link}"=>$offset+$limit<$cnt*$limit?$offset+$limit:($cnt-1)*$limit));
				$next="<a href=\"{$_SERVER['PHP_SELF']}?{$link}\">nast�pna strona &raquo;</a>";
				$tmp = implode(' | ',$tu);
				$tu = "<table width=\"100%\" cellspacing=1 cellpadding=2 bgcolor=#e0e0e0><tr><Td bgcolor=#f0f0f0 width=150 align=left>{$prev}</td><Td bgcolor=#f0f0f0 align=center><b>Znaleziono {$allrows} wpis�w</b></td><Td bgcolor=#f0f0f0 width=150 align=right>{$next}</td></tr><tr><td bgcolor=#ffffff align=center colspan=3>{$tmp}</td></tr></table>";
				return $tu;
			} else return '';
			
		} return '';
	}
	
	function set_relation($rt, $rid, $rs, $rwhere='', $rorder='') {
		$this->rtable=$rt;
		$this->rid=$rid;
		$this->rshow=$rs;
		$this->rwhere=$rwhere;
		$this->rorder=$rorder;
	}

	function show() {
		$a=new etpl('relwin1n.tpl');
		$a->add('sitetitle',$this->rin_show);

		$f=$_REQUEST;

		$limit = 20;

		$sql = "from {$this->rtable}".($this->rwhere!=''?" where {$this->rwhere}":'').($this->rorder!=''?" order by {$this->rorder}":'');

		if ($f['offs']!='') $offs=$f['offs'];
		elseif ($this->setselect!='0') {
			$int = db_getsqltable($this->sqlconn, "select {$this->rid} {$sql}");
			$offs=array_search(array("{$this->rid}"=>$this->setselect), $int );
			if ($offs===false) $offs=0;
			else $offs = floor($offs/$limit)*$limit;
		} else $offs=0;
		$limit_str=" limit {$offs}, {$limit} ";
		
		$cnt = db_getsinglevalue($this->sqlconn,"select count(*) {$sql}");
		$sql = "select {$this->rid}, {$this->rshow} {$sql} {$limit_str}";
		$res = db_getbyindex($this->sqlconn, $sql);
		if ($this->notnull!='notnull') $res = array('0'=>' -- brak -- ') + ($res!==false?$res:array());
		if ($res!==false) {
			$data['fieldid']=$this->fieldid;
			$data['fieldshow']=$this->fieldshow;
			foreach ($res as $k=>$v) {
				$data['value']=$k;
				$data['showvalue']=$v;
				if ($this->setselect==$k) {
					$a->set_item('content','relrowselect',$data);
				} else {
					$a->set_item('content','relrow',$data);
				}
			}
		}
		$tu.=$this->op_showoffsetlink($offs, $limit, $cnt);
		$tu.=$a->show();
		$tu.=$this->op_showoffsetlink($offs, $limit, $cnt);
		
		return $tu;
	}
	
}


?>
