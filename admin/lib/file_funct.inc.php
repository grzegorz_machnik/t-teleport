<?php

function get_filelist($start, $pat='*', $ftype='dir') {
	$dir=array();
	if ($ftype=='all')
		$pliki = glob($start.'/'.$pat);
	else
		$pliki = glob($start.'/'.$pat, GLOB_ONLYDIR);
	if ($pliki!==false && count($pliki)>0) {
		foreach ($pliki as $k=>$v) {
			$dir[$v]['filetype']=filetype($v);
			if ($dir[$v]['filetype']=='dir' && basename($v)!='.' && basename($v)!='..') $dir[$v]['files']=get_filelist($v,$pat, $ftype);
			else $dir[$v]['filesize']=filesize($v);
		}
	}
	return $dir;
}

function safefname($fn) {

	$ext=substr( $fn, strrpos($fn,'.'));
	$filename=substr( $fn, 0, strrpos($fn,'.') );

	$pattern = array(
		'/�/'=>'e',
		'/�/'=>'o',
		'/�/'=>'a',
		'/�/'=>'s',
		'/�/'=>'l',
		'/[��]/'=>'z',
		'/�/'=>'c',
		'/�/'=>'n',
		'/[\.\!\@\#\$\%\^\&\*\(\)\'\"\\/;\:\,\ \`\[]/'=>'_'
	);

	return preg_replace( array_keys($pattern), array_values($pattern), $filename).$ext;
}

?>
