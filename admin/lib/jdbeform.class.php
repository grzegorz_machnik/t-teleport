<?

// jdbeform_v_1.40
// la_mo_2.11.4
// joker@joker.art.pl

library_exists('db_funct.inc.php');
library_exists('easytable.class.php');
library_exists('jform_funct.inc.php');

class jdbeform {

	// DB
	var $sqlconn;
	var $table;
	
	// JDBE - corespond.
	var $fielddescriptions=array();
	var $fields=array();
	var $idname;
	var $fu;
	var $lng;
	var $error=array();
	var $tpl=false;
	var $values=array();
	var $foto_details=array();
	var $outfiles=array();
	var $relation=array();
	
	// FORM vars
	var $action=false;
	var $method='post';
	var $enctype='multipart/form-data';
	
	function jdbeform( &$sqlconn, $table, $id='', $idname='id', $lng='' ) {
		global $lng_cfg;
		$this->idname=$idname;
		if ($lng=='')
			$this->lng=&$lng_cfg;
		else 
			$this->lng=&$lng;
		$this->sqlconn=$sqlconn;
		$this->table=$table;
		if ($id!='') $this->values=db_getsinglerow( $this->sqlconn, " select * from {$this->table} where {$this->idname}='{$id}' ");
	}
	
	function set_tpl($t) {
		$this->tpl=$t;
	}
	
	function import_fields( &$f ) {
		$this->fields=$f;
	}
	
	function import_relation( &$f ) {
		$this->relation=$f;
	}
	
	function import_fu( &$fu) {
		$this->fu=$fu;
	}
	
	function import_fotodetails(&$f) {
		$this->foto_details=$f;
	}
	
	function import_outfiles(&$f) {
		$this->outfiles=$f;
	}

	function import_fielddescriptions(&$d) {
		$this->fielddescriptions=$d;
	}

	function import_errors(&$d) {
		$this->error=$d;
	}
	
	function foto_form() {
		
		$tu.='<BR><fieldset style="width:98%"><legend>Zdjęcia:</legend>';
		
		if ( file_exists('mphoto.php') ) $mph=true; else $mph=false;
		
		if ($this->fu[$this->idname]!='') $idek=$this->fu[$this->idname];
		elseif ($this->fu['jdb_tmp_id']!='') {
			$idek=$this->fu['jdb_tmp_id'];
			$tu.=jform_hidden('jdb_tmp_id',$idek);
		} else {
			$idek="tmpid".uniqid(time());
			$this->fu['jdb_tmp_id']=$idek;
			$tu.=jform_hidden('jdb_tmp_id',$idek);
		}
		
		
		$exists = glob($this->foto_details['upload_dir'].$this->table.'_'.$idek.'_*.*');
		if ( $exists!==false && count($exists)>0) {

			foreach ($exists as $v) {
				$size=getimagesize($v);
				
				$tmp[$v]=" [{$size[0]}x{$size[1]}]<br><img src=".($mph===true?"mphoto.php?max={$this->foto_details['max']}&img={$v}":"{$v} width={$this->foto_details['max']}")." border=0 align=center>";
			}

			$tu.= jform_checkbox_multi('foto2del','',$tmp, $deli=$this->foto_details['cols'], 'valign=top');
			$tu.= jform_submit('del_foto_btn',$this->lng['del_selected']).'<br><br>';
		}
		if ( $exists===false || (count($exists)<$this->foto_details['limit']) || $this->foto_details['limit']==0) {
			$tu.= '<img src="gfx/upload_foto.gif" align=absmiddle border=0> &nbsp;';
			$tu.= jform_file('userfile','size=60');
			$tu.= jform_submit('add_foto_btn',$this->lng['upload']).'<br><br>';
		}
		
		$tu.='</fieldset><Br>';

		
		return $tu;


	}

	function ico($type) {
		switch ($type) {
			case 'relation':
				$tu='<img src="gfx/relation.png" align=absmiddle border=0>';
				break;
			case 'relwin1n':
				$tu='<img src="gfx/relation1.png" align=absmiddle border=0>';
				break;
			case 'combo':
			case 'counter':
				$tu='<img src="gfx/combo_field.gif" align=absmiddle border=0> ';
				break;
			default:
				$tu='<img src="gfx/edit_field.gif" align=absmiddle border=0> ';
				break;
		}
		return $tu;
	}

	function show() {
	
		$form=array();		
		foreach ($this->fields as $k=>$v) {
			$val = $this->values[$k]!='' ? $this->values[$k] : $this->fields[$k]['default'];
			if ($k!='op' && isset($this->fu[$k]) ) $val=$this->fu[$k];
			$len = $this->fields[$k]['len']>0 && $this->fields[$k]['len']<50 ? $this->fields[$k]['len'] : 50;
			
			if ($v['type']!='hidden') {
				$form["desc_{$k}"]=$this->fielddescriptions[$k]!=''?$this->fielddescriptions[$k]:ucfirst(str_replace('_',' ',$k));
				if ($this->error[$k]==1) $form["desc_{$k}"]="<b style=\"color:red;\">{$form["desc_{$k}"]}</b>";
			}

			switch ($v['type']) {
				case 'hidden':
					$form["jform_{$k}"] = jform_hidden( $k, $val );
					break;
				case 'htmlhidden':
					$form["jform_{$k}"] = jform_htmlhidden( $k, $val, $this->fields[$k]['foreign']===true ? $this->fields[$k]['data'][$val]:$val);
					break;
				case 'outfile':
					$form["jform_{$k}"] = jform_text( $k, $val," readonly size=80 id=\"{$k}\"")."<input type=\"button\" value=\"wybierz\" onclick=\"openDialogWin('{$this->outfiles[$k]}?var={$k}','outfile_{$k}')\">";
					break;
				case 'filebrowse':
					$form["jform_{$k}"] = jform_text( $k, $val," readonly size=40 id=\"{$k}\"")."<input type=\"button\" value=\"wybierz\" onclick=\"openDialogWinFB('{$this->outfiles[$k]}?var={$k}','outfile_{$k}')\"> <input type=\"button\" value=\"Wyczyść\" onclick=\"document.getElementById('{$k}').value='';\">";
					break;
				case 'password':
					$form["jform_{$k}"] = jform_password( $k, $val," readonly size=30 id=\"{$k}\"")."<input type=\"button\" value=\"zmień\" onclick=\"openDialogWin('jdb_chpass.php?dest={$k}','jdb_chpass_{$k}',300,150)\">";
					break;
				case 'passwordmd5':
					$form["jform_{$k}"] = jform_password( $k, $val," readonly size=30 id=\"{$k}\"")."<input type=\"button\" value=\"zmień\" onclick=\"openDialogWin('jdb_chpass.php?dest={$k}&md5=y','jdb_chpass_{$k}',300,150)\">";
					break;
				case 'relwin1n':
					if ($this->fu[$this->idname]!='' || ($val!='' && $val!='0')) {
						$vall = db_getsinglerow($this->sqlconn, "select {$v['relation']['rid']} as id, {$v['relation']['rs']} as showfield from {$v['relation']['rtable']} where {$v['relation']['rid']}='{$val}'");
						if ($vall===false && $v['relation']['notnull']=='null')
							$vall=array('id'=>'0','showfield'=>' -- brak -- '); 
					} elseif ($v['relation']['notnull']!='null') {
						$vall = db_getsinglerow($this->sqlconn, "select {$v['relation']['rid']} as id, {$v['relation']['rs']} as showfield from {$v['relation']['rtable']} ".($v['relation']['rwhere']!=''?"where {$v['relation']['rwhere']}":'').($v['relation']['rorder']!=''?" order by {$v['relation']['rorder']}":'')." limit 1");
					} else $vall=array('id'=>'0','showfield'=>' -- brak -- ');
					$v['relation']['rs'] = rawurlencode($v['relation']['rs']);
					$v['relation']['rid'] = rawurlencode($v['relation']['rid']);
					$v['relation']['rwhere'] = rawurlencode($v['relation']['rwhere']);
					$v['relation']['rorder'] = rawurlencode($v['relation']['rorder']);
					$form["jform_{$k}"] = jform_hidden($k, $vall['id']).jform_text( "{$k}_relwinshow", $vall['showfield'], " readonly class=\"noedit\" style=\"width:370px;\"").jform_input('button','relwin1n_btn','Wybierz',"onclick=\"openDialogWin('relwin1n.php?op=rel&fid={$k}&fs={$k}_relwinshow&rtable={$v['relation']['rtable']}&rid={$v['relation']['rid']}&rs={$v['relation']['rs']}&name=wybierz&val='+{$k}.value+'&notnull={$v['relation']['notnull']}&rwhere={$v['relation']['rwhere']}&rorder={$v['relation']['rorder']}','relwin');\""); 
					break;
				case 'combo':
				case 'select':
					$form["jform_{$k}"] = jform_combo( $k, $this->fields[$k]['data'], $val); 
					break;
				case 'counter':
					$form["jform_{$k}"] = jform_combo_counter($k, $val, $this->fields[$k]['data'][0], $this->fields[$k]['data'][1],$this->fields[$k]['data'][2] );
					break;
				case 'text':
				case 'textarea':
					$form["jform_{$k}"] = jform_textarea( $k, $val, '', true).(($_SESSION['setadmin']=='on')?'<span onclick="return fru_lorem(\''.$k.'\');">Lorem</span>':'');
					break;
				case 'htmlarea':
					$form["jform_{$k}"] = jform_htmlarea( $k, $val, 590, 300, 'Default').(($_SESSION['setadmin']=='on')?'<span onclick="return fru_lorem(\''.$k.'\');">Lorem</span>':'');
					break;
				case 'enum':
					$form["jform_{$k}"] = jform_radio( $k, $this->fields[$k]['data'], $val );
					break;
				case 'checbox_switch':
				case 'ajax_switch':
				case 'switch':
					$form["jform_{$k}"] = jform_checkbox_switch( $k, $val, $this->fields[$k]['data'] );
					break;
				case 'checkboxmulti':
				case 'multicheckbox':
					$form["jform_{$k}"] = jform_checkbox_multi( $k, $val, $this->fields[$k]['data'], 3 );
					break;
				case 'checkbox':
					$form["jform_{$k}"] = jform_checkbox( $k, $val );
					break;
				case 'date':
					$form["jform_{$k}"] = jform_date($k, ($val=='0000-00-00' || $val=='') ? date('Y-m-d') : $val );
					break;
				case 'newdate':
					$form["jform_{$k}"] = jform_newdate($k, ($val=='0000-00-00' || $val=='') ? date('Y-m-d') : $val );
					break;
				case 'html':
					$form["jform_{$k}"] = etpl($this->fields, $val);
					break;
				case 'integer':
				case 'int':
				case 'varchar':
				case 'char':
				default:
					$form["jform_{$k}"]= jform_text($k,htmlspecialchars($val), "size=\"{$len}\"");
			}

		}
		if ( $this->tpl !== false ) {
			$a=new etpl($this->tpl);
			$a->add_item($form);
			if (count($this->foto_details)>0) {
				$a->add_item('foto_form',$this->foto_form());
			}

			$tu.=$a->show();
			
		} else {

			$tb=new easytable('et','et_noneaction','et');
			$tb->opentable();
			$hiddens='';
			foreach ($this->fields as $k=>$v) {
			
				if ($v['type']=='hidden') {
				
					$hiddens.=$form["jform_{$k}"];
					
				} else {
				
					if ( strpos( 'htmlarea, textarea',$v['type'])!==false ) {	
						$tb->row();
						$tb->cell(  $this->ico($v['type']).$form["desc_{$k}"].'<Br>'.$form["jform_{$k}"], 'colspan=2');
					} elseif(strpos( 'relwin1n',$v['type'])!==false) {
						$tb->row();
						$tb->cell( $this->ico($v['type']).$form["desc_{$k}"] ,'valign=top' );
						$tb->cell( $form["jform_{$k}"] ,'valign=top');
					} else {
						$tb->row();
						$tb->cell( $this->ico($v['type']).$form["desc_{$k}"] ,'valign=top');
						$tb->cell( $form["jform_{$k}"] ,'valign=top');
					}
					
				}
				
			}
			
			//relacje
			if (count($this->relation)>0) {
				foreach ($this->relation as $k=>$v) {
					$res=false;
					if ($this->fu[$this->idname]!='') {
						$field = $this->sqlconn->fields($v['rtable']);

						$ids = db_getbyindex($this->sqlconn, "select {$v['rout']}, {$v['rout']} from {$v['rtable']} where {$v['rin']}='{$this->fu[$this->idname]}' ".( isset($field['jdb_orderkey'])?" order by jdb_orderkey":'') );
						if ($ids!==false) {
							$ids_txt = implode('\',\'',$ids);
							$where_add = ($v['rout_where']!=''?" ({$v['rout_where']}) and ":'');
							
							$res = db_getbyindex($this->sqlconn, "select {$v['rout_id']}, {$v['rout_sqlshow']} from {$v['rout_table']} where {$where_add} {$v['rout_id']} in ('{$ids_txt}')");
							
						} else $res=false;
					}

					$options='';
					$ids_contener='';

					if ($res!==false) {
						$ids_contener = implode(',',array_keys($res) );
						
						if ( isset($field) ) {
							foreach ($ids as $oid) {
								$oval = $res[$oid];
								$options.="<option id=\"{$k}_cs_{$oid}\">{$oval}</option>";
							}
						} else {
							foreach ($res as $oid=>$oval) {
								$options.="<option id=\"{$k}_cs_{$oid}\">{$oval}</option>";
							}
						}

					}

					$tb->row();
					
					$desc=$this->fielddescriptions[$k]!=''?$this->fielddescriptions[$k]:ucfirst(str_replace('_',' ',$k));
					
					$tb->cell($this->ico('relation').$desc);
					foreach ($v as $in=>$out) {
						$v[$in]=rawurlencode($out);
					}
					
					$tb->cell(
						"<select id=\"{$k}_show\" size=5 style=\"width:300px;\">{$options}</select>".
						jform_hidden("{$k}_contener","{$ids_contener}")
//						.( isset($field['jdb_orderkey']) ? "<img src=\"gfx/up.gif\" border=0 style=\"cursor:pointer;\" onclick=\"javascript: chpos('{$k}',-1)\"><img src=\"gfx/down.gif\" border=0 style=\"cursor:pointer;\" onclick=\"javascript: chpos('{$k}',1)\">":'')
						.jform_input('button',$k.'_btnNW','Wybierz',
						"onclick=\"openDialogWin('relwinnm.php?contener={$k}&rout_table={$v['rout_table']}&rout_id={$v['rout_id']}&rout_sqlshow={$v['rout_sqlshow']}&rout_where={$v['rout_where']}&rout_order={$v['rout_order']}','{$k}_relwin');\"")
//						.($this->fu['jdb_tmp_id']==''?jform_input('button', $k.'_btnNWord', 'Ustal kolejność', " onclick=\"openDialogWin('relwinnmsort.php?contener={$k}&rowid={$this->fu[$this->idname]}&rtable={$v['rtable']}&rout={$v['rout']}&rin={$v['rin']}&rout_table={$v['rout_table']}&rout_id={$v['rout_id']}&rout_sqlshow={$v['rout_sqlshow']}&rout_where={$v['rout_where']}&rout_order={$v['rout_order']}','{$k}_sortwin');\" "):'')
					 );
				}
			}
			
			//koniec relacje

			$tu=jform_open($this->action !== false ? $this->action : $_SERVER['PHP_SELF'] );
			$tu.=jform_htmlarea_script();
			$tu.=$tb->show();
			if (count($this->foto_details)>0) {
				$tu.=$this->foto_form();
			}

			$tu.=$hiddens;
			if ( $this->fu['jdb_tmp_id']!='' ) {
				$addlink='?'.sqs("jdb_tmp_id={$this->fu['jdb_tmp_id']}&op=canceldel");
			} else $addlink='';

			$tu.=jform_redirect('redir',$this->lng['cancel'],$this->action !== false ?$this->action.$addlink : $_SERVER['PHP_SELF'].$addlink );
      
      if(defined('JFORM_CONFIRM') && $_GET['op']=='add') $confirm =  " onClick=\"return confirm('".JFORM_CONFIRM."');\"";
			
      $tu.='&nbsp;&nbsp;&nbsp;&nbsp;'.jform_submit('ok','ok',' style="width:150px;" '.$confirm);

			
			
			$tu.=jform_close();
			
		}
		
		return $tu;
	}

}


?>
