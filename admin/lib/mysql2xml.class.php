<?php
class mysql2xml {
	var $table;
	var $sqlconn;
	var $limit;
	var $offs;
	var $table_details=array();
	var $orderby=array();
	var $toshow=array();
	
	function mysql2xml(&$sqlconn, $table) {
		$this->sqlconn=$sqlconn;
		$this->table=$table;
		$this->table_details = $this->sqlconn->fields($this->table);
	}
	
	function table_cnt($f) {
		$cnt = db_getsinglevalue($this->sqlconn, "select count(*) from {$this->table}");
		return $cnt;
	}
	
	function table_details($f) {
		$xml = new XML();
		$root = $xml->createElement('table_details');
		$root->setAttribute('name',$this->table);
		$xml->appendChild($root);

		foreach	($this->table_details as $k=>$v) {
			$item[$k] = $xml->createElement('field');
			$item[$k]->setAttribute('name',$k);
			foreach ($v as $vk=>$vv) {
				if (!is_array($vv)) {
					$item[$k.'_'.$vk]=$xml->createElement($vk);
					$item[$k.'_'.$vk]->appendChild( $xml->createTextNode($vv) );
					$item[$k]->appendChild($item[$k.'_'.$vk]);
				} else {
					$item[$k.'_data']=$xml->createElement('data');
					foreach ($vv as $vvk=>$vvv) {
						$item[$k.'_'.$vk.'_'.$vvv]=$xml->createElement('data_item');
						$item[$k.'_'.$vk.'_'.$vvv]->setAttribute('value',$vvv);
						$item[$k.'_data']->appendChild($item[$k.'_'.$vk.'_'.$vvv]);
					}
					$item[$k]->appendChild($item[$k.'_data']);
				}
			}
			
			$root->appendChild($item[$k]);
		}
		
		echo $xml->toString(1);
		
		
	}
	
	function table_list($f) {
		if ($f['limit']!='') $this->limit=$f['limit'];
		if ($f['offs']!='') $this->offs=$f['offs'];
		if ($f['orderby']!='') $this->orderby=split('[|,]',$f['orderby']);
		if ($f['toshow']!='') {
			$this->toshow=split('[|,]',$f['toshow']);
		} else {
			$this->toshow=array_keys($this->table_details);
		}
		
		$sql = "select ".(!in_array('id',$this->toshow) && $this->toshow[0]!='*' ? 'id,' : '').implode(',',$this->toshow)." from {$this->table} ".
			(count($this->orderby)>0?" order by ".implode(',',$this->orderby):'')." ".
			($this->limit!='' || $this->offs!='' ? "limit ".($this->offs!=''?"{$this->offs},":'')."{$this->limit}" : '');
			
			
		$res = db_getsqltable($this->sqlconn, $sql);
		if ($res!==false) {
			$xml=new XML();
			
			$root=$xml->createElement('table_data');
			$root->setAttribute('name',$this->table);

			foreach ($res as $k=>$v) {
				$row[$k]=$xml->createElement('row');
				$row[$k]->setAttribute('id',$v['id']);
				foreach ($this->toshow as $field) {
					$row[$k.'_'.$field]=$xml->createElement($field);
					if (in_array($this->table_details[$field]['type'], array('text','varchar','char'))) {
						$row[$k.'_'.$field]->appendChild( $xml->createCDATAsection($v[$field]) );
					} else {
						$row[$k.'_'.$field]->appendChild( $xml->createTextNode($v[$field]) );
					}
					$row[$k]->appendChild($row[$k.'_'.$field]);
				}
				$root->appendChild($row[$k]);
			}			
			$xml->appendChild($root);
			
			$tu=$xml->toString(1);
		}
		return $tu;
		
	}
	
	function show($f) {
		if (count($f)>0) {
			switch ($f['op']) {
				case 'tdetails':
					$tu=$this->table_details($f);
					break;
				case 'cnt':
					$tu=$this->table_cnt($f);
					break;
				case 'show':
				default:
					$tu=$this->table_list($f);
					break;
			}		
		} else $tu='no op';
		return $tu;
	}

}

?>
