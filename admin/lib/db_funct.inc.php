<?
/*
*
*	db_funct v2.9
*	joker@joker.art.pl
*
*
*/

//library_exists('sql.class.php');

function array2simple($tablica, $index, $unique=1) {
	$tmp=array();
	if (count($tablica)>0) {
		foreach ($tablica as $k=>$v) {
			$tmp[]=$v[$index];
		}
		if ($unique==1) $tmp=array_unique($tmp);
	}
	return $tmp;
}

function db_trace_parent($table, $val, $id, $parent) {
	global $sqlconn;

	$tu=array();

	$res = db_getsinglerow($sqlconn, "select * from {$table} where {$id}='{$val}'");
	if ($res[$parent]!='' && $res[$parent]!='0') {
		$tu = array_merge($tu, db_trace_parent($table, $res[$parent], $id, $parent));
	};
	$tu[]=$res;
	return $tu;
	
}

function db_trace_relation($val, $id1, $table,$id2) {
	global $sqlconn;
	$sql = "select {$id2}, {$id2} from {$table} where {$id1}='{$val}'";
	$res = db_getbyindex($sqlconn, $sql); 
	return $res;
}

function db_insertrow(&$sqlconn, $table, $row, $op='insert', $retid = false) {

	$fields=$sqlconn->fields($table);

	foreach ($fields as $k=>$v) {
		
		if ($op=='update') {
			if (eregi('pri',$v['key'])) $primary[]=$k;
		}

		if (!eregi('auto_increment',$v['extra']) && !eregi('pri',$v['key']) && array_key_exists($k, $row) ) {
			$field[]=$k;
			$values[]=!isset($row[$k])?"null":" '{$row[$k]}' ";
		}
		
	}

	if ($op=='insert') {
		$sql=" insert into {$table} (".join(',',$field).") values (".join(',',$values).")";
	}	elseif ($op=='update') {
		
		$sql=" update {$table} set ";
		
		foreach ($field as $k=>$v) {
			$sql_item[] = "{$v} = {$values[$k]}";
		}
		$sql.= @join(',',$sql_item);
		unset($sql_item);
		
		foreach ($primary as $k=>$v) {
			$sql_item[]="{$v} = '{$row[$v]}'";
		}
		
		$sql.=' where '.@join(',',$sql_item);
		
	}
	if ($sqlconn->query($sql)) {
		if ($retid===true) {
			$sqlconn->lastID();
		} else return true;
	} else {
		return false;
	}

}

function db_getmultibyindex(&$sqlconn, $sql) {
	$tmp=array();
	if ($sqlconn->query( $sql )) {
		$nr=$sqlconn->numRows();
		for ($i=0; $i<$nr; $i++) {
			$row=$sqlconn->fetchRowAssocc();
			reset($row);
			$tmp[current($row)]=$row;
		}
	}
	return count($tmp)>0?$tmp:false;
}


function db_getbyindex( &$sqlconn, $sql ) {
	$tmp=array();
	if ($sqlconn->query( $sql )) {
		$nr=$sqlconn->numRows();
		for ($i=0; $i<$nr; $i++) {
			list($by, $val)=$sqlconn->fetchRow();
			$tmp[$by]=$val;
		}
	}
	
	return count($tmp)>0?$tmp:false;
}

function db_getsinglerow( &$sqlconn, $sql ) {
	if ( $sqlconn->query($sql) ) {
		if ($sqlconn->numRows()>0) return $sqlconn->fetchRowassocc();
		else return false;
	}
}

function db_getsinglevalue(&$sqlconn, $sql) {
	if ( $sqlconn->query( $sql ) ) {
		if ($sqlconn->numRows()>0) {
			list($val)=$sqlconn->fetchRow();
			return $val;
		}
		else return false;
	}
}

function db_getsqltable(&$sqlconn, $sql) {
	$tmp=array();
	if ($sqlconn->query($sql)) {
		$nr=$sqlconn->numRows();
		if ($nr>0) {
			for ($i=0; $i<$nr; $i++) {
				$tmp[]=$sqlconn->fetchRowassocc();
			}
		}
	}
	return count($tmp)>0?$tmp:false;
}

	
function db_table_exists(&$sqlconn, $tn) {
	$sql = "show tables;";
	$res = db_getmultibyindex($sqlconn, $sql);
	if (isset($res[$tn])) return true; else return false;
}



?>
