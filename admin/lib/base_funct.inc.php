<?php
// base funct
// v.0.1
// joker@joker.art.pl
// la_mo_2005.9.24

function jdb_error_box($title, $msg, $type='error') {
	if ($type=='error') $bgcolor='#b00101'; else $bgcolor='#0101b0';
			$backfile=debug_backtrace();

	echo '
<table style="border:1px solid black; padding:0px; background-color:#a0a0a0; width:500px;">
<tr><td style="background-color:'.$bgcolor.'; font-family:tahoma; font-size:9px; font-weight:bold; padding:4px; color:white;" align="left">'.
$title.'<br><span style="font-weight:normal;">[linia '.$backfile[0]['line'].': '.$backfile[0]['file'].']</span>
</td></tr>
<tr><td align="left" style="background-color:#e0e0e0; color:#000000; font-family:tahoma; font-size:9px; padding:4px; font-weight:bold;">
'.$msg.'
</td></tr>
</table><br>
';
}

function myErrorHandler($errno, $errstr, $errfile, $errline) {
    $errortype = array (
                E_ERROR           => "Error",
               // E_WARNING         => "Warning",
                E_PARSE           => "Parsing Error",
                //E_NOTICE          => "Notice",
                E_CORE_ERROR      => "Core Error",
                E_CORE_WARNING    => "Core Warning",
                E_COMPILE_ERROR   => "Compile Error",
                E_COMPILE_WARNING => "Compile Warning",
                E_USER_ERROR      => "User Error",
                E_USER_WARNING    => "User Warning",
                E_USER_NOTICE     => "User Notice",
//                 E_STRICT          => "Runtime Notice"
                );
                //pre($errortype);
                //echo $errno;
                // echo $errortype[$errno];
	if ( ($errno!=8 || (defined('_REPORTING_NOTICE') && _REPORTING_NOTICE=='on' )) && $errortype[$errno] !='' )
		jdb_error_box('B��D! ['.$errortype[$errno].']<br><span style="font-weight:normal">[linia: '.$errline.': '.$errfile.']</span>', $errstr, $errno==8 || $errno==1024 ? 'info':'error');
}

if (_WORKTYPE=='test') {
	error_reporting(E_ALL & ~E_NOTICE);
	set_error_handler("myErrorHandler");
} else 
	error_reporting(0);

function library_exists($file, $load=1, $stop=1) {
	if ( in_array($file, get_included_files()) ) 
		return true;
	elseif ($load==1) {
		if (file_exists($file)) {
			include_once($file);
			return true;
		} elseif (file_exists(_INCLUDE_PATH.$file)) {
			include_once(_INCLUDE_PATH.$file);
			return true;
		} else
			$backfile=debug_backtrace();
			$cnt=count($backfile);
			jdb_error_box('BRAK BIBLIOTEKI [linia '.$backfile[0]['line'].': '.$backfile[0]['file'].' w funkcji '.$backfile[0]['function'].']',"Brak biblioteki <b style=\"color:red\">{$file}</b> lub nieprawid�owa �cie�ka do pliku");
			if ($stop==1) die();
			return false;
	} else {
		if ($stop==1) die();
		return false;
	}
}



?>
