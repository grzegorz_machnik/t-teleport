<?php

class collapseGroup {
	var $tpl, $default, $data;
	
	function collapseGroup($en, $on='gfx/down.gif', $off='gfx/right.gif', $start='on') {
		if (isset($_COOKIE[$en])) $start=$_COOKIE[$en];
		$this->data['imgon']=$on;
		$this->data['width']='200px;';
		$this->data['imgoff']=$off;
		$this->data['elementname']=$en;
		$this->data['tpl']='collapsegroup.tpl';
		$this->data['classname']='collapsegroup';
		$this->data['groupon']=($start=='on'?'block':'none');
		$this->data['startimage']=($start=='on'?$this->data['imgon']:$this->data['imgoff']);
	}
	
	function set($name, $value) {
		$this->data[$name]=$value;
	}
	
	function show() {
		$a=new etpl($this->data['tpl']);
		$a->add($this->data);
		return $a->show();
	}
	
}


?>
