<?php /* Smarty version 2.6.16, created on 2012-11-12 16:07:36
         compiled from pl/index.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Regnerówka</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta name="Robots" content="all" />
<script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
<script type="text/javascript" src="js/moje.js"></script>
<meta name="Revisit-after" content="14 days" />
<link href='http://fonts.googleapis.com/css?family=Italianno&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</script>
</head>
<body>
<?php echo '
<script language="JavaScript" type="text/javascript">
<!--
// PLAYER VARIABLES

var mp3snd = "muzyka.mp3";
var bkcolor = "000000";

if ( navigator.userAgent.toLowerCase().indexOf( "msie" ) != -1 ) {
document.write(\'<bgsound src="\'+mp3snd+\'" loop="1">\');
}
else if ( navigator.userAgent.toLowerCase().indexOf( "firefox" ) != -1 ) {
document.write(\'<object data="\'+mp3snd+\'" type="application/x-mplayer2" width="0" height="0">\');
document.write(\'<param name="filename" value="\'+mp3snd+\'">\');
document.write(\'<param name="autostart" value="1">\');
document.write(\'</object>\');
}
else {
document.write(\'<audio src="\'+mp3snd+\'" autoplay="autoplay" >\');
document.write(\'<object data="\'+mp3snd+\'" type="application/x-mplayer2" width="0" height="0">\');
document.write(\'<param name="filename" value="\'+mp3snd+\'">\');
document.write(\'<param name="autostart" value="1">\');
document.write(\'<embed height="2" width="2" src="\'+mp3snd+\'" pluginspage="http://www.apple.com/quicktime/download/" type="video/quicktime" controller="false" controls="false" autoplay="true" autostart="true" loop="true" bgcolor="#\'+bkcolor+\'"><br>\');
document.write(\'</embed></object>\');
document.write(\'</audio>\');
}
//-->
</script>

'; ?>



<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
		<script src="<?php echo $this->_tpl_vars['_DOMAIN']; ?>
js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
		
		<?php echo '
		 <script type="text/javascript" charset="utf-8">
			$(document).ready(function(){				
				
				$("a[rel^=\'prettyPhoto\']").prettyPhoto({social_tools:\'\',animation_speed:\'normal\',theme:\'light_rounded\',overlay_gallery:true, konie:false});				
			});
			</script>
		
		'; ?>

<div class="header">
  <div class="inner"><a href="<?php echo $this->_tpl_vars['_DOMAIN']; ?>
" class="home"></a></div>
</div>
<div class="content">
  <div class="inner linia">
    <div class="menu_top"> 
    <?php unset($this->_sections['m']);
$this->_sections['m']['name'] = 'm';
$this->_sections['m']['loop'] = is_array($_loop=$this->_tpl_vars['menu']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['m']['show'] = true;
$this->_sections['m']['max'] = $this->_sections['m']['loop'];
$this->_sections['m']['step'] = 1;
$this->_sections['m']['start'] = $this->_sections['m']['step'] > 0 ? 0 : $this->_sections['m']['loop']-1;
if ($this->_sections['m']['show']) {
    $this->_sections['m']['total'] = $this->_sections['m']['loop'];
    if ($this->_sections['m']['total'] == 0)
        $this->_sections['m']['show'] = false;
} else
    $this->_sections['m']['total'] = 0;
if ($this->_sections['m']['show']):

            for ($this->_sections['m']['index'] = $this->_sections['m']['start'], $this->_sections['m']['iteration'] = 1;
                 $this->_sections['m']['iteration'] <= $this->_sections['m']['total'];
                 $this->_sections['m']['index'] += $this->_sections['m']['step'], $this->_sections['m']['iteration']++):
$this->_sections['m']['rownum'] = $this->_sections['m']['iteration'];
$this->_sections['m']['index_prev'] = $this->_sections['m']['index'] - $this->_sections['m']['step'];
$this->_sections['m']['index_next'] = $this->_sections['m']['index'] + $this->_sections['m']['step'];
$this->_sections['m']['first']      = ($this->_sections['m']['iteration'] == 1);
$this->_sections['m']['last']       = ($this->_sections['m']['iteration'] == $this->_sections['m']['total']);
?>
    <a href="<?php echo $this->_tpl_vars['_DOMAIN'];  echo $this->_tpl_vars['menu'][$this->_sections['m']['index']]['url']; ?>
.html" <?php if ($this->_tpl_vars['menu'][$this->_sections['m']['index']]['url'] == $_GET['site']): ?>style="text-decoration:underline;"<?php endif; ?>><?php echo $this->_tpl_vars['menu'][$this->_sections['m']['index']]['nazwa']; ?>
</a> 
    <?php endfor; endif; ?>
    </div>
    <?php echo $this->_tpl_vars['content']; ?>

       
    <?php echo $this->_tpl_vars['footer']; ?>

    
  </div> 
  </div>
</div>
<div class="footer">
  <div class="inner">
  <div class="fr">Projekt i realizacja <a href="http://www.net-line.com.pl" target="_blank">Net-Line Warszawa</a></div>
  </div>
</div>
</body>
</html>