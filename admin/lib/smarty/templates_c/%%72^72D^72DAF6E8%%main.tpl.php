<?php /* Smarty version 2.6.16, created on 2015-01-04 12:02:18
         compiled from main.tpl */ ?>
<?php echo '
 <script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			
      $(".fancybox").fancybox({
        helpers: {
            media: {}
        }
      });
      
  });
  
</script>      
'; ?>

<div class="boxy">  	
        <div class="group">    
          
          <div class="grid">      
            
            <?php if ($this->_tpl_vars['banners']['0']['link'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners']['0']['link']; ?>
" class="box">
            <?php elseif ($this->_tpl_vars['banners']['0']['yt'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners']['0']['yt']; ?>
" class="box fancybox iframe">   
            <?php else: ?>
            <a href="#" class="box">   
            <?php endif; ?>   
              
              <?php if ($this->_tpl_vars['banners']['0']['plik'] != ''): ?>
               <div class="foto" style="background-image: url(<?php echo $this->_tpl_vars['banners']['0']['plik']; ?>
);">
              </div> 
              <?php endif; ?>            
              <span class="label <?php if ($this->_tpl_vars['banners']['0']['plik'] == ''): ?>bezfoto<?php endif; ?>"><?php echo $this->_tpl_vars['banners']['0']['nazwa']; ?>

              </span> </a>  	
          </div>
              
          <div class="grid">      
            
            <?php if ($this->_tpl_vars['banners']['2']['link'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners']['2']['link']; ?>
" class="box">
            <?php elseif ($this->_tpl_vars['banners']['2']['yt'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners']['2']['yt']; ?>
" class="box fancybox iframe">   
            <?php else: ?>
            <a href="#" class="box">   
            <?php endif; ?>   
              
              <?php if ($this->_tpl_vars['banners']['2']['plik'] != ''): ?>
               <div class="foto" style="background-image: url(<?php echo $this->_tpl_vars['banners']['2']['plik']; ?>
);">
              </div> 
              <?php endif; ?>            
              <span class="label <?php if ($this->_tpl_vars['banners']['2']['plik'] == ''): ?>bezfoto<?php endif; ?>"><?php echo $this->_tpl_vars['banners']['2']['nazwa']; ?>

              </span> </a>  	
          </div> 
             
        </div>    
        <div class="grid_big">      
          <div class="box box_black">        
            <video class="movie" loop="loop"  id="video" autoplay="autoplay" controls="controls">        
              <source src="images/movie_1.mp4" type="video/mp4">  		
                <source src="images/movie_1.webm" type="video/webm">  			  			 I'm sorry; your browser doesn't support HTML5 video in WebM with VP8 or MP4 with H.264.   			
                  <!-- You can embed a Flash player here, to play your mp4 video in older browsers --> 		
            </video>                 
          </div>    
        </div>    
        <div class="group">    
          
          <div class="grid">      
            
            <?php if ($this->_tpl_vars['banners']['1']['link'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners']['1']['link']; ?>
" class="box">
            <?php elseif ($this->_tpl_vars['banners']['1']['yt'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners']['1']['yt']; ?>
" class="box fancybox iframe">   
            <?php else: ?>
            <a href="#" class="box">   
            <?php endif; ?>   
              
              <?php if ($this->_tpl_vars['banners']['1']['plik'] != ''): ?>
               <div class="foto" style="background-image: url(<?php echo $this->_tpl_vars['banners']['1']['plik']; ?>
);">
              </div> 
              <?php endif; ?>            
              <span class="label <?php if ($this->_tpl_vars['banners']['1']['plik'] == ''): ?>bezfoto<?php endif; ?>"><?php echo $this->_tpl_vars['banners']['1']['nazwa']; ?>

              </span> </a>  	
          </div>  
          
          <div class="grid">      
            
            <?php if ($this->_tpl_vars['banners']['3']['link'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners']['3']['link']; ?>
" class="box">
            <?php elseif ($this->_tpl_vars['banners']['3']['yt'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners']['3']['yt']; ?>
" class="box fancybox iframe">   
            <?php else: ?>
            <a href="#" class="box">   
            <?php endif; ?>   
              
              <?php if ($this->_tpl_vars['banners']['3']['plik'] != ''): ?>
               <div class="foto" style="background-image: url(<?php echo $this->_tpl_vars['banners']['3']['plik']; ?>
);">
              </div> 
              <?php endif; ?>            
              <span class="label <?php if ($this->_tpl_vars['banners']['3']['plik'] == ''): ?>bezfoto<?php endif; ?>"><?php echo $this->_tpl_vars['banners']['3']['nazwa']; ?>

              </span> </a>  	
          </div>
             
        </div>    
        
        <?php unset($this->_sections['b']);
$this->_sections['b']['name'] = 'b';
$this->_sections['b']['loop'] = is_array($_loop=$this->_tpl_vars['banners']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['b']['start'] = (int)4;
$this->_sections['b']['show'] = true;
$this->_sections['b']['max'] = $this->_sections['b']['loop'];
$this->_sections['b']['step'] = 1;
if ($this->_sections['b']['start'] < 0)
    $this->_sections['b']['start'] = max($this->_sections['b']['step'] > 0 ? 0 : -1, $this->_sections['b']['loop'] + $this->_sections['b']['start']);
else
    $this->_sections['b']['start'] = min($this->_sections['b']['start'], $this->_sections['b']['step'] > 0 ? $this->_sections['b']['loop'] : $this->_sections['b']['loop']-1);
if ($this->_sections['b']['show']) {
    $this->_sections['b']['total'] = min(ceil(($this->_sections['b']['step'] > 0 ? $this->_sections['b']['loop'] - $this->_sections['b']['start'] : $this->_sections['b']['start']+1)/abs($this->_sections['b']['step'])), $this->_sections['b']['max']);
    if ($this->_sections['b']['total'] == 0)
        $this->_sections['b']['show'] = false;
} else
    $this->_sections['b']['total'] = 0;
if ($this->_sections['b']['show']):

            for ($this->_sections['b']['index'] = $this->_sections['b']['start'], $this->_sections['b']['iteration'] = 1;
                 $this->_sections['b']['iteration'] <= $this->_sections['b']['total'];
                 $this->_sections['b']['index'] += $this->_sections['b']['step'], $this->_sections['b']['iteration']++):
$this->_sections['b']['rownum'] = $this->_sections['b']['iteration'];
$this->_sections['b']['index_prev'] = $this->_sections['b']['index'] - $this->_sections['b']['step'];
$this->_sections['b']['index_next'] = $this->_sections['b']['index'] + $this->_sections['b']['step'];
$this->_sections['b']['first']      = ($this->_sections['b']['iteration'] == 1);
$this->_sections['b']['last']       = ($this->_sections['b']['iteration'] == $this->_sections['b']['total']);
?>
        
         <div class="grid">      
            
            <?php if ($this->_tpl_vars['banners'][$this->_sections['b']['index']]['link'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners'][$this->_sections['b']['index']]['link']; ?>
" class="box">
            <?php elseif ($this->_tpl_vars['banners'][$this->_sections['b']['index']]['yt'] != ''): ?>
            <a href="<?php echo $this->_tpl_vars['banners'][$this->_sections['b']['index']]['yt']; ?>
" class="box fancybox iframe">   
            <?php else: ?>
            <a href="#" class="box">   
            <?php endif; ?>   
              
              <?php if ($this->_tpl_vars['banners'][$this->_sections['b']['index']]['plik'] != ''): ?>
               <div class="foto" style="background-image: url(<?php echo $this->_tpl_vars['banners'][$this->_sections['b']['index']]['plik']; ?>
);">
              </div> 
              <?php endif; ?>            
              <span class="label <?php if ($this->_tpl_vars['banners'][$this->_sections['b']['index']]['plik'] == ''): ?>bezfoto<?php endif; ?>"><?php echo $this->_tpl_vars['banners'][$this->_sections['b']['index']]['nazwa']; ?>

              </span> </a>  	
          </div>
        
        <?php endfor; endif; ?>       
      </div>