<?php /* Smarty version 2.6.16, created on 2013-06-18 11:18:24
         compiled from yachts.tpl */ ?>
         
          <?php $_from = $this->_tpl_vars['yachts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['y']):
?>
          <h2><?php echo $this->_tpl_vars['y']['nazwa']; ?>
</h2>
          <?php if ($this->_tpl_vars['y']['pictures']): ?>
          <?php unset($this->_sections['pi']);
$this->_sections['pi']['name'] = 'pi';
$this->_sections['pi']['loop'] = is_array($_loop=$this->_tpl_vars['y']['pictures']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['pi']['show'] = true;
$this->_sections['pi']['max'] = $this->_sections['pi']['loop'];
$this->_sections['pi']['step'] = 1;
$this->_sections['pi']['start'] = $this->_sections['pi']['step'] > 0 ? 0 : $this->_sections['pi']['loop']-1;
if ($this->_sections['pi']['show']) {
    $this->_sections['pi']['total'] = $this->_sections['pi']['loop'];
    if ($this->_sections['pi']['total'] == 0)
        $this->_sections['pi']['show'] = false;
} else
    $this->_sections['pi']['total'] = 0;
if ($this->_sections['pi']['show']):

            for ($this->_sections['pi']['index'] = $this->_sections['pi']['start'], $this->_sections['pi']['iteration'] = 1;
                 $this->_sections['pi']['iteration'] <= $this->_sections['pi']['total'];
                 $this->_sections['pi']['index'] += $this->_sections['pi']['step'], $this->_sections['pi']['iteration']++):
$this->_sections['pi']['rownum'] = $this->_sections['pi']['iteration'];
$this->_sections['pi']['index_prev'] = $this->_sections['pi']['index'] - $this->_sections['pi']['step'];
$this->_sections['pi']['index_next'] = $this->_sections['pi']['index'] + $this->_sections['pi']['step'];
$this->_sections['pi']['first']      = ($this->_sections['pi']['iteration'] == 1);
$this->_sections['pi']['last']       = ($this->_sections['pi']['iteration'] == $this->_sections['pi']['total']);
?>
          <img src="/<?php echo $this->_tpl_vars['y']['pictures'][$this->_sections['pi']['index']]; ?>
" width="160" />
          <?php endfor; endif; ?>
          <?php endif; ?>
          <table style="float:right; clear:right; margin:0 20px 10px" class="yacht_tech" cellspacing="0" cellpadding="0">
           <tr>
            <td colspan="2" class="yacht_tech_top"><strong>Charakterystyka techniczna</strong></td>
           </tr>
           <tr>
            <td>Długość całkowita</td>
            <td><?php echo $this->_tpl_vars['y']['dlugosc']; ?>
</td>
           </tr>
           <tr>
            <td>Szerokość</td>
            <td><?php echo $this->_tpl_vars['y']['szerokosc']; ?>
</td>
           </tr>
           <tr>
            <td>Wyporność</td>
            <td><?php echo $this->_tpl_vars['y']['wypornosc']; ?>
</td>
           </tr>
           <tr>
            <td>Balast</td>
            <td><?php echo $this->_tpl_vars['y']['balast']; ?>
</td>
           </tr>
           <tr>
            <td>Silnik</td>
            <td><?php echo $this->_tpl_vars['y']['moc']; ?>
</td>
           </tr>
           <tr>
            <td>Zbiornik paliwa</td>
            <td><?php echo $this->_tpl_vars['y']['zbiornik_paliwa']; ?>
</td>
           </tr>
           <tr>
            <td>Zbiornik wody</td>
            <td><?php echo $this->_tpl_vars['y']['zbiornik_wody']; ?>
</td>
           </tr>
           <tr>
            <td>Powierzchnia żagli</td>
            <td><?php echo $this->_tpl_vars['y']['powierzchnia_zagli']; ?>
</td>
           </tr>
           <tr>
            <td>Liczba załogi</td>
            <td><?php echo $this->_tpl_vars['y']['liczba_zalogi']; ?>
</td>
           </tr>
          </table>
          <?php echo $this->_tpl_vars['y']['opis']; ?>

          
          <?php endforeach; endif; unset($_from); ?>
          