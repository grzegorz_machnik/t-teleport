<?php /* Smarty version 2.6.16, created on 2013-01-24 16:37:02
         compiled from oferta.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title></title>
  <link rel="stylesheet" type="text/css" href="style<?php echo $this->_tpl_vars['lngpostfix']; ?>
.css" />
  <link rel="stylesheet" type="text/css" href="master.css" />
  <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>
 <link href="js/jquery.bxslider.css" rel="stylesheet" />
  <script type="text/javascript" src="js/moje.js"></script>
  <meta name="Keywords" content="" />
  <meta name="Description" content="" />
  <meta name="Robots" content="all" />
  <meta name="Revisit-after" content="14 days" />
  </head>
<body class="ofertowa">
<div class="flags">
<?php if ($this->_tpl_vars['lngpostfix'] == 'en'): ?>
<a href="?lng=pl&site=oferta" class="pl"></a>
<div class="en"></div>
<?php else: ?>
<div class="pl"></div>
<a href="?lng=en&site=oferta" class="en"></a>
<?php endif; ?>
</div>
<div class="box5">
	<div class="inner">
        <?php unset($this->_sections['of']);
$this->_sections['of']['name'] = 'of';
$this->_sections['of']['loop'] = is_array($_loop=$this->_tpl_vars['oferta']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['of']['show'] = true;
$this->_sections['of']['max'] = $this->_sections['of']['loop'];
$this->_sections['of']['step'] = 1;
$this->_sections['of']['start'] = $this->_sections['of']['step'] > 0 ? 0 : $this->_sections['of']['loop']-1;
if ($this->_sections['of']['show']) {
    $this->_sections['of']['total'] = $this->_sections['of']['loop'];
    if ($this->_sections['of']['total'] == 0)
        $this->_sections['of']['show'] = false;
} else
    $this->_sections['of']['total'] = 0;
if ($this->_sections['of']['show']):

            for ($this->_sections['of']['index'] = $this->_sections['of']['start'], $this->_sections['of']['iteration'] = 1;
                 $this->_sections['of']['iteration'] <= $this->_sections['of']['total'];
                 $this->_sections['of']['index'] += $this->_sections['of']['step'], $this->_sections['of']['iteration']++):
$this->_sections['of']['rownum'] = $this->_sections['of']['iteration'];
$this->_sections['of']['index_prev'] = $this->_sections['of']['index'] - $this->_sections['of']['step'];
$this->_sections['of']['index_next'] = $this->_sections['of']['index'] + $this->_sections['of']['step'];
$this->_sections['of']['first']      = ($this->_sections['of']['iteration'] == 1);
$this->_sections['of']['last']       = ($this->_sections['of']['iteration'] == $this->_sections['of']['total']);
?>
        <?php $this->assign('o', $this->_tpl_vars['oferta'][$this->_sections['of']['index']]); ?>
        <div class="foto_outer"><div class="foto" style="background:url(<?php echo $this->_tpl_vars['_DOMAIN']; ?>
mphoto2.php?img=<?php echo $this->_tpl_vars['o']['foto']; ?>
&op=resize&max=235&cx=235&cy=140) no-repeat 50% 50%"></div></div>
        <div class="tekst">
        <?php echo $this->_tpl_vars['o']['content']; ?>

        </div>
        <div class="clean"></div>
        <?php endfor; endif; ?>
        
        

  </div>
</div>

    <div class="stopka">
    <div style="margin-left:50%; left:-450px; position:absolute;">
    <?php if ($this->_tpl_vars['social']): ?>
    <?php if ($this->_tpl_vars['lngpostfix'] == ''): ?>Znajdź nas na:<?php else: ?>Find us on:<?php endif; ?>&nbsp;&nbsp; 
     <?php unset($this->_sections['so']);
$this->_sections['so']['name'] = 'so';
$this->_sections['so']['loop'] = is_array($_loop=$this->_tpl_vars['social']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['so']['show'] = true;
$this->_sections['so']['max'] = $this->_sections['so']['loop'];
$this->_sections['so']['step'] = 1;
$this->_sections['so']['start'] = $this->_sections['so']['step'] > 0 ? 0 : $this->_sections['so']['loop']-1;
if ($this->_sections['so']['show']) {
    $this->_sections['so']['total'] = $this->_sections['so']['loop'];
    if ($this->_sections['so']['total'] == 0)
        $this->_sections['so']['show'] = false;
} else
    $this->_sections['so']['total'] = 0;
if ($this->_sections['so']['show']):

            for ($this->_sections['so']['index'] = $this->_sections['so']['start'], $this->_sections['so']['iteration'] = 1;
                 $this->_sections['so']['iteration'] <= $this->_sections['so']['total'];
                 $this->_sections['so']['index'] += $this->_sections['so']['step'], $this->_sections['so']['iteration']++):
$this->_sections['so']['rownum'] = $this->_sections['so']['iteration'];
$this->_sections['so']['index_prev'] = $this->_sections['so']['index'] - $this->_sections['so']['step'];
$this->_sections['so']['index_next'] = $this->_sections['so']['index'] + $this->_sections['so']['step'];
$this->_sections['so']['first']      = ($this->_sections['so']['iteration'] == 1);
$this->_sections['so']['last']       = ($this->_sections['so']['iteration'] == $this->_sections['so']['total']);
?>
     <a href="<?php echo $this->_tpl_vars['social'][$this->_sections['so']['index']]['url']; ?>
" class="social"><img src="<?php echo $this->_tpl_vars['_DOMAIN'];  echo $this->_tpl_vars['social'][$this->_sections['so']['index']]['foto']; ?>
"></a>
     <?php endfor; endif; ?>
      <?php endif; ?>
     </div>
     <span style="margin-left:50%; left:300px; display:block; position:absolute;">Copyright by Edit sp. z o.o.</span>
    
   
    </div>
    
</body>
</html>