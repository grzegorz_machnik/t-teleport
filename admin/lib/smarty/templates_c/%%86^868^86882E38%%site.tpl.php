<?php /* Smarty version 2.6.16, created on 2012-12-24 10:46:06
         compiled from pl/site.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title></title>
  <link rel="stylesheet" type="text/css" href="style.css" />
  <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>
 <link href="js/jquery.bxslider.css" rel="stylesheet" />
  <script type="text/javascript" src="js/moje.js"></script>
  <meta name="Keywords" content="" />
  <meta name="Description" content="" />
  <meta name="Robots" content="all" />
  <meta name="Revisit-after" content="14 days" />
  </head>
<body>
<div class="flags">
<div class="pl"></div>
<a href="#" class="en"></a>
</div>
<div class="box1">
	<div class="inner">
    <div class="logo_top">
    <img src="gfx/logo.png" alt=""  />
    </div>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
 	</div>

</div>
<div class="box2">
	<div class="inner">
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
      <div class="col1">
        <div class="foto" style="background:url(temp.jpg) no-repeat 50% 50%"></div>
        <div class="tekst">Amet irilit incil dolobor periure core min hendignisci elit non henissequi tat alit iliquipit lobore volore vullandit velisim quipsumsan euismod doloborer alis ex eugiamet, quam vulla faccum dipismod eugue faccum dunt pratum nos nulput ad tet wis ecte minisi. Lit alis dionsed</div>
      
      </div>
      <div class="col2">
        <div class="foto" style="background:url(temp.jpg) no-repeat 50% 50%"></div>
        <div class="tekst">Amet irilit incil dolobor periure core min hendignisci elit non henissequi tat alit iliquipit lobore volore vullandit velisim quipsumsan euismod doloborer alis ex eugiamet, quam vulla faccum dipismod eugue faccum dunt pratum nos nulput ad tet wis ecte minisi. Lit alis dionsed</div>
      </div>
      <div class="col3">
        <div class="foto" style="background:url(temp.jpg) no-repeat 50% 50%"></div>
        <div class="tekst">Amet irilit incil dolobor periure core min hendignisci elit non henissequi tat alit iliquipit lobore volore vullandit velisim quipsumsan euismod doloborer alis ex eugiamet, quam vulla faccum dipismod eugue faccum dunt pratum nos nulput ad tet wis ecte minisi. Lit alis dionsed</div>
      </div>

  </div>

</div>
<div class="box3">
	<div class="inner">
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <div class="slider">
    <ul class="bxslider">
    	<li>
        <div class="col">
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
        </div>
        <div class="col">
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
          <div class="foto_big" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
        </div>
        <div class="col last">
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
        </div>
        </li>
        <li>
        <div class="col">
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
        </div>
        <div class="col">
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
          <div class="foto_big" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
        </div>
        <div class="col last">
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
          <div class="foto_small" style="background:url(temp.jpg) no-repeat 50% 50%"><a href="#" class="lightbox"></a></div>
        </div>
        </li>
    </ul>
    </div>
 	</div>

</div>
<div class="box4" id="kontakt">
	<div class="inner">
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <div class="adres"></div>
    <form method="post" action="#kontakt">
    <div class="form">
      <div class="col1">
                Nazwa firmy
                  <br />
                  Imię i nazwisko*<br />
            Adres e-mail*<br />
            Numer telefonu<br />
            Treść zapytania* <br />
            <input type="submit" class="wyslij" />
      </div>
      <div class="col2">
      <input type="text" /><br />
      <input type="text" /><br />
      <input type="text" /><br />
      <input type="text" /><br />
      <textarea></textarea>
      <div class="pw">* pola wymagane</div>
      <div class="error">
      ! Podaj imię i nazwisko
        <br />
        ! Podaj poprawny adres e-mail
        <br />
        ! Numer telefonu bez znaków specjalnych i spacji
        <br />
        ! Napisz treść zapytania</div>
      </div>
    </div>
    </form>
    <div class="stopka">Znajdź nas na: <a href="#" class="fb"></a><a href="#" class="gm"></a> <span style="float:right">Copyright by Edit sp. z o.o.</span>
    </div>
    
 	</div>

</div>
</body>
</html>