<?php /* Smarty version 2.6.16, created on 2015-01-03 12:41:24
         compiled from site.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo $this->_tpl_vars['meta']['meta_title']; ?>
</title>
    <meta name="description" content="<?php echo $this->_tpl_vars['meta']['meta_description']; ?>
" />
    <meta name="keywords" content="<?php echo $this->_tpl_vars['meta']['meta_keywords']; ?>
 " />
    <link rel="stylesheet" type="text/css" href="/style.css" />
    
    <meta name="Robots" content="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Revisit-after" content="14 days" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="/moje.js"></script>

	<script type="text/javascript" src="/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link rel="stylesheet" type="text/css" href="/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<link rel="stylesheet" type="text/css" href="/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

  </head>
  <body>
    <div class="logo_back">
    </div>
    <div class="content"> 
      <a href="/" class="logo"></a> 
      <div class="flags">
        <a href="?lng=pl" class="pl"></a>
        <a href="?lng=en" class="en"></a>
      </div>  
      <div class="menu">  	
        <div class="round">
        </div>    
        <ul> 
          <?php $_from = $this->_tpl_vars['mtop']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['menu'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['menu']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
        $this->_foreach['menu']['iteration']++;
?>   
          <li>
            <a href="<?php if ($this->_tpl_vars['v']['url'] == ''):  echo $this->_tpl_vars['_DOMAIN'];  echo $this->_tpl_vars['v']['id']; ?>
/<?php echo $this->_tpl_vars['v']['slug']; ?>
.html<?php else:  echo $this->_tpl_vars['v']['url'];  endif; ?>"><?php echo $this->_tpl_vars['v']['title']; ?>
</a>
          </li>      
          <?php endforeach; endif; unset($_from); ?>  
        </ul>  
      </div>  
      <div class="clear">
      </div>
      
      <?php if (isset ( $_GET['site'] )): ?>
      <div class="boxy">
      <div class="grid  newline fr">
          <div class="box">
            
            <span class="label "><?php echo $this->_tpl_vars['st']['title']; ?>
</span> </div>
      	</div>
       <?php echo $this->_tpl_vars['content']; ?>

      </div> 
      <?php else: ?>
        <?php echo $this->_tpl_vars['content']; ?>

      <?php endif; ?>
          
      
    </div>
    </div>
</html>      
