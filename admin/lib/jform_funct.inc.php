<?
// jform
// v2.7
// la_mo_9.10.4
// joker@joker.art.pl
  function quotedPrintableEncode($input , $line_max = 76)
  {
      $lines  = preg_split("/\r?\n/", $input);
      $eol    = "\n";
      $escape = '=';
      $output = '';

      while(list(, $line) = each($lines)){
          $linlen     = strlen($line);
          $newline = '';

          for ($i = 0; $i < $linlen; $i++) {
              $char = substr($line, $i, 1);
              $dec  = ord($char);

              if (($dec == 32) AND ($i == ($linlen - 1))){
                  $char = '=20';

              } elseif($dec == 9) {
                  ;
              } elseif(($dec == 61) OR ($dec < 32 ) OR ($dec > 126)) {
                  $char = $escape . strtoupper(sprintf('%02s', dechex($dec)));
              }

              if ((strlen($newline) + strlen($char)) >= $line_max) {
                  $output  .= $newline . $escape . $eol;
                  $newline  = '';
              }
              $newline .= $char;
          }
          $output .= $newline . $eol;
      }
      $output = substr($output, 0, -1 * strlen($eol));
      return $output;
  }

  function plCharset($string) {
 
    $win2utf = array(
      "\xb9" => "\xc4\x85", "\xa5" => "\xc4\x84",
      "\xe6" => "\xc4\x87", "\xc6" => "\xc4\x86",
      "\xea" => "\xc4\x99", "\xca" => "\xc4\x98",
      "\xb3" => "\xc5\x82", "\xa3" => "\xc5\x81",
      "\xf3" => "\xc3\xb3", "\xd3" => "\xc3\x93",
      "\x9c" => "\xc5\x9b", "\x8c" => "\xc5\x9a",
      "\x9f" => "\xc5\xbc", "\x8f" => "\xc5\xbb",
      "\xbf" => "\xc5\xba", "\xaf" => "\xc5\xb9",
      "\xf1" => "\xc5\x84", "\xd1" => "\xc5\x83"
    );
    
    $iso2utf = array(
      "\xb1" => "\xc4\x85", "\xa1" => "\xc4\x84",
      "\xe6" => "\xc4\x87", "\xc6" => "\xc4\x86",
      "\xea" => "\xc4\x99", "\xca" => "\xc4\x98",
      "\xb3" => "\xc5\x82", "\xa3" => "\xc5\x81",
      "\xf3" => "\xc3\xb3", "\xd3" => "\xc3\x93",
      "\xb6" => "\xc5\x9b", "\xa6" => "\xc5\x9a",
      "\xbc" => "\xc5\xba", "\xac" => "\xc5\xb9", //�
      "\xbf" => "\xc5\xbc", "\xaf" => "\xc5\xbb", //�
      "\xf1" => "\xc5\x84", "\xd1" => "\xc5\x83"
    );
 
    return strtr($string, array_flip($iso2utf));
  }     

function jmail_html($to, $title, $msg, $from) {
  
  $header .= "From: {$from} \n";
  $header .= "MIME-Version: 1.0\n";
  $header .= "Content-Type: text/html; charset=iso-8859-2\n";

	$trescmaila = "<html><head><META HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; charset=iso-8859-2\"><style>* { font-family: arial, helvetica; font-size: 12px; color: #000000 }</style></head><body bgcolor=#ffffff>{$msg}</body></html>";
  $trescmaila = plCharset($trescmaila);
 
  return mail ($to, plCharset($title), plCharset($trescmaila), $header);
}

function jmail($to, $title, $msg, $from) {
  			$header = 'From: '.$from.'
Content-Type: text/plain; charset="iso-8859-2"
';

	return mail($to, plCharset($title), plCharset($msg), $header);

}

function jform_saverequest($req, $noch='') {
	if ($noch!='') $noch=split('[ ]*,[ ]*',$noch);
	if (count($req)>0) {
		foreach ($req as $k=>$v) {
		}
	}
}

function jform_valid_p( $pattern, $value ) {
	if ( preg_match( $pattern, $value ) ) {
		return true;
	} else {
		return false;
	}
}

function jform_validate($type, $value) {
	$ok=array();
	$type=explode(',',$type);
	
	foreach ($type as $v) {
		switch (trim($v)) {
			case 'phone':
				if ( !preg_match("(^[\d]{9}$)", $value, $reg)) {
					$ok[]=$v;
				}
				break;
			case 'notnull':
				if (strlen(trim($value))==0) {
					$ok[]=$v;
				}
				break;
			case 'email':
				if ( !preg_match( "((^[\d\w\-_\.]+@([\w\d_\-]+\.?)+\.[\w\d\-_]{1,4}$)|(^$))", $value) ) {
					$ok[]=$v;
				}
				break;
			case 'postcode':
				if ( !ereg("^([0-9]{2})-([0-9]{3})$", $value) ) {
					$ok[]=$v;
				}
				break;
			case 'pass':
			case 'password':
				if ( !preg_match( "((^[\d\w\-\._]{3,10}$)|(^$))",$value ) ) {
					$ok[]=$v;
				}
				break;
			default:
				return true;
				break;
		}
		
	}
	return count($ok)>0?$ok:true;
	
}

function jform_open($action, $method='post', $add='') {
	if (file_exists('js/calendar.js')) $calendar_load='<script language="JavaScript" type="text/javascript" src="js/calendar.js"></script>'; else $calendar_load='';
	return "{$calendar_load} <form action=\"{$action}\" method=\"{$method}\" enctype=\"multipart/form-data\" {$add}>";
}

function jform_close() {
	return "</form>";
}

function jform_newdate($name, $value) {
	$tu.='<input id="'.$name.'" name="'.$name.'" size="11" maxlength="10" value="'.$value.'" type="text" readonly>';
	$tu.='<img src="gfx/calendar.gif" align=absmiddle title="Wybierz z kalendarza" onclick="calendarShow(this, '.$name.', null, null);">';
	return $tu;
}

function jform_date($name, $value) {
	if (file_exists('js/calendar.js')) $calendar_load='<script language="JavaScript" type="text/javascript" src="js/calendar.js"></script>'; else $calendar_load='';

			list($year, $month, $day)=split('-',$value);
			$w="{$calendar_load}<input type=hidden name=\"{$name}\" id=\"{$name}\" value=\"{$value}\"><select id=\"{$name}_year\" onchange=\"javascript:{$name}.value={$name}_year.value+'-'+{$name}_month.value+'-'+{$name}_day.value;\">";
			for ($i=1999; $i<=intval(date('Y'))+20; $i++) {
				$sel=($i==$year)?' selected ':'';
				$w.="<option value=\"{$i}\"{$sel}>{$i}";
			}
			$w.="</select>-<select id=\"{$name}_month\" onchange=\"javascript:{$name}.value={$name}_year.value+'-'+{$name}_month.value+'-'+{$name}_day.value;\">";
			for ($i=1; $i<=12; $i++) {
				$sel=($i==$month)?' selected ':'';
				$w.="<option value=\"{$i}\"{$sel}>{$i}";
			}
			$w.="</select>-<select id=\"{$name}_day\" onchange=\"javascript:{$name}.value={$name}_year.value+'-'+{$name}_month.value+'-'+{$name}_day.value;\">";
			for ($i=1; $i<=31; $i++) {
				$sel=($i==$day)?' selected ':'';
				$w.="<option value=\"{$i}\"{$sel}>{$i}";
			}
			$w.="</select>";

			$w.='<img src="gfx/calendar.gif" id="calshow_'.$name.'" align=absmiddle title="Wybierz z kalendarza" class="activeel" onclick="calendarShow(\''.$name.'\', null, null, event);">';

		return $w;

}

function jform_combo_counter($name, $value, $start=0, $stop=100, $step=1, $add='', $lcnt='') {
	for ($i=$start; $i<=$stop; $i=$i+$step) {
		if ($lcnt!='') $tmp[$i]=sprintf("%{$lcnt}d",$i);
		else $tmp[$i]=$i;
	}
	return jform_combo($name, $tmp, $value, $add);
}

function jform_combo($name, $value='', $sel='', $add='') {
		$w="<select name=\"{$name}\" id=\"{$name}\" {$add}>";
		if (is_array($value)&&count($value)>0) {
			foreach ($value as $k=>$v) {
				$w.="<option";
// 				echo "::{$sel}::{$k}::{$v}".($sel===$k?" selected":'').gettype($sel).'::'.gettype($k).'<br>';
				$w.=("{$sel}"=="{$k}")?" selected":'';
				$k=strval($k);
// 				$w.=($k!='')?" value=\"{$k}\"":'';
				$w.=" value=\"{$k}\"";
				$w.=">".strval($v)."</option>";
			}
		}
		$w.='</select>';
		return $w;
}

function jform_text($name, $value='', $add='') {
	return "<input type=text id=\"{$name}\" name=\"{$name}\" value=\"{$value}\" {$add}>";
}

function jform_password($name, $value='', $add='') {
	return "<input type=password id=\"{$name}\" name=\"{$name}\" value=\"{$value}\" {$add}>";
}

function jform_textarea($name, $value='', $add='', $collapse=false) {
	if ($collapse==true) {
		$tu.="<table border=0 cellspacing=0 cellpadding=1 bgcolor=\"#f0f0f0\"><tr><td>
<img src=\"gfx/down.gif\" title=\"Kliknij aby zmieni� rozmiar okna edycji\" style=\"cursor:pointer\" id=\"jform_collapse_img_{$name}\" onclick=\"javascript:jform_collapse('{$name}',200,50);\"></td></tr><tr><td>
<textarea id=\"{$name}\" style=\"height:50px; width:600px;\" name=\"{$name}\" {$add}>{$value}</textarea>
</td></tr></table>";
	} else {
		$tu.="<textarea id=\"{$name}\" name=\"{$name}\" {$add}>{$value}</textarea>";
	}
	return $tu;
}

function jform_htmlarea_script() {
	return '<SCRIPT language="javascript" src="editor/fckeditor.js"></SCRIPT>';
}

function jform_htmlarea($name, $value, $width=590, $height=400, $toolbar='Default', $css='') {

				$value=ereg_replace("\r\n",'',$value);
				$value=ereg_replace("\'","&#039;",$value);
				$value=ereg_replace("\n\r",'',$value);
				$value=ereg_replace("\n",'',$value);
				$value=ereg_replace("
",'',$value);
				$wynik.="<b>{$desc}</b><br>
<div style=\"width:{$width}px;\"></div>
<SCRIPT language=\"javascript\">
<!--
var oFCKeditor ;
oFCKeditor = new FCKeditor('{$name}',{$width},{$height},'{$toolbar}','{$value}') ;
".($css!=''?"oFCKeditor.Config[\"EditorAreaCSS\"] = '{$css}'; ":'')."
oFCKeditor.Create() ;
//-->
			</SCRIPT>";
return $wynik;
}
//,{$width},{$height},'{$toolbar}','{$value}') ;
function jform_file($nazwa, $add='') {
	return "<input type=file id=\"{$nazwa}\" name=\"{$nazwa}\" {$add}>";
}

function jform_checkbox($name, $value='', $add='', $desc='') {
	if (strtoupper($value)=='Y'||$value==1||$value=='on') $value='on';
	return "<input type=checkbox id=\"{$name}\" name=\"{$name}\"".($value=='on'?' checked':'')." {$add}>".($desc!=''?"<label for=\"{$name}\">{$desc}</label>":'');
}

function jform_checkbox_switch($name, $value, $values) {
	if ( !is_array($values) ) {
		$values=explode(',',$values);
	} 

	if ($value==$values[0]) {
		$check='on'; 
	} else {
		$check='';
		$value=$values[1];
	}

	$tu=jform_hidden($name, $value).
		jform_checkbox('',$check,"onclick=\"if (this.checked) {$name}.value='{$values[0]}'; else {$name}.value='{$values[1]}';\"");
	
	return $tu;
}

function jform_io_checkbox($name, $value, $desc='', $dest='', $add='') {
	return '<script language=javascript>io_onchange(this.form, '.$name.', \''.$dest.'\');</script>'.
				jform_checkbox($name,$value, "onkeypress=\"io_onchange(this.form, this,'{$dest}')\" onmousedown=\"io_onchange(this.form, this, '{$dest}')\" ".$add, $desc);
}

function jform_checkbox_multi($name, $val, $arr, $deli=1, $add='') {
	$wynik="<script language=javascript>

var zm_{$name}=new Array();

function jcmjs_{$name}(a,b,c) {
	if (b.value!='') zm_{$name}=b.value.split(',');
	if (a.checked==true) zm_{$name}.push(c);
	else {
		for (var i=0; i<zm_{$name}.length; i++) {
			if (c==zm_{$name}[i]) zm_{$name}.splice(i,1);
		}
	}
	if (zm_{$name}.length==1) return zm_{$name}[0];
	else return zm_{$name}.join(',');
}
 </script>
"; 

	if (is_array($arr)&&count($arr)>0) {
		$tb=new easytable();
		$tb->setsimple();
		$tb->opentable();
		$wynik.="<input type=hidden name=\"{$name}\" id=\"{$name}\" value=\"{$val}\">";
		$checkedy=@split(',',$val);
		$licznik==0;
		foreach ($arr as $k=>$v) {
			if ( $licznik % $deli == 0 && $deli>0 ) $tb->openrow();
			if (in_array($k, $checkedy)) $checked=' checked '; else $checked='';
			$cell="<input type=checkbox {$checked} onclick=\"{$name}.value=jcmjs_{$name}(this, {$name}, '{$k}')\">{$v}<br>";
			$tb->cell($cell, $add);
			$licznik++;
		}
		$wynik.=$tb->show();
	}
	return $wynik;
}

function jform_radio($name, $value='', $sel='') {
	$w='';
	if (count($value)>0) {
		foreach ($value as $k=>$v) {
			$w.="<input type=radio name=\"{$name}\"";
			$w.=($sel==$v)?" checked":'';
			$k=strval($k);
			$w.=($v!=''||$k==0)?" value=\"{$v}\"":'';
			$w.=">{$v}<br>";
		}
	}
	return $w;
}

function jform_hidden($name, $value, $add='') {
	return "<input type=hidden id=\"{$name}\" name=\"{$name}\" value=\"{$value}\" {$add}>";
}

function jform_input($type, $name, $value, $add='') {
	return "<input type=\"{$type}\" id=\"{$name}\" name=\"{$name}\" value=\"{$value}\" {$add}>";
}

function jform_redirect($name, $value, $link, $add='') {
	if (file_exists($value)) {
		return "<a href=\"{$link}\" {$add}><img src=\"{$value}\" border=0></a>";
	} else return "<input type=button name=\"{$name}\" value=\"{$value}\" onclick=\"window.location='{$link}'\" {$add}>";
}

function jform_submit($name, $value, $add='') {
	if (file_exists($value)) {
		return "<input type=image id=\"{$name}\" name=\"{$name}\" value=\"{$value}\" src=\"{$value}\" {$add}>";
	} else return "<input type=submit id=\"{$name}\" name=\"{$name}\" value=\"{$value}\" {$add}>";
}


function jform_submit_confirm($name, $value, $add='') {
	return jform_input('button',$name, $value, "onclick=\"checksubmit(this.form,'Jeste� pewien?');\" ".$add);
}


function jform_htmlhidden($name, $val, $show='') {
	if ($show=='') $show=$val;
	return jform_hidden($name, $val).$show;
}

?>
