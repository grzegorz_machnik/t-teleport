<?
// v_4.76
// brak kompatybilnosci ponizej tej wersji dla funkcji insertfunct;
// la_mo_2.11.4
// joker@joker.art.pl


library_exists('db_funct.inc.php');
library_exists('easytable.class.php');
library_exists('jform_funct.inc.php');
library_exists('jdbeform.class.php');
library_exists('jdb_lglob.inc.php');

class jdbet {

	// DB
	var $idname;
	var $sqlconn;
	var $table;
	var $where;
	var $limit;
	var $offset;
	var $offset_link;
	var $allrows;
	var $numrows;
	var $orderby;
	var $orderdesc='asc';
	var $multiorder=0;
	var $orderkey;
	var $lp=1;
	
	// FromUser	
	var $fu;

	// JDBE vars
	var $lng=array();
	var $toshow=array();
	var $toshow_size=array();
	var $fields=array();
	var $deletechild=array();
	var $foreignkeys=array();
	var $artforeignkeys=array();
	var $fielddescriptions=array();	
	var $validate=array();
	var $error=array();
	var $messages=array();
	var $outfiles=array();
	var $relation=array();
	var $imgs=array();
	
	
	var $foto_details=array();
			
	var $active=array();
	var $readonly=false;

	var $show_funct=false;
	var $bg_funct=false;
	var $insert_funct=false;
	var $afterinsert_funct=false;
	
	
	var $crypt_link=false;
	
	var $operation=array();
	var $operation_move=array();
	var $operation_active=array();
	var $mainoperation=array();
	
	var $deniedoperation=array();
	var $simpleadd=false;
	


	function jdbet( &$sqlconn, $table, $lng='', $idname='id' ) {
		global $lng_cfg;
		$this->sqlconn = $sqlconn;
		$this->idname=$idname;
		$this->table = $table;
		$fields = $sqlconn->fields($table);

		foreach ($fields as $k=>$v) {
			$this->fields[$k]=$v;
			$this->fields[$k]['db']=1;
		}

		$this->orderby=$this->idname;
		
		if (isset($this->fields['jdb_active'])) {
			$this->set_active('jdb_active');
			$this->set_fielddescriptions('jdb_active','Aktywny rekord');
			
		}
		
		if (isset($this->fields['jdb_readonly'])) {
			$this->set_readonly('jdb_readonly');
		}
		
		if (isset($this->fields['jdb_orderkey'])) {
			$this->set_orderkey('jdb_orderkey');
			$this->multiorder=0;
		} elseif (isset($this->fields['orderkey'])) {
			$this->set_orderkey('orderkey');
			$this->multiorder=0;
		} else {
			$this->multiorder=0;
		}
		
		$this->set_fieldtype($this->idname,'hidden');
		if ($lng!='') $this->lng=&$lng;
		else $this->lng=&$lng_cfg;
		$this->set_defaultvalues();
		$this->set_offset();
	}
	
// RELATION

	function set_relation( $rname, $rtable, $rin, $rout) {
		$this->relation[$rname]=array(
			'rtable'=>$rtable,
			'rin'=>$rin,
			'rout'=>$rout,
			);
		$this->set_deletechild($rtable, $rin);
	}
	
	function set_rout( $rname, $tname, $id, $sqlshow, $htmlshow='', $where='', $order='') {
		$this->relation[$rname]=$this->relation[$rname]+array(
			'rout_table'=>$tname,
			'rout_id'=>$id,
			'rout_sqlshow'=>$sqlshow,
			'rout_htmlshow'=>$htmlshow,
			'rout_where'=>$where,
			'rout_order'=>$order,
		);
	}
	
	function set_rin( $rname, $idval, $show, $showtype=true ) {
		$this->relation[$rname]=array(
			'rin_value'=>$idval,
			'rin_show'=>$show,
			'rin_showtype'=>$showtype,
		);
	}
	
// END RELATION


	function set_simpleadd($v=true) {
		$this->simpleadd=$v;
		$this->unset_mainoperation('add');
	}
	
	function set_fotodetails($limit=1, $max=100, $mphoto=true, $upload_dir='./../imgs_upload/', $cols=4 ) {
		
		$this->foto_details=array('limit'=>$limit, 'max'=>$max, 'mphoto'=>$mphoto, 'upload_dir'=>$upload_dir, 'cols'=>$cols);
	}
	
	function upload_foto() {
		if (isset($_FILES)) {
			foreach ($_FILES as $k=>$v) {
				if ($v['error']!=4) {
					if ($this->fu[$this->idname]!='') $idek=$this->fu[$this->idname];
					else $idek=$this->fu['jdb_tmp_id'];
					$ext=strtolower(file_ext($v['name']));
					if ( ereg($ext,'jpg,gif,png') ) {
						$nazwa = $this->foto_details['upload_dir'] . $this->table . '_' . $idek . '_' . uniqid(time()).'.'.$ext;
						if ( move_uploaded_file( $v['tmp_name'],$nazwa ) ) return '1'; else return '0';
					}					
				} 
			}
		}
		
	}
	
	
	
	function del_foto($id='') {
		if ($id=='') {
			$plik=split(',',$this->fu['foto2del']);
			foreach ($plik as $v) {
				@unlink($this->foto_details['upload_dir'].$v);
			}
		} else {
			$pliki=glob($this->foto_details['upload_dir'].$this->table.'_'.$id.'_*.*');
			if ($pliki!==false && count($pliki)>0) {
				foreach ($pliki as $v) {
					@unlink($v);
				}
			}
		}
	}

	function set_cryptlink() {
		$this->crypt_link=true;
	}
	
	function unset_cryptlink() {
		$this->crypt_link=false;
	}
	
	function make_link($link) {
		preg_match("(^([\w_]+\.php)?\??([\s\S]+))",$link, $reg);
// 		pre($reg);
		if ($this->crypt_link===true) {
			return $reg[1].'?'.sqs($reg[2]);
		} else {
			return $reg[1].'?'.$reg[2];
		}
	}
	
	function set_active($f, $val='y,n') {
		$val=explode(',',$val);
		$this->active['name']=$f;
		$this->active['y']=$val[0];
		$this->active['n']=$val[1];
		$this->set_fieldtype($f,'switch');
	}
	
	function set_multiorder($v=0) {
		$this->multiorder=$v;
	}
	
	function set_readonly($f) {
		$this->readonly=$f;
		$this->set_fieldtype($f, 'hidden');
	}
	
	function set_message($t,$m, $op='') {
		$this->messages[$op==''?'all':$op][]=array('title'=>$t,'msg'=>$m);
	}
	
	function set_validate($field, $type) {
		$this->validate[]=array('field'=>$field, 'type'=>$type);
	}
	
	function set_deniedoperation($k) {
		if (!in_array($k,$this->deniedoperation)) $this->deniedoperation[]=$k;
	}
	
	function unset_operation($op) {
		unset($this->operation[$op]);
	}
	
	function set_operation($name, $show, $link, $confirm=0) {
		$this->operation[$name]['name']=$show;
		$this->operation[$name]['link']=$link;
		if ($confirm!=0) $this->operation[$name][confirm]=1;
	}

	
	function unset_mainoperation($op) {
		unset($this->mainoperation[$op]);
	}
	
	function set_defaultvalues() {
		$this->operation=	array(
			'edit'=>array(
				'name'=>"<img src=\"{$this->lng['gfx_dir']}/edit.gif\" border=0 title=\"{$this->lng['edit']}\">",
				'link'=>'?op=edit&'.$this->idname.'=%'.strtoupper($this->idname).'%',
			),
			'del'=>array(
				'name'=>"<img src=\"{$this->lng['gfx_dir']}/del.gif\" border=0 title=\"{$this->lng['del']}\">",
				'link'=>'?op=del&'.$this->idname.'=%'.strtoupper($this->idname).'%',
				'confirm'=>1,
			),
		);
		$this->operation_move=array(
				'moveup'=>array(
				'name'=>"<img src=\"gfx/up.gif\" border=0 title=\"{$this->lng['up']}\">",
				'link'=>'?op=up&'.$this->idname.'=%'.strtoupper($this->idname).'%',
			),
			'movedown'=>array(
				'name'=>"<img src=\"gfx/down.gif\" border=0 title=\"{$this->lng['down']}\">",
				'link'=>'?op=down&'.$this->idname.'=%'.strtoupper($this->idname).'%',
			),
		);
	
		$this->mainoperation=array(
				'add'=>array(
				'name'=>"<img src=\"{$this->lng['gfx_dir']}/add.gif\" border=0>",
				'link'=>'?op=add',
			),
		);
		$this->operation_active=array(
			'y'=>array(
				'name'=>"<img src=\"gfx/active.gif\" border=0 title=\"{$this->lng['active']}\">",
				'link'=>'?op=deactiv&'.$this->idname.'=%'.strtoupper($this->idname).'%',
			),
			'n'=>array(
				'name'=>"<img src=\"gfx/deactive.gif\" border=0 title=\"{$this->lng['deactive']}\">",
				'link'=>'?op=activ&'.$this->idname.'=%'.strtoupper($this->idname).'%',
			),
		);
		$this->imgs=array(
			'ajax_switch_y'=>'gfx/enum_y.gif',
			'ajax_switch_n'=>'gfx/enum_n.gif',
		);

	}
	
	function set_orderby($o) {
		$this->orderby=$o;
	}
	
	function set_orderkey($o) {
		$this->orderkey=$o;
		$this->orderby=$o;
		$this->set_fieldtype($o, 'hidden');
	}
	
	function set_desc($desc) {
		$this->orderdesc=$desc;
	}
	
	function unset_offset() {
		$this->limit=false;
		$this->offset=false;
		$this->offset_link=false;
	}
	
	function set_offset($ofs='', $lim=20, $link='offs') {
		$this->limit=$lim;

		if ($_REQUEST[$link]!='') {
			$_SESSION[$link]=$_REQUEST[$link];
		}

		if (!isset($_SESSION[$link]) && $ofs=='') {
			$_SESSION[$link]=0;
		}

		if (basename($_SERVER['PHP_SELF'])!=$_SESSION['jdb_currentsite']) {
			$_SESSION[$link]=0;
			$_SESSION['jdb_currentsite']=basename($_SERVER['PHP_SELF']);
		}
		
		$this->offset=$_SESSION[$link];
		$this->offset_link=$link;
	}

	function op_showoffsetlink() {
		$lt=6;
		if ($this->limit!==false) {
			$cnt=ceil($this->allrows/$this->limit);
			if ($this->offset>($cnt*$this->limit)) {
				$this->offset=(($cnt*$this->limit));
				$_SESSION[$this->offset_link]=$this->offset;
			}
			$tu=array();
			if ($cnt>1) {

				$link="{$this->offset_link}=0";
				$link=$this->crypt_link===true?sqs($link):$link;
				$tu[]="<a href=\"{$_SERVER['PHP_SELF']}?{$link}\" title=\"Początek listy\">&laquo;&laquo;</a>&nbsp;&nbsp;";
				if (($this->offset/$this->limit-$lt)>0) {
					$start=$this->offset/$this->limit-$lt+1;
					$tu[]="...";
				} else {
					$start=1;
				}
				if (($this->offset/$this->limit+$lt)>$cnt) {
					$stop=$cnt;
					$end=0;
				} else {
					$stop=$this->offset/$this->limit+$lt+1;
					$end=1;
				}

				for ($i=$start; $i<=$stop; $i++) {
					$link="{$this->offset_link}=".(($i-1)*$this->limit);
					$link=$this->crypt_link===true?sqs($link):$link;
					$style=$_SESSION[$this->offset_link]==($i-1)*$this->limit?' style="font-weight:bold; color:red; font-size:13px;"':'';
					$tu[]=" <a href=\"{$_SERVER['PHP_SELF']}?{$link}\"{$style}>{$i}</a> ";
				}
				
				if ($end==1) $tu[]='...';

				$link="{$this->offset_link}=".($cnt*$this->limit-$this->limit);
				$link=$this->crypt_link===true?sqs($link):$link;
				$tu[]="&nbsp;&nbsp;<a href=\"{$_SERVER['PHP_SELF']}?{$link}\" title=\"Koniec listy\">&raquo;&raquo;</a>";
				
			}


			if (count($tu)>0) {
				$link="{$this->offset_link}=".($this->offset>0?$this->offset-$this->limit:0);
				$link=$this->crypt_link===true?sqs($link):$link;
				$prev="<a href=\"{$_SERVER['PHP_SELF']}?{$link}\">&laquo; poprzednia strona</a>";
				$link="{$this->offset_link}=".($this->offset+$this->limit<$cnt*$this->limit?$this->offset+$this->limit:($cnt-1)*$this->limit);
				$link=$this->crypt_link===true?sqs($link):$link;
				$next="<a href=\"{$_SERVER['PHP_SELF']}?{$link}\">następna strona &raquo;</a>";
				$tmp = implode(' | ',$tu);
				$tu = "<table width=700 cellspacing=1 cellpadding=2 bgcolor=#e0e0e0><tr><Td bgcolor=#f0f0f0 width=150 align=left>{$prev}</td><Td bgcolor=#f0f0f0 align=center><b>Znaleziono {$this->allrows} wpisów</b></td><Td bgcolor=#f0f0f0 width=150 align=right>{$next}</td></tr><tr><td bgcolor=#ffffff align=center colspan=3>{$tmp}</td></tr></table>";
				return $tu;
			} else return '';
			
		} return '';
	}
	
	function set_showfunct($name) {
		$this->show_funct=$name;
	}
	
	function set_insertfunct($name) {
		$this->insert_funct=$name;
	}
	
	function set_afterinsertfunct($name) {
		$this->afterinsert_funct=$name;
	}
	
	function set_bgfunct($name) {
		$this->bg_funct=$name;
	}
	
	function set_toshow($t) {
		if ( is_array($t) ) $this->toshow=$t;
		else $this->toshow=split('[, ]+',$t);
	}

	function set_toshowsize($t) {
		if ( is_array($t) ) $this->toshow_size=$t;
		else $this->toshow_size=split('[, ]+',$t);
	}
	
	function set_where($w) {
		$this->where=$w;
	}
	
	function set_deletechild( $table, $remoteid ) {
		$this->deletechild[]= compact( 'table','remoteid' );
	}
	
	function set_field($name, $type, $value, $data='', $foreign='') {
		$this->fields[$name]['type']=$type;
		$this->fields[$name]['default']=$value;
		if ($data!='') $this->fields[$name]['data']=$data;
		if ($foreign!='') $this->fields[$name]['foreign']=$foreign;
	}
	
	function set_fieldvalue($name, $value) {
		$this->fields[$name]['default']=$value;
	}
	
	function set_fieldtype($name, $type) {
		$this->fields[$name]['type']=$type;
	}

	function set_outfile($name,$filename) {
		$this->fields[$name]['type']='outfile';
		$this->outfiles[$name]=$filename;
	}

	function set_filebrowse($name,$filename) {
		$this->fields[$name]['type']='filebrowse';
		$this->outfiles[$name]=$filename;
	}
	
	function set_fielddescriptions($name, $desc='') {
		if (is_array($name) && $desc=='') {
			foreach ($name as $k=>$v) {
				$this->fielddescriptions[$k]=$v;
			}
		} elseif (is_array($name) && $desc!='') {
			foreach ($name as $v) {
				$this->fielddesciptions[$v]=$desc;
			}
		} else {
			$this->fielddescriptions[$name]=$desc;
		}
	}
	
	function set_foreignkey($fieldname, $tablename, $index, $value, $nw=true, $notnull='null', $where='', $order='') {
		$this->fields[$fieldname]['relation']=array('rtable'=>$tablename, 'rid'=>$index, 'rs'=>$value, 'nw'=>$nw, 'notnull'=>$notnull,'rorder'=>$order, 'rwhere'=>$where);
		$this->fields[$fieldname]['type']=$nw===true?'relwin1n':'combo';
		$this->fields[$fieldname]['foreign']=true;
	}

	function set_foreignkey_sql($fieldname, $sql) {
		$this->fields[$fieldname]['type']='combo';
		$this->fields[$fieldname]['data'] = db_getbyindex( $this->sqlconn, $sql );
		$this->fields[$fieldname]['foreign']=true;
	}

	function set_artforeignkey($fieldname, $data) {
		if ( !in_array($this->fields[$fieldname]['type'], array('combo','select','enum') ) ) $this->fields[$fieldname]['type']='combo';
		$this->fields[$fieldname]['data'] = $data;
		$this->fields[$fieldname]['foreign'] = true;
	}

	function op_delete() {
		
		if ($this->fu[$this->idname]!='') {
			$sql[]=" delete from {$this->table} where {$this->idname} = '{$this->fu[$this->idname]}' ";
			if ( count( $this->deletechild )>0 ) {
				foreach ($this->deletechild as $v) {
					$sql[]=" delete from {$v['table']} where {$v['remoteid']}='{$this->fu[$this->idname]}' ";
				}
			}
			foreach ($sql as $v) {
				$this->sqlconn->query($v);
			}
			$this->del_foto($this->fu[$this->idname]);
		}
		return $this->op_show();
		
	}
	
	
	function op_edit() {
		
		switch ($this->fu['op']) {
			case 'edit':
			case 'update':
				$op='update';
				break;
			case 'insert':
			case 'add':
			default:
				$op='insert';
		}
		
		$this->set_field('op','hidden',$op);
				
		$a=new jdbeform($this->sqlconn, $this->table, $this->fu[$this->idname], $this->idname);
		$a->import_fotodetails($this->foto_details);
		$a->import_outfiles($this->outfiles);
		$a->import_relation($this->relation);
		$a->import_fielddescriptions($this->fielddescriptions);
		$a->import_fields($this->fields);
		if (count($this->fu)>0) $a->import_fu($this->fu);
		$a->import_errors($this->error);
		
		return $a->show();
		
	}
	
	function set_validfield($field, $t) {
		$pat=split('[, ]+',$t);
		$this->validate[$field]=$pat;
	}
	
	function validate() {
		$ok=array();
		if (count($this->validate)>0) {
			foreach ($this->validate as $field=>$types) {
				foreach ($types as $v) {
					$fd=$this->fielddescriptions[$field]!=''?$this->fielddescriptions[$field]:ucfirst(str_replace('_',' ',$field));
					switch (trim($v)) {
						case 'notnull':
							if (strlen(trim($this->fu[$field]))==0) {
								$ok[]=etpl( array('field'=>$fd) , $this->lng['field_valid_notnull']);
								$this->error[$field]=1;
							}
							break;
						case 'number':
							if ( preg_match( "(^[\d]*$)", $this->fu[$field] ) ) {
								$ok[]=etpl( array('field'=>$fd) ,$this->lng['field_valid_bad']);
								$this->error[$field]=1;
							}
							break;
						case 'phone':
							if (true) {
								$ok[]=etpl( array('field'=>$this->fielddescriptions[$field]) ,$this->lng['field_valid_bad']);							
								$this->error[$field]=1;
							}
							break;
						case 'postcode':
							if ( !preg_match( "((^[\d]{2}\-[\d]{3}$)|(^$))",$this->fu[$field] ) ) {
								$ok[]=etpl( array('field'=>$fd) ,$this->lng['field_valid_bad']);
								$this->error[$field]=1;
							}
							break;
						case 'email':
							if ( !preg_match( "((^[\d\w\-_\.]{1,}@([\d\w\-_]{1,}\.)+[\d\w\-_]{1,4}$)|(^$))",$this->fu[$field] ) ) {
								$ok[]=etpl( array('field'=>$fd) ,$this->lng['field_valid_bad']);
								$this->error[$field]=1;
							} 
							break;
						case 'login':
							if (true) {
								$ok[]=etpl( array('field'=>$fd) ,$this->lng['field_valid_bad']);
								$this->error[$field]=1;
							}
							break;
						case 'password':
							if (true) {
								$ok[]=etpl( array('field'=>$fd) ,$this->lng['field_valid_bad']);
								$this->error[$field]=1;
							}
							break;
						case 'city':
							if (true) {
								$ok[]=etpl( array('field'=>$fd) ,$this->lng['field_valid_bad']);
								$this->error[$field]=1;
							}
							break;
						case 'url':
							if (true) {
								$ok[]=etpl( array('field'=>$fd) ,$this->lng['field_valid_bad']);
								$this->error[$field]=1;
							}
							break;
						case 'date':
							preg_match( "(^([\d]{4})\-([\d]{1,2})\-([\d]{1,2})$)", $this->fu[$field], $res );
							if (checkdate($res[2],$res[3],$res[1])) {
							
								$ok[]="Niewłaściwa data - popraw wartość pola <b>{$fd}</b>";
								$this->error[$field]=1;
								
							} elseif (count($res)<4) {
							
								$ok[]="Niewłaściwy format <b>{$fd}</b>";
								$this->error[$field]=1;
								
							}
							break;
						case 'price':
							if ( !preg_match("((^[\d]+(\.[\d]{1,2})?$)||(^$))",$this->fu[$field]) ) {
								$ok[]=etpl( array('field'=>$fd) ,$this->lng['field_valid_bad']);
								$this->error[$field]=1;
							}
							break;
						default:
					}
				}
			}
		}
		return count($ok)>0?$ok:true;
	}

	function insert_relation($rname, $id, $contener) {
		if (count($this->relation[$rname])>0) {

			$rel = &$this->relation[$rname];

			$tdata = $this->sqlconn->fields($rel['rtable']);
			$jord= isset($tdata['jdb_orderkey'])?true:false;
			
//			$order = db_getsqltable($this->sqlconn, "select {$rel['rout']} from {$rel['rtable']} where {$rel['rin']} = '{$id}' ".($jord?" order by jdb_orderkey":''));
			
			$sql = "delete from {$rel['rtable']} where {$rel['rin']}='{$id}' ";
			
			$this->sqlconn->query($sql);
			
			if ($contener!='') {
				$ids = explode(',',$contener);
//				if ($order===false) $order=array();
//				$same = array_intersect($order, $ids);
//
//				$diff = array_diff($ids, $order);
				
//				$good = array_merge($same, $diff);
				
//				if ($jord)
//					$maxord = db_getsinglevalue($this->sqlconn, "select max(jdb_orderkey) from {$rel['rtable']}");
//				else
//					$maxord=0;

				$maxord=0;

				foreach ($ids as $v) {
					$maxord++;
					$sql = "insert into {$rel['rtable']} set {$rel['rin']}='{$id}', {$rel['rout']}='{$v}'".($jord?", jdb_orderkey='{$maxord}' ":'');
					$this->sqlconn->query($sql);
				}
			}
		}
	}


	function op_insert() {
	
		$ok=$this->validate();
		if ($ok!==true) {
			return implode('<br>',$ok).$this->op_edit().$this->show_messages('edit');
		}
		
	
		if ($this->insert_funct!==false) {
			$userf=call_user_func($this->insert_funct, $this->fu);
			if ($userf['result']==false) {
				if (isset($userf['error']) && count($userf['error'])>0) $this->error+=$userf['error'];
				unset($userf['result']);
				unset($userf['error']);
 				$tu=join('<br>',$userf);
				$tu.=$this->op_edit();
				return $tu;
			} else {
				unset($userf['result']);
// 				$this->fu=$userf;
			}
		}
	
	
		switch ($this->fu['op']) {
			case 'insert':
				$sql = " insert into {$this->table} set ";
				break;
			case 'update':
				$sql = " update {$this->table} set ";
				break;
		}


		foreach ($this->fu as $k=>$v) {
			if ($this->fields[$k]['db']==1) $lf[]=$k;
		}

		if (count($lf)>0) {
			foreach ($lf as $v) {
				if ($v!=$this->idname && ( ( $this->fu['op']=='insert' && ($this->orderkey!='' || $v!=$this->orderkey)) || $this->fu['op']!='insert') ) $sql_v[]=" {$v}='{$this->fu[$v]}'";
			}
		
			$sql.=implode(',',$sql_v);
		}
		
		if ($this->fu['op']=='update') $sql.=" where {$this->idname}='{$this->fu[$this->idname]}'";
		$this->sqlconn->query($sql);
		if ($this->fu['op']=='insert') {
			$lastid=$this->sqlconn->lastID();
			$this->fu[$this->idname]=$lastid;
			if ($this->orderkey!='') {
				$max = db_getsinglevalue($this->sqlconn, "select max({$this->orderkey}) from {$this->table}");
				$max+=1;
				$sql=" update {$this->table} set {$this->orderkey} = '{$max}' where {$this->idname}= '{$lastid}' ";
				$this->sqlconn->query($sql);
			}

			$fotki=glob($this->foto_details['upload_dir'].$this->table.'_'.$this->fu['jdb_tmp_id'].'_*.*');

			if ( $fotki!==false && count($fotki)>0) {
				foreach ($fotki as $v) {
					$ext=file_ext($v);
					rename($v, $this->foto_details['upload_dir'].$this->table.'_'.$lastid.'_'.uniqid(time()).'.'.$ext);
				}
			}
		}
		
		if (count($this->relation)>0) {
			foreach ($this->relation as $k=>$v) {
				$this->insert_relation($k, $this->fu[$this->idname], $this->fu[$k.'_contener']);
			}
		}
		
		if ($this->afterinsert_funct!==false)
			call_user_func($this->afterinsert_funct, $this->fu );
		return $this->op_show();
		
	}

	
	function op_movedown() {
		$order=db_getsinglevalue($this->sqlconn, " select {$this->orderkey} from {$this->table} where {$this->idname}='{$this->fu[$this->idname]}' ");
		
		$eq = strtoupper($this->orderdesc)=='DESC'?'<=':'>=';
		$desc = strtoupper($this->orderdesc)=='DESC'?'DESC':'ASC';

		
		$sql=" select * from {$this->table} where ".($this->where!=''?"{$this->where} and ":'')." {$this->orderkey}{$eq}'{$order}' order by {$this->orderkey} {$desc} limit 2";
		$this->sqlconn->query($sql);
		if ($this->sqlconn->numRows()==2) {
			$row=$this->sqlconn->fetchRowassocc();
			$id_up=$row[$this->idname];
			$order_up=$row[$this->orderkey];

			$row=$this->sqlconn->fetchRowassocc();
			$id_down=$row[$this->idname];
			$order_down=$row[$this->orderkey];

			unset($sql);
			$sql[]=" update {$this->table} set {$this->orderkey}='{$order_up}' where {$this->idname}='{$id_down}' ";
			$sql[]=" update {$this->table} set {$this->orderkey}='{$order_down}' where {$this->idname}='{$id_up}' ";
			foreach ($sql as $v) {
				$this->sqlconn->query($v);
			}

		}
	}

	function op_moveup() {
		$order=db_getsinglevalue($this->sqlconn, " select {$this->orderkey} from {$this->table} where {$this->idname}='{$this->fu[$this->idname]}' ");
		
		$desc = strtoupper($this->orderdesc)=='DESC'?'ASC':'DESC';
		$eq = strtoupper($this->orderdesc)=='DESC'?'>=':'<=';

		$sql=" select * from {$this->table} where ".($this->where!=''?"{$this->where} and ":'')." {$this->orderkey}{$eq}'{$order}' order by {$this->orderkey} {$desc} limit 2";

		$this->sqlconn->query($sql);
		if ($this->sqlconn->numRows()==2) {
			$row=$this->sqlconn->fetchRowassocc();
			$id_up=$row[$this->idname];
			$order_up=$row[$this->orderkey];

			$row=$this->sqlconn->fetchRowassocc();
			$id_down=$row[$this->idname];
			$order_down=$row[$this->orderkey];

			unset($sql);
			$sql[]=" update {$this->table} set {$this->orderkey}='{$order_up}' where {$this->idname}='{$id_down}' ";
			$sql[]=" update {$this->table} set {$this->orderkey}='{$order_down}' where {$this->idname}='{$id_up}' ";

			foreach ($sql as $v) {
				$this->sqlconn->query($v);
			}

		}
	}
	
	
	function op_multiorder2() {

// 		$desc = strtoupper($this->orderdesc)=='DESC'?'ASC':'DESC';
		$eq = strtoupper($this->orderdesc)=='DESC'?'max':'min';
		$eq2 = strtoupper($this->orderdesc)=='DESC'?'>=':'<=';
		$qe = $eq=='max'?'min':'max';
		

		$where=$this->where!=''? " and {$this->where} ":'';
		if ($this->fu['multiusersort']!='') {
			$tmp='';
			if (is_array($this->fu['dellist']) && count($this->fu['dellist'])>0 )
				$tmp=@join(',', array_keys($this->fu['dellist']));
/*			reset($this->fu['dellist']);
			for ($i=0; $i<count($this->fu['dellist']); $i++) {
				$tmp.=key($this->fu['dellist']);
				if ($i<count($this->fu['dellist'])-1) {
					$tmp.=',';
					next($this->fu['dellist']);
				}
			}
*/
			$this->sqlconn->query("update {$this->table} set {$this->orderkey}=0 where id in ({$tmp})");
			echo $where;
			$ordid=db_getsinglevalue($this->sqlconn, "select {$this->orderkey} from {$this->table} where {$this->orderkey}>0 {$where} order by {$this->orderkey} {$this->orderdesc} limit ".intval($this->fu['newpoz']-1).",1");
echo $ordid;
			if (count($ordid)>0 && $ordid!==false) {
				$this->sqlconn->query("update {$this->table} set {$this->orderkey}={$this->orderkey}+".count($this->fu['dellist'])." where {$this->orderkey}{$eq2}{$ordid}");
				echo "update {$this->table} set {$this->orderkey}={$this->orderkey}+".count($this->fu['dellist'])." where {$this->orderkey}{$eq2}{$ordid}";
			} else {
				$ordid=db_getsinglevalue($this->sqlconn, "select {$eq}({$this->orderkey}) as {$this->orderkey} from {$this->table}");
				$ordid=$eq=='max'?$ordid-1:$ordid+1;
			}
			$licznik=0;
			foreach ($this->fu['dellist'] as $k=>$v) {
				$this->sqlconn->query("update {$this->table} set {$this->orderkey}=".intval($ordid+$licznik)." where id={$k}");
				$licznik++;
			}
			$min=db_getsinglevalue($this->sqlconn, "select {$qe}({$this->orderkey}) as {$this->orderkey} from {$this->table} ");
			$min=$min;
			if ($min>1) $this->sqlconn->query("update {$this->table} set {$this->orderkey}={$this->orderkey}-{$min}+1 ");
		}
	}

	function op_multiorder() {
		
		$ids = $this->fu['dellist'];
		$npoz = $this->fu['newpoz'];
		
		$ordd = db_getsinglevalue($this->sqlconn, "select {$this->orderkey} from {$this->table} order by {$this->orderkey} {$this->orderdesc} limit ".($npoz-1)." ,1");
		echo "{$ordd}<br>";
		
		$skeys = @implode(', ', array_keys($ids) );
		
		$min_ids = db_getsinglevalue($this->sqlconn, "select min({$this->orderkey}) from {$this->table} where id in ({$skeys}) ");

		$this->sqlconn->query("update {$this->table} set {$this->orderkey} = {$this->orderkey}+".(count($ids)+1)." where {$this->orderkey}>{$ordd} ");
		
		$licznik=0;
		foreach ($ids as $k=>$v) {
			$tmp_ord = $ordd + $licznik;
			$this->sqlconn->query("update {$this->table} set {$this->orderkey}={$tmp_ord} where id='{$k}' ");
		}

	}
	
	function op_show() {

// 		$this->multiorder=0;
		$where=$this->where!=''? " where {$this->where} ":'';
		
		$sql_count="select count(*) ";
		$sql_curr="select * ";
		$sql_main = " from {$this->table} {$where}";
		$sql_order = " order by {$this->orderby} {$this->orderdesc}";
		$sql_limit=$this->limit!==false?" limit {$this->offset}, {$this->limit} ":'';
		
		$this->allrows=db_getsinglevalue( $this->sqlconn, $sql_count.$sql_main );
		
		$res=db_getsqltable( $this->sqlconn, $sql_curr.$sql_main.$sql_order.$sql_limit );
		if ($res===false) {
			$res=array();
		} else {
// sprawdzanie czy sa relwin1n
			foreach ($this->fields as $k=>$v) {
				if ($v['type']=='relwin1n') {
					$this->fields[$k]['data']=db_getbyindex($this->sqlconn, "select {$v['relation']['rid']}, {$v['relation']['rs']} from {$v['relation']['rtable']}");
					if ($this->fields[$k]['data']===false) $this->fields[$k]['data']=array();
					if ($v['relation']['notnull']=='null') $this->fields[$k]['data']=array('0'=>' -- brak --') + $this->fields[$k]['data'];
				}
			}

		}
		$this->numrows=count($res);
		
		if (count($this->foto_details)>0) {
			
			array_unshift($this->toshow, '-');
			array_unshift($this->toshow_size, 15);

			$tmp=glob( $this->foto_details['upload_dir'].$this->table.'_*.*');

			$fotki=array();

			if ( $tmp!==false && count($tmp)>0 ){
				foreach ( $tmp as $v ) {
					preg_match("(\_([\d]+)\_)",$v, $reg);

					$fotki[$reg[1]]=1;
				}
			}
			unset($tmp);
			
		}


		if ( count($this->active)>0) {
			array_unshift($this->toshow, '-');
			array_unshift($this->toshow_size, 11);
		}

		if ($this->orderkey == $this->orderby ) {
			array_unshift($this->toshow, '-');
			array_unshift($this->toshow_size, 36);
		}
		
		if ($this->multiorder==1) {
			array_unshift($this->toshow,'-');
			array_unshift($this->toshow_size, 11);
		}

		if ($this->multiorder==1 || $this->lp==1) {
			array_unshift($this->toshow,'lp');
			array_unshift($this->toshow_size, 25);
		}

		if ( count($this->operation)>0 ) {
			$this->toshow_size[count($this->toshow)]=57*count($this->operation);
			array_push($this->toshow, '-operacje-');
		}
		
		$tb= new easytable('et','et_noneaction','et');
		$tb->opentable();
		$tb->setclass_once('et_header');
		$tb->row();
		
		foreach ($this->toshow as $k=>$v) {
			if ($this->fields[$v]['type']!='hidden')
			$tb->cell($this->fielddescriptions[$v]!='' ? $this->fielddescriptions[$v] : ucfirst(str_replace('_',' ',$v)) ,( $this->toshow_size[$k]!=''?"width={$this->toshow_size[$k]} ":'').'align=center' );
		}
		 
		foreach ($res as $k=>$val) {

			if ($this->show_funct!==false) $val=call_user_func($this->show_funct, $val);
			$bgcolor='';
			if ($this->bg_funct!==false) $bgcolor=call_user_funct($this->bg_funct, $val);

			if ($this->readonly!==false && $val[$this->readonly]=='y') $bgcolor='#ff9090';

			if ($bgcolor!='') $bgcolor=" bgcolor={$bgcolor} ";
			$tb->row($bgcolor);
			// ordekeje
			if ($this->multiorder==1 || $this->lp==1) {
				$tb->cell( $k+$this->offset+1 );
			}
			if ($this->multiorder==1) {
				$tb->cell( jform_checkbox("dellist[{$val['id']}]",'') );
			}
			if ($this->orderby==$this->orderkey) {
				if (count($this->operation_move)>0) {
					$oper='';
					foreach ($this->operation_move as $v) {
						$v['link']=$this->make_link(etpl($val, $v['link']));
						$oper.=" <a href=\"{$v['link']}\">{$v['name']}</a> ";
					}
					$tb->cell($oper);
				}
			}

			// aktywki
			if ( count($this->active)>0) {
				if ($val[$this->active['name']]=='y') {
					$img='gfx/active.gif';
				} else {
					$img='gfx/deactive.gif';
				}
				$tb->cell("<img  title=\"Kliknij aby zmienić\" alt=\"kliknij aby zmienić\" style=\"cursor:pointer;\" id=\"{$this->active['name']}_{$val[$this->idname]}_ajax_switch_img\" src=\"{$img}\" onclick=\"ajax_switch('{$this->table}','{$this->active['name']}','{$this->idname}','{$val[$this->idname]}','gfx/active.gif','gfx/deactive.gif')\">");
			}
/*
			if ( count($this->active)>0) {
				if ($this->readonly!==false && $val[$this->readonly]=='y') $tb->cell($this->operation_active[ array_search( $val[$this->active['name']],$this->active) ]['name']);
				else {
					$link=$this->make_link( etpl( $val, $this->operation_active[ array_search( $val[$this->active['name']],$this->active) ]['link'] ) );
					$tb->cell( "<a href=\"{$link}\">{$this->operation_active[ array_search( $val[$this->active['name']],$this->active) ]['name']}</a>" );
				}
			}
*/
			
			// fotki
			if ( count($this->foto_details)>0 ) {
				if ( $fotki[$val[$this->idname]]==1 ) $tb->cell("<img src=\"gfx/foto.gif\" border=0>");
				else $tb->cell('');
			}
			
			// tuszouy
			foreach ($this->toshow as $k=>$v) {
				if ( !in_array($v, array('-','-operacje-','lp') ) )
				switch ( $this->fields[$v]['type']) {
					case 'ajax_switch':
						if ($val[$v]=='y') {
							$img=$this->imgs['ajax_switch_y'];
						} else {
							$img=$this->imgs['ajax_switch_n'];
						}
						$tb->cell("<img style=\"cursor:pointer;\" title=\"Kliknij aby zmienić\" alt=\"kliknij aby zmienić\" id=\"{$v}_{$val[$this->idname]}_ajax_switch_img\" src=\"{$img}\" onclick=\"ajax_switch('{$this->table}','{$v}','{$this->idname}','{$val[$this->idname]}','{$this->imgs['ajax_switch_y']}','{$this->imgs['ajax_switch_n']}')\">",' align=center '.($this->toshow_size[$k]!=''?"width={$this->toshow_size[$k]}":''));
						break;
					case 'hidden':
						break;
					case 'combo':
					case 'relwin1n':
					case 'select':
					case 'enum':
						$tb->cell( $this->fields[$v]['data'][$val[$v]] , $this->toshow_size[$k]!=''?"width={$this->toshow_size[$k]}":'');
						break;
					case 'htmlhidden':
					default:
						$tb->cell ( $val[$v] ,  $this->toshow_size[$k]!=''?"width={$this->toshow_size[$k]}":'');
				}

			}
			if (count($this->operation)>0) {
				$oper='';
				foreach ($this->operation as $v) {
					if ($this->readonly!==false && $val[$this->readonly]=='y') {
						$oper.="";// {$v['name']} ";
					} else {
						$v['link']=$this->make_link(etpl($val,$v['link']));
						if ($v['confirm']==1) $oper.="<a href=\"#\" onclick=\"check('{$v['link']}','Jesteś pewien?');\">{$v['name']}</a>";
						else $oper.=" <a href=\"{$v['link']}\">{$v['name']}</a> ";
					}
				}
				$tb->cell($oper);
			}

		}

		$tu.=$tb->show();


		$oper='';
		if (count($this->mainoperation)>0) {
			$oper='';
			foreach ($this->mainoperation as $k=>$v) {
				$v['link']=$this->make_link($v['link']);
				$oper.=" <a href=\"{$v['link']}\">{$v['name']}</a> ";
			}
		}


		$tb=new easytable();
		$tb->setsimple();
		$tb->opentable();

		if ($oper!='') {
			$tb->row();
			$tb->cell($oper,'align=right');
		}

		$tb->row();
		$tb->cell($tu);

		if ($oper!='') {
			$tb->row();
			$tb->cell($oper,'align=right');
		}
		
		

		$tu=jform_open($_SERVER['PHP_SELF']);		
		$tu.=$tb->show();

		if ($this->multiorder==1) {
			$tu.="<fieldset style=\"width:200px;\"><legend>zaznaczone:</legend>";
			
			$tu.="<br><input type=\"hidden\" name=\"op\" value=\"multiorder\"><input type=submit name=\"multiusersort\" value=\"Przenieś na pozycję\"> <select name=newpoz>";

			for ($i=1; $i<=$this->allrows; $i++) {
				$tu.="<option value=\"{$i}\">{$i}";
			}

			$tu.="</select>";
			
			
			$tu.="</fieldset>";
		}
		
		$tu.=jform_close();

		if ($this->simpleadd==true) {
			$add='<fieldset><legend><b>DODAJ NOWY WPIS</b></legend>'.$this->op_edit().'</fieldset>';
		} else $add='';

		$tu=$add.$this->op_showoffsetlink().$tu.$this->op_showoffsetlink();
		// MAIN OPERATION
		
		return $tu;
		
	}

	function show_messages($op) {
		if (count($this->messages)>0) {
			foreach ( $this->messages as $k=>$v) {
				if ($k=='all' || strpos($k, $op)!==false) $msg[]=$v;
			}

			if (count($msg)>0) {
			
				$tb=new easytable();
				$tb->setsimple();
				$tb->opentable();
				foreach ($msg as $smsg) {
					foreach ($smsg as $v) {
						$tb->row();
						$tb->cell("<img src=\"gfx/msg_warning.gif\" border=0 align=absmiddle> <b style=\"font-size:15px;\">{$v['title']}</b>");
						$tb->row();
						$tb->cell($v['msg']);
					}
				}
				
				$tu.=$tb->show();
			
			}
				
		}
		return $tu;
	}
	
	function op_active($a) {
		
		$sql = " update {$this->table} set {$this->active['name']}='".($a=='activ'?$this->active['y']:$this->active['n'])."' where {$this->idname}='{$this->fu[$this->idname]}' ";

		$this->sqlconn->query($sql);
		
	}
	
	function canceldel() {
		if ($this->fu['jdb_tmp_id']!='') {
			$f=glob($this->foto_details['upload_dir'].$this->table.'_'.$this->fu['jdb_tmp_id'].'_*.*');
			if ( $f!==false && count($f)>0) {
				foreach ($f as $v) {
					@unlink($v);					
				}
			}
		}
	}
	
	function operation($fu) {
		if ($fu=='') $this->fu=$_REQUEST;
		else $this->fu=$fu;

		if (in_array($this->fu['op'], $this->deniedoperation)) $this->fu['op']='show';

		switch ($this->fu['op']) {
			case 'canceldel':
				$this->canceldel();
				$tu=$this->op_show();
				break;
			case 'add':
				$tu=$this->op_edit();
				break;
			case 'edit':
				$tu=$this->op_edit();
				break;
			case 'del':
				$tu=$this->op_delete();
				break;
			case 'insert':
			case 'update':
				if ( $this->fu['add_foto_btn'] !='' || $this->fu['del_foto_btn']!='' ) {
					if ($this->fu['add_foto_btn']!='') $this->upload_foto();
					if ($this->fu['del_foto_btn']!='') $this->del_foto();
					$tu=$this->op_edit();
				} else {
					$tu=$this->op_insert();
				}
				
				break;
			case 'up':
				$tu=$this->op_moveup();
				$tu.=$this->op_show();
				break;
			case 'down':
				$tu=$this->op_movedown();
				$tu.=$this->op_show();
				break;
			case 'multiorder':
				$tu.=$this->op_multiorder();
				$tu.=$this->op_show();
				break;
			case 'activ':
			case 'deactiv':
				$tu=$this->op_active($this->fu['op']);
				$tu.=$this->op_show();
				break;
			case 'show':
			default:
				$tu=$this->op_show();
		}

		$op=$this->fu['op']!=''?$this->fu['op']:'show';
		
		$tu.=$this->show_messages($op);
		
		return $tu;
		
		
	}	
	
}
