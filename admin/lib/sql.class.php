<?php
// jdb_sql_class
// only mysql
// v.02.01
// joker@joker.art.pl



		
	class SQL {
		
		
		var $dbhost;
		var $dbport;
		var $dbname;
		var $dbuser;
		var $dbpass;
		var $conn;
		var $result;
		var $logs=true;
		var $logfile='logs/sql_log.txt';
		
		
		function SQL($dbname, $dbuser, $dbpass, $dbhost, $dbport='') {
		
			$this -> dbhost = $dbhost.($dbport!=''?":{$dbport}":'');
			$this -> dbport = $dbport;
			$this -> dbname = $dbname;
			$this -> dbuser = $dbuser;
			$this -> dbpass = $dbpass;
			if ( function_exists('save_log') && _LOG=='on' ) $this->logs=true;
			else $this->logs=false;
		
		}
		
		
		function connect() {
		
			$this -> conn = @mysql_connect($this -> dbhost , $this -> dbuser, $this -> dbpass);
		
			if (!$this -> conn) {
				$this->log();
				return false;
			} else {
				if (!@mysql_select_db($this -> dbname, $this -> conn)) {
					$this->log();
					return false;
				}
				else
					return true;
			}
		
		}
		
		function log($query='') {
			if ($this->logs===true) save_log($this->logfile, "[".date('Y-m-d H:i:s')."] ".@mysql_error($this->conn).($query!=''?"
{$query}
":''));
		}
		
		function close() {
			if (gettype($this -> conn) == "resource") {
				if (!@mysql_close($this -> conn)) {
					$this->log();
				}
			}
		
		}
		
		
		function query($query) {

			//if (_WORKTYPE=='test' && _SQLQUERY_REPORTING=='on') jdb_error_box('SQL QUERY',$query,'info');

			if (gettype($this -> conn) == "resource") {
				
				$this -> result = mysql_query($query, $this -> conn);
		
				if (!$this -> result) {

					$this->log($query);
					//$this -> close();
		
				}
		
				else 
					return true;
			}
		
			else {
				$this -> close();
			}
		
		}
		
		
		function query_count($query) {
		
			if (gettype($this -> conn) == "resource") {
				$this -> result = @mysql_query($query, $this -> conn);
				if ($this->result!==false) return $this -> result;
				else $this->log();
			}
			else {
				$this->log();
				$this -> close();
			}
		
		}
		
		
		function numRows() {
			
			if (gettype($this -> result) == "resource") {
				$numrows=@mysql_num_rows($this -> result);
				if ($numrows < 0) {
					$this->log();
				} else {
					return $numrows;
				}
			}
		
		}
		
		
		function fetchRow() {
			
			if (gettype($this -> result) == "resource")
				$tu=@mysql_fetch_row($this -> result);
				if ($tu!==false) return $tu;
				else $this->log();
		
		}
		
		function fetchRowassocc() {
			
			if (gettype($this -> result) == "resource")
				$tu=@mysql_fetch_array($this -> result, MYSQL_ASSOC);
				if ($tu!==false) return $tu;
				else $this->log();
		}
		
		
		function affectedRows() {
			
			$tu=@mysql_affected_rows($this -> conn);
			if ($tu!==false) return $tu;
			else $this->log();
		
		}
		
		
		function lastID() {
			
			$tu=@mysql_insert_id($this -> conn);
			if ($tu!==false) {
				return $tu;
			} else {
				$this->log();
			}
		
		}
		
		function fields($tname) {
			$row=array();
			$res=@mysql_query(" SHOW FULL FIELDS FROM {$tname}", $this->conn);
			if ($res!==false) {
				$cnt=mysql_num_rows($res);
				for ($i=0; $i<$cnt; $i++) {
					$r=@mysql_fetch_assoc($res);
// print_r($r);
					if ($r===false) $this->log();
					if (strpos($r['Type'],'(')!==false) {
						$type=substr($r['Type'], 0, strpos($r['Type'],'('));
						$enum=substr($r['Type'], strpos($r['Type'], '(')+1, strpos($r['Type'],')')-strpos($r['Type'], '(')-1 );
					} else $type=$r['Type'];
					$row[$r['Field']]['type']=$type;
					$row[$r['Field']]['default']=$r['Default'];
					$row[$r['Field']]['extra']=$r['Extra'];
					$row[$r['Field']]['null']=$r['Null'];
					$row[$r['Field']]['key']=$r['Key'];
					if ($type!='enum') $row[$r['Field']]['len']=$enum;
					else $row[$r['Field']]['data']=explode(',',str_replace("'","",$enum));
				}
				return $row;
			} else $this->log();
		}
		
		
	}


if (!isset($sqlconn)) {       
	$GLOBALS['sqlconn']=new SQL(_DBNAME,_DBUSER,_DBPASS,_DBHOST, _DBPORT);
	if ($GLOBALS['sqlconn']->connect()!==true) {
		//jdb_error_box('B��D PO��CZENIA Z BAZ� DANYCH!','Sprawd� parametry po��czenia z baz� w plikach konfiguracyjnych lub skontaktuj si� z administratorem');
		die('Brak po��cznia z baz�');
	}
}


?>
