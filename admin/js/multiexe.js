function $(id) {
    return document.getElementById(id);
}

function show_wait(idel) {
	if ( $(idel).src.indexOf('indicator.gif') == -1 )
		$(idel).src='gfx/indicator.gif';
}

function show_ok(idel) {
	$(idel).src='gfx/roz_ax_ok.gif';
}

function show_nook(idel) {
	$(idel).src='gfx/roz_ax_nook.gif';
}

function update_progressbar(progresid, currrow, allrow) {
	perc = Math.round((currrow*100)/allrow);
	if (perc>100) perc=100;
	document.getElementById(progresid+'_percent').style.display='block';
	document.getElementById(progresid+'_txt').style.display='block';
	document.getElementById(progresid+'_percent').style.width=perc+'%';
	document.getElementById(progresid+'_txt').innerHTML=perc+'%';
}

function ajax_multiexec(aarr,params,idroz, backlink) {

	if (backlink=='') backlink='index.php'; 
	var counter = aarr.length;
	if (aarr.length>0) {
		el = aarr.shift();
		elp = params.shift();

		if (document.getElementById('progressbarek_'+el+'_'+counter)!=undefined) {
			progresstype=true;
		} else { 
			progresstype=false;
		}
		
	  advAJAX.get({
	      url : "ME_ax_operation.php?op="+el+"&"+elp+"&id="+idroz,
	      onInitialization : function() {
	      	if (progresstype==true && idroz>0) {
	      	} else {
						show_wait('img_'+el+'_'+counter);
					}
	      },
	      onError : function() {
	      	show_nook('img_'+el+'_'+counter);
	      },
        onComplete: function () {
        	if (progresstype==false)
	      	show_ok('img_'+el+'_'+counter);
	      },
	      onSuccess : function(obj) {
		  
	      	if ( document.getElementById('progressbarek_'+el+'_'+counter)!=undefined ) {
						resp=obj.responseText;
		        resptxt = resp.split('#');
						currow = resptxt[0];
						allrow = resptxt[1];	      
		        $('reps_'+el+'_'+counter).innerHTML+=resptxt[2];
						update_progressbar('progressbarek_'+el+'_'+counter,currow,allrow);
						if (parseInt(currow)>=parseInt(allrow)) {
							show_ok('img_'+el+'_'+counter);
							if (aarr.length>0) {
								ajax_multiexec(aarr, params,resptxt[3],backlink);
							} else {
					  		$('update_btn').setAttribute('onclick',"location.replace('"+backlink+"');");
					  		$('update_btn').setAttribute('value',"Wróć");
					  	}
						} else {
							aarr.unshift(el);
							params.unshift(elp);
							ajax_multiexec(aarr,params,currow+';',backlink);
						}		
	      	} else {
		        show_ok('img_'+el+'_'+counter);
		        resp = obj.responseText;
		        resptxt = resp.split('#');
		        if ((resptxt[0]=='error') || (resptxt[0]=='stop')) {
		        	show_nook('img_'+el+'_'+counter);
		        	resptxt[1]='<span class="error">'+resptxt[1]+'</span>';
		        } 
		        $('reps_'+el+'_'+counter).innerHTML=resptxt[1];
		        if (resptxt[0]!='stop') ajax_multiexec(aarr,params,idroz,backlink);
		        else {
					  	$('update_btn').setAttribute('onclick',"location.replace('"+backlink+"');");
					  	$('update_btn').setAttribute('value',"Wróć");
		        }
		      }
	      }
	  });
  } else {
  	$('update_btn').setAttribute('onclick',"localtion.replace('"+backlink+"');");
  	$('update_btn').setAttribute('value',"Wróć");
  }
}


function multiexec( idroz, operation, params, backlink ) {

	aarr=operation.split(',');
	paramsarr = params.split(',');
	
 	$('update_btn').setAttribute('value',"czekaj...");
 	$('update_btn').setAttribute('onclick',"return false;");
 	$('update_btn').style.disabled=true;

	ajax_multiexec(aarr,paramsarr,idroz,backlink);
	
}
