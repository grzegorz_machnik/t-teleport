var target    = null;
var date_ctrl = null;
var curr_date = null;
var from_date = null;
var till_date = null;
var dt_format = "Y-m-d";
var calendar  = null;  // Calendar window

function calendarShow(_date_ctrl, _from_date, _till_date, el)
{
  _target = $(_date_ctrl);
  if (_target == null || !_target)
    return;
  target = $(_date_ctrl+'_day');

  date_ctrl = _date_ctrl;
  curr_date = $(date_ctrl+'_year').value+'-'+$(date_ctrl+'_month').value+'-'+$(date_ctrl+'_day').value
  from_date = _from_date
  till_date = _till_date;
  var left = el.clientX;
  var top = el.clientY+66;
  var features = ',left='+left+',top='+top;
  
  if (calendar != null && !calendar.closed)
    calendar.close();
  calendar = window.open('calendar.htm', 'Kalendarz', 'channelmode=0,directories=0,fullscreen=0,location=0,menubar=0,resizable=0,scrollbars=0,status=0,titlebar=0,toolbar=0,dependent=yes,width=200,height=132'+features);
  if (calendar.opener == null)
    calendar.opener = self;
  calendar.focus();
}

function calendarSetFormat(fmt)
{
  dt_format = fmt;
}

function calendarSetDate(str_year, str_month, str_day)
{
  if (date_ctrl != null && date_ctrl)
  {
    $(date_ctrl).value = str_year+'-'+str_month+'-'+str_day;
    $(date_ctrl+'_year').value=str_year;
    $(date_ctrl+'_month').value=str_month;
    $(date_ctrl+'_day').value=str_day;
    if (self.onCalendarSetDate)
      onCalendarSetDate();
	}
}
