var clickcnt_upload = 0;
var clickcnt_create = 0;
function $(id) {
    return document.getElementById(id);
}

function show_flyelement(idel) {
	$(idel).style.display="block";
}

function hide_flyelement(idel) {
	$(idel).style.display="none";
}

function ajax_createfolder(ffname) {
		
    advAJAX.get({
        url : "ax_files_operation.php?op=createfolder&ffn="+ffname,
        onInitialization : function() {
        	show_flyelement('ajaxwaitmodule');
        },
        onComplete: function () {
        	hide_flyelement('ajaxwaitmodule');
        },
        onError : function() {
        	alert('Wystąpił błąd!');
        },
        onSuccess : function(obj) {
        	switch (obj.responseText) {
        		case 'ok':
        			link = window.location.toString();
        			link = link.substr(0, link.indexOf('?'));
							window.location.replace(link+'?dir='+ffname.substr(5));
        			break;
        		case 'error':
        			alert('Nie udało się utworzyć katalogu');
        			break;
        		case 'null':
        		default:
        			break;
        	}
        }
    });
}

function ajax_deletefile(ffname) {
		
    advAJAX.get({
        url : "ax_files_operation.php?op=deletefile&ffn="+ffname,
        onInitialization : function() {
        	show_flyelement('ajaxwaitmodule');
        },
        onComplete: function () {
        	hide_flyelement('ajaxwaitmodule');
        },
        onError : function() {
        	alert('Wystąpił błąd!');
        },
        onSuccess : function(obj) {
        	switch (obj.responseText) {
        		case 'ok':
							window.location.reload();
        			break;
        		case 'error':
        			alert('Nie udało się skasować pliku');
        			break;
        		case 'null':
        		default:
        			break;
        	}
        }
    });
}


function deleteFile(ffname) {
	if (confirm('Jesteś pewien?')) ajax_deletefile(ffname);
}

function uploadResult(res) {
	switch (res) {
		case -1:
			alert('Nie znaleziono pliku!');
			break;
		case 0:
			alert('Nie udało się wgrać pliku na serwer!');
			break;
		case 1:
			window.location.replace(top.location);
	}
  hide_flyelement('ajaxwaitmodule');
}

function uploadfile(idform, idfile, vdir) {
	fn = $(idfile).value;
	if (fn!='') {
    show_flyelement('ajaxwaitmodule');
    frm = $(idform);
    frm.action='ax_files_operation.php?op=uploadfile&dir='+vdir;
    frm.submit();
  } else {
  	clickcnt_upload++;
  	if (clickcnt_upload>1 & clickcnt_upload%5==0) {
			alert('Zgłupiałeś?');
		}
  	return false;
  }
  return false;
}

function createFolder(idel,ffc) {
	d=$(idel);
	if (d.value!='') {
		ajax_createfolder(ffc+'/'+d.value);
		d.value='';
	} else {
		clickcnt_create++;
		if (clickcnt_create>1 && clickcnt_create%3==0) {
			alert('Nie klikaj jak idiota! Czytaj ze zrozumieniem: musisz wpisać nazwę katalogu!');
		} else {
			alert('Musisz wpisać nazwę katalogu!');
		}
	}
	return false;
}
