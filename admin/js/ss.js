function $(id) {
    return document.getElementById(id);
}

function parseRecords(xml, nid) {
    it = xml.getElementsByTagName(nid);
		result = "";
    for (i = 0; i < it.length; i++) {
    	if ( $(it[i].firstChild.nodeValue)!=undefined )
    		$(it[i].firstChild.nodeValue ).checked=true;
    }
    return result;
}

function show_flyelement(idel) {
	$(idel).style.display="block";
}

function hide_flyelement(idel) {
	$(idel).style.display="none";
}

function ajax_switch_image(idel, im) {
	idel.src = im;
}

function ajax_switch(tn, fn, idn, idv,imgsrc1,imgsrc2) {
		
		d = $(fn+'_'+idv+'_ajax_switch_img');
		if (d.src.substr(d.src.length-imgsrc1.length)==imgsrc1) {
			im = imgsrc2;
			fv='n';
		} else {
			im = imgsrc1;
			fv='y';
		}
		
    advAJAX.get({
        url : "ajax_switch.php?tn="+tn+"&fn="+fn+"&fv="+fv+"&idn="+idn+"&idv="+idv,
        onInitialization : function() {
        	show_flyelement('ajaxwaitmodule');
        },
        onComplete: function () {
        	hide_flyelement('ajaxwaitmodule');
        },
        onError : function() {
        	alert('Wystąpił błąd!');
        },
        onSuccess : function(obj) {
          ajax_switch_image(d, im);
        }
    });
}

function getcurrentselection(rtable, mid, nid, id, item) {
    advAJAX.get({
        url : "ajax_sselect.php?rtable="+rtable+"&mid="+mid+"&nid="+nid+"&id="+id+"&item="+item,
        onInitialization : function() {
        	show_flyelement('ajaxwaitmodule');
        },
        onComplete: function () {
        	hide_flyelement('ajaxwaitmodule');
        },
        onError : function() {
        	alert('Wystąpił błąd!');
        },
        onSuccess : function(obj) {
        	if (itemid == '') 
            parseRecords(obj.responseXML, nid);
        }
    });
}

function setselection(rtable, mid, nid, id, item) {
	getcurrentselection(rtable, mid, nid, id, item);

}

function initSelection(rtable,mid,nid,id) {
	getcurrentselection(rtable,mid,nid,id,'');
	
}
