// joker@joker.art.pl
// la_mo_16.06.06



photowindow_op = null;

function photowindow(url, nazwa, width, height){
  if (photowindow_op) photowindow_op.close();
  width += 18;
  height += 18;
	params = "status=no, statusbar=no, menubar=no, scrollbars=no, resizeable=yes, toolbar=no, location=no, directories=no, left=20, top=20, width=" + width + ", height=" + height;	
  photowindow_op = window.open(url, 'photobig', params);
  photowindow_op.focus();
}


function check(url, co) {
	if (confirm(co)) { 
		window.location.replace(url); 
	} else { 
		return false;
	}
}


function checksubmit(a, co) {
	if (confirm(co)) { 
		a.submit(); 
	} else { 
		return false;
	}
}


function io_onchange(foremka, aa, bb) {
	arr=new Array();
	if (bb.match(',')) arr=bb.split(',');
	else arr.push(bb);
	for (i=0; i<arr.length; i++) {
		if (aa.checked) {
			foremka.elements[arr[i]].disabled=true;
		} else {
			foremka.elements[arr[i]].disabled=false;
		}
	}
}


function openDialogWin(file,name) {
	if (arguments.length>=3) {
		ww=arguments[2]
	} else {
		ww=500;
	}
	if (arguments.length==4) {
		wh=arguments[3]
	} else {
		wh=400;
	}
	leftnw = (window.screen.availWidth - ww )/2;
	topnw = (window.screen.availHeight - wh)/2;
	window.open(file,name, 'toolbar=no,status=no,resizable=yes,scrollbars=yes,scrollable=yes,dependent=yes,top='+topnw+',left='+leftnw+',width='+ww+',height='+wh);
}

function openDialogWinFB(file,name) {
		ww=600;
		wh=400;
	leftnw = (window.screen.availWidth - ww )/2;
	topnw = (window.screen.availHeight - wh)/2;
	window.open(file,name, 'toolbar=no,status=yes,resizable=no,scrollbars=no,scrollable=no,dependent=yes,top='+topnw+',left='+leftnw+',width='+ww+',height='+wh);
}

function openOutFile(file,name) {
	window.open(file,name, 'scrollbars=yes, resizeable=yes, width=600, height=500');
}

function outFileSetSelect(id,val) {
	opener.document.getElementById(id).value=val;
	window.close();
}

function jform_collapse(idel, a, b) {
	ob = document.getElementById(idel);
	img = document.getElementById('jform_collapse_img_'+idel);
	if (ob.style.height==b+'px') {
		ob.style.height=a+'px';
		img.src='gfx/up.gif';
	} else {
		ob.style.height=b+'px';
		img.src='gfx/down.gif';
	}
}

function rel1n_sel(fid, fidv, fs, fsv) {
	opener.document.getElementById(fid).value=fidv;
	opener.document.getElementById(fs).value=fsv;
	window.close();
}

function winjs_url(wtitle, ww,wh, wt, wl, wurl) {

	if (wt==0) {
		wl = (window.screen.availWidth - ww )/2;
		wt = (window.screen.availHeight - wh)/2 -80;
	}
	
	wname = 'nw1'+Math.random();
	win = new Window(wname, {title: wtitle, opacity: 0.95, width:ww, height:wh, top:wt, left:wl, showEffect:Element.show, hideEffect: Effect.SwitchOff, maximizable:true, minimizable:true, closable:true, resizable:true, url: wurl});   
	win.setDestroyOnClose();
	win.show();
}

function winjs_txt(wtitle, ww, wh, wt, wl, wtxt) {
	if (wt==0) {
		wl = (window.screen.availWidth - ww)/2;
		wt = (window.screen.availHeight - wh)/2-80;
	}
	wname = 'nw1'+Math.random();
	win = new Window(wname, {title: wtitle, opacity: 0.95, showEffect:Element.show, hideEffect: Effect.SwitchOff, width:ww, height:wh, top:wt, left:wl, maximizable:true, minimizable:true, closable:true, resizable:true});   
	win.getContent().innerHTML=wtxt;
	win.setDestroyOnClose();
	win.show();
}

function winjs_pure_url(wtitle, ww, wh, wt, wl, wurl) {
	if (wt==0) {
		wl = (window.screen.availWidth - ww )/2;
		wt = (window.screen.availHeight - wh)/2-80;
	}
	wname = 'nw1'+Math.random();
	win = new Window(wname, {title: wtitle, opacity: 0.95, showEffect:Element.show, hideEffect: Effect.SwitchOff, width:ww, height:wh, top:wt, left:wl, maximizable:false, minimizable:false, closable:true, resizable:false, url: wurl});   
	win.setDestroyOnClose();
	win.show();
}

function winjs_pure_txt(wtitle, ww,wh, wt, wl, wtxt) {
	if (wt==0) {
		wl = (window.screen.availWidth - ww )/2;
		wt = (window.screen.availHeight - wh)/2-80;
	}
	wname = 'nw1'+Math.random();
	win = new Window(wname, {title: wtitle, opacity: 0.95, showEffect:Element.show, hideEffect: Effect.SwitchOff, width:ww, height:wh, top:wt, left:wl, maximizable:false, minimizable:false, closable:true, resizable:false});   
	win.setDestroyOnClose();
	win.getContent().innerHTML=wtxt;
	win.show();
}

function setCookie(cookieName,cookieValue,nSec) {
 var today = new Date();
 var expire = new Date();
 if (nSec==null || nSec==0) nSec=3600;
 expire.setTime(today.getTime() + 1000*nSec);
 document.cookie = cookieName+"="+escape(cookieValue)
                 + ";expires="+expire.toGMTString();
}


function collapseGroup(elementid) {
	el=document.getElementById(elementid);
	
	mainelid = el.getAttribute('CONNECT');
	mainel = document.getElementById(mainelid);
	imgon = el.getAttribute('ONACTIVE');
	imgoff = el.getAttribute('ONUNACTIVE');
	imagecollapse = document.getElementById(elementid+'_cimg');
	if (mainel.style.display=='none') {
		setCookie(elementid,'on',3600);
		mainel.style.display='block';
		imagecollapse.src=imgon;
	} else {
		setCookie(elementid,'off',3600);
		mainel.style.display='none';
		imagecollapse.src=imgoff;
	}
}

function collapseElement(elementid) {
	el=document.getElementById(elementid);
	
	if (el.style.display=='none') {
		el.style.display='block';
	} else {
		el.style.display='none';
	}
}

function jdbet_chpass(crypto, dest, src1, src2) {
	
	s1=document.getElementById(src1).value;
	s2=document.getElementById(src2).value;

	if (s1==s2 & s1!='') {
		if (crypto=='md5')
			opener.document.getElementById(dest).value=hex_md5(s1);
		else 
			opener.document.getElementById(dest).value=s1;
		window.close();
	} else if (s1=='') {
		alert('Hasło nie może być puste');
		return false;
	} else if (s1!=s2) {
		alert('Hasło i powtórzenie nie zgadzają się!');
		return false;
	} 
	
}

function chpos( selid, newpos) {
	msel = document.getElementById(selid+'_show');
	if ( msel.selectedIndex != -1 ) {
		if ( ( newpos==-1 && msel.selectedIndex>0) || ( newpos==1 && msel.selectedIndex<msel.options.length-1) ) {
			id = msel.options[msel.selectedIndex].value;
			val = msel.options[msel.selectedIndex].text;
			id2 = msel.options[msel.selectedIndex+newpos].value;
			val2 = msel.options[msel.selectedIndex+newpos].text;
			
			msel.options[msel.selectedIndex].value=id2;
			msel.options[msel.selectedIndex].text=val2;
			msel.options[msel.selectedIndex+newpos].value=id;
			msel.options[msel.selectedIndex+newpos].text=val;
			
			msel.selectedIndex=msel.selectedIndex+newpos;
			
		}
	}
	
	sortContener(selid);
	
}

function sortContener( cname ) {
	scombo = document.getElementById(cname+'_show');
	cont = document.getElementById(cname+'_contener');
	cc = new Array();
	alert(cont.value);
	for (i=0; i<scombo.options.length; i++) {
		cc.push(scombo.options[i].value);
	}
	
	cont.value=cc.join(',');
	alert(cont.value);
}

function fru_lorem(id) {
	$(id).value=$(id).value+'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sodales, mauris ac fermentum pulvinar, ante nulla consequat ipsum, a scelerisque ligula lorem eget sapien. Nam felis sapien, convallis a, tincidunt sed, tincidunt hendrerit, magna. Ut mi velit, tristique et, sodales eget, volutpat at, massa. Mauris magna pede, elementum vitae, cursus eget, lobortis eget, libero. Vestibulum suscipit elit sit amet diam. Mauris vestibulum. Phasellus leo magna, placerat ut, faucibus vel, scelerisque et, augue. Donec mollis rhoncus eros. Suspendisse scelerisque dolor a diam. Ut facilisis eros nec enim. Integer in risus sit amet dolor placerat venenatis. Pellentesque orci.';
	return false;
}
