<style>
 #result_table td{padding:5px; font-size:12px; border-bottom:black 1px solid;}
 .top_t td{font-weight:bold; border-bottom:black 3px solid !important;}
 #sum_table{margin-top:30px;}
 #sum_table td{font-size:13px; padding:5px;}
</style>

<table>
 <tr>
  <td>  
Dotyczy uchwały: <b><?=$uch['nazwa']?></b> z dnia <?=$uch['data']?>  <br><br>
  </td>
  </tr>
  <tr>
   <td>Treść uchwały: <br><?=$uch['tresc']?><br><br></td>
  </tr>
  <tr>
   <td>
    <table id="result_table" cellpadding="0" cellspacing="0">
     <tr class="top_t">
      <td>Numer mieszkania</td><td>Imię</td><td>Nazwisko</td><td>Głos</td><td>Data głosowania</td><td>Data potwierdzenia</td>
     </tr>
     <?foreach($us as $k=>$v):?>
     <tr>
      <td><?=$v['nr_lokalu'];?></td>
      <td><?=$v['imie'];?></td>
      <td><?=$v['nazwisko'];?></td>
      <td><?=$glosy[$v['glos']];?></td>
      <td><?=date('Y-m-d H:i:s',$v['data_glosowania']);?></td>
      <td><?=$v['data_potwierdzenia']!='0'?date('Y-m-d H:i:s',$v['data_potwierdzenia']):'---brak---';?></td>
     </tr>
     <?endforeach;?>
    </table>
   </td>
  </tr>
  <tr>
   <td>
    <table id="sum_table">
    <tr>
      <td></td><td>Głosów</td><td>Udziałów</td>
     </tr>
     <tr>
      <td>Łącznie głosów</td><td><?=$g_sum?></td><td><?=$ud_sum?></td>
     </tr>
     <tr>
      <td>Głosów ZA</td><td><?=$g_1?></td><td><?=$ud_1?></td>
     </tr>
     <tr>
      <td>Głosów PRZECIW</td><td><?=$g_0?></td><td><?=$ud_0?></td>
     </tr>
     <tr>
      <td>Głosów WSRZYMUJĄCYCH SIĘ</td><td><?=$g_2?></td><td><?=$ud_2?></td>
     </tr>
    </table>
   </td>
  </tr>
  
 </table> 