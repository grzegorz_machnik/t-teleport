<?php

function get_image_nocrop($foto, $width, $height, $add='')
{
	if ( file_exists('./../'.$foto) ) {
		$size = @getimagesize('./../'.$foto);
		if ($size!==false) {
			$maxw='';
			if ($size[2]==2) {
			  $fn = "mphoto.php?mode=1&crop={$height}&ymax={$height}&img=./../{$foto}";
				$maxw="";
			} elseif ( in_array( $size[2], array(1,3) )) {
				$fn = './../'.$foto;
				$maxw=" width=\"{$width}\" height=\"{$height}\"";
			}
		}

		$img = "<img src=\"{$fn}\"{$maxw} align=\"center\" {$add} />";
	}
	return $img;
}

function get_image_scalex($foto, $width, $height, $add='')
{
	if ( file_exists('./../'.$foto) ) {
		$size = @getimagesize('./../'.$foto);
		if ($size!==false) {
			$maxw='';
			if ($size[2]==2) {
			  $fn = "/mphoto.php?mode=1&xmax={$width}&img={$foto}";
				$maxw="";
			} elseif ( in_array( $size[2], array(1,3) )) {
				$fn = './../'.$foto;
				$maxw=" width=\"{$width}\" height=\"{$height}\"";
			}
		}

		$img = "<img src=\"{$fn}\"{$maxw} align=\"center\" {$add} />";
	}
	return $img;
}

function get_image($foto, $width, $height, $add='')
{
	if ( file_exists('./../'.$foto) ) {
		$size = @getimagesize('./../'.$foto);
		if ($size!==false) {
			$maxw='';
			if ($size[2]==2) {
			  $fn = "mphoto.php?mode=2&crop={$height}&xmax={$width}&ymax={$height}&img=./../{$foto}";
//				$maxw=" width=\"{$width}\" height=\"{$height}\"";
			} elseif ( in_array( $size[2], array(1,3) )) {
				$fn = './../'.$foto;
				$maxw=" width=\"{$width}\" height=\"{$height}\"";
			}
		}

		$img = "<img src=\"{$fn}\"{$maxw} align=\"center\" border='0' {$add} />";
	}
	return $img;
}

function scale_image($foto, $width, $height)
{
	if ( file_exists('./../'.$foto) ) {
		$size = @getimagesize('./../'.$foto);
		if ($size!==false) {
			if ($size[2]==2) {
			  return "mphoto.php?mode=2&crop={$height}&xmax={$width}&ymax={$height}&img=./../{$foto}";
			}
		}
	}
	return $foto;
}

function get_image_preview($foto, $img_size=360, $add='')
{
	$size = @getimagesize('./../'.$foto);
	if ($size!==false) {
		$max = $size[0]>$img_size?$img_size:$size[0];
		$maxw='';
		if ($size[2]==2) {
			$fn = "mphoto.php?max={$max}&img=./../{$foto}";
			$maxw='';
		} elseif ( in_array( $size[2], array(1,3) )) {
			$fn = './../'.$foto;
			$maxw=$size[0]>$size[1]?" width=\"{$max}\"":" height=\"{$max}\"";
		} else {
			$fn='gfx/file_ico.png';
		}
	} else {
		$fn='gfx/file_ico.png';
	}
	
	$img = "<img src=\"{$fn}\" {$maxw} border='0' align=\"center\" {$add}/>";
	
  return $img;
}

?>