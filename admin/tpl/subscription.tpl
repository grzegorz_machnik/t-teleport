<table style='margin:0px 0px 20px 20px;'>
<tr>
<td style='vertical-align:top;padding:5px;'>
<div style='margin-left:5px;font-weight:bold;'>WYDANIE DRUKOWANE<br/>{$content.content}</div>
<div style='font-size:13px;margin:5px;'>
Numer zamówienia:<br/>{$content.numer_zamowienia}
</div>
{if $content.subscription_kind}
<div style='font-size:13px;margin:5px;'>Rodzaj prenumeraty:<br/>{$content.subscription_kind}</div>
{/if}
<div style='font-size:13px;margin:5px;'>
Data zamówienia:<br/>{$content.data_zamowienia}
</div>                                  
</td>
<td style='vertical-align:top;padding:5px;'>
<table border='1' style='background-color:#EFEFEF;'>
<tr><td style='padding:5px;'>Imię</td><td style='padding:5px;'>{$content.imie}</td></tr>
<tr><td style='padding:5px;'>Nazwisko</td><td style='padding:5px;'>{$content.nazwisko}</td></tr>
<tr><td style='padding:5px;'>Stanowisko</td><td style='padding:5px;'>{$content.stanowisko}</td></tr>
<tr><td style='padding:5px;'>Tel. kontaktowy</td><td style='padding:5px;'>{$content.telefon_kontaktowy}</td></tr>
</table>
</td>
<td style='vertical-align:top;padding:5px;'>  
<table border='1' style='background-color:#EFEFEF;'>
<tr><td style='padding:5px;'>Nazwa firmy</td><td style='padding:5px;'>{$content.nazwa_firmy}</td></tr>  
<tr><td style='padding:5px;'>Ulica</td><td style='padding:5px;'>{$content.ulica}</td></tr> 
<tr><td style='padding:5px;'>Miasto</td><td style='padding:5px;'>{$content.miasto}</td></tr>
<tr><td style='padding:5px;'>Kod pocztowy</td><td style='padding:5px;'>{$content.kod}</td></tr>
<tr><td style='padding:5px;'>Telefon</td><td style='padding:5px;'>{$content.telefon}</td></tr>
<tr><td style='padding:5px;'>Fax</td><td style='padding:5px;'>{$content.fax}</td></tr>
<tr><td style='padding:5px;'>Email</td><td style='padding:5px;'>{$content.email}</td></tr>
<tr><td style='padding:5px;'>WWW</td><td style='padding:5px;'>{$content.www}</td></tr>
<tr><td style='padding:5px;'>Regon</td><td style='padding:5px;'>{$content.regon}</td></tr>
<tr><td style='padding:5px;'>Nip</td><td style='padding:5px;'>{$content.nip}</td></tr>
</table> 
</td><td style='vertical-align:top;padding:5px;'>
<div style='font-size:13px;margin-bottom:5px;'>
Inny adres wysyłki
</div> 
<table border='1' style='background-color:#EFEFEF;'>
<tr><td style='padding:5px;'>Ulica-Inny</td><td style='padding:5px;'>{$content.ulica_inny}</td></tr>
<tr><td style='padding:5px;'>Kod-Inny</td><td style='padding:5px;'>{$content.kod_inny}</td></tr>
<tr><td style='padding:5px;'>Miasto-Inny</td><td style='padding:5px;'>{$content.miasto_inny}</td></tr>
</table>
</td>
<td style='vertical-align:top;padding:5px;'>
<a href='zamowienia.php' style='margin-left:15px;'>[ Zamknij ]</a>
</td>
</tr></table>