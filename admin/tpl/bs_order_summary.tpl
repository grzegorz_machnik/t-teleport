<table cellspacing='0' cellpadding='0'>
<tr>
    <td width="300" style='font-size:12px;border-bottom:1px solid #CDCDCD;padding:5px;'>Tytuł</td>
    <td width="50" style='font-size:12px;border-bottom:1px solid #CDCDCD;padding:5px;'>Cena</td>
    <td width="50" style='font-size:12px;border-bottom:1px solid #CDCDCD;padding:5px;'>Ilość</td>
    <td width="50" style='font-size:12px;border-bottom:1px solid #CDCDCD;padding:5px;'>Rabat</td>
    <td width="50" style='font-size:12px;border-bottom:1px solid #CDCDCD;padding:5px;'>Wartość</td>
</tr>
{foreach from=$content item=v key=k}
<tr>
    <td width="300" style='font-size:12px;vertical-align:top;padding:5px;'>{$v.title}</td>
    <td width="70" style='font-size:12px;vertical-align:top;padding:5px;'>{$v.price}</td>
    <td width="20" style='font-size:12px;vertical-align:top;padding:5px;'>{$v.count}</td>
    <td width="70" style='font-size:12px;vertical-align:top;padding:5px;'>{$v.discount}%</td>
    <td width="70" style='font-size:12px;vertical-align:top;padding:5px;'>{$v.price_all}</td>
</tr>
{/foreach}
</table>
<div class='color' style='font-size:12px;margin:0px 30px 5px 0px;text-align:right;'>Kwota za produkty: {$price_all}</div>
<div class='color' style='font-size:12px;margin:0px 30px 5px 0px;text-align:right;'>{$price_title}: {$price}</div>
<div class='color' style='font-size:12px;margin:0px 30px 5px 0px;text-align:right;'>Kwota do zapłaty: {$price_all_plus}</div>