<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=iso8859-2">
  <title>%SITETITLE%</title>
	<script type="text/javascript" src="js/js.js"></script>
	<script type="text/javascript" src="js/jmain_funct.js"></script>
	<LINK REL="stylesheet" HREF="css/easytable.css" TYPE="text/css">
	<style>
		* {font-size:10px; font-family:verdana;}
		a {text-decoration:none; }
		a.topmenu {color:white; text-decoration:none;}
		a.topmenu:hover {background-color:#808080; color:white;}
	</style>
  </head>
  <body>

%%BEGIN TOPPANEL IF (TOPPANEL)%%
%%END TOPPANEL%%

<table class="relwin1n" width="100%">
%CONTENT%
</table>

%%ITEM RELROW%%
	<Tr>
		<td id=class="activeel" onclick="rel1n_sel('%FIELDID%','%VALUE%','%FIELDSHOW%','%SHOWVALUE%')">%SHOWVALUE%</td>
	</tr> 
%%END RELROW%%

%%ITEM RELROWSELECT%%
	<Tr class="relrowselect">
		<td class="activeel" onclick="rel1n_sel('%FIELDID%','%VALUE%','%FIELDSHOW%','%SHOWVALUE%')">%SHOWVALUE%</td>
	</tr> 
%%END RELROWSELECT%%

%%ITEM RELROW_EMPTY%%
	<tr>
		<td align=center><b>Nie znaleziono żadnych pasujących rekordów!</b></td>
	</tr>
%%END RELROW_EMPTY%%

%%BEGIN FOOT IF (FOOT)%%
%%END FOOT%%

  </body>
</html>
