<table style='margin:0px 0px 20px 20px;'>
<tr>
<td style='vertical-align:top;padding:5px;'>
<div style='margin-left:5px;font-weight:bold;'>KSIĘGARNIA</div>
<div style='font-size:13px;margin:5px;'>
Numer zamówienia:<br/>{$content.numer_zamowienia}
</div>
<div style='font-size:13px;margin:5px;'>
Data zamówienia:<br/>{$content.data_zamowienia}
</div>
</td>
<td style='vertical-align:top;padding:5px;'>

<table border='1' style='background-color:#EFEFEF;'>
<tr><td style='padding:5px;'>Imię</td><td style='padding:5px;'>{$content.imie}</td></tr>
<tr><td style='padding:5px;'>Nazwisko</td><td style='padding:5px;'>{$content.nazwisko}</td></tr>
<tr><td style='padding:5px;'>Województwo</td><td style='padding:5px;'>{$content.wojewodztwo}</td></tr>
<tr><td style='padding:5px;'>Ulica</td><td style='padding:5px;'>{$content.ulica}</td></tr>
<tr><td style='padding:5px;'>Kod pocztowy</td><td style='padding:5px;'>{$content.kod}</td></tr>
<tr><td style='padding:5px;'>Miasto</td><td style='padding:5px;'>{$content.miasto}</td></tr>
<tr><td style='padding:5px;'>Telefon</td><td style='padding:5px;'>{$content.telefon}</td></tr>
</table>

</td>
<td style='vertical-align:top;padding:5px;'>

<table cellspacing='0' cellpadding='0'>
<tr>
<td>
<table border='1' style='background-color:#EFEFEF;'>
<tr><td style='padding:5px;'>Email</td><td style='padding:5px;'>{$content.email}</td></tr>
<tr><td style='padding:5px;'>Nazwa firmy</td><td style='padding:5px;'>{$content.nazwa_firmy}</td></tr>
<tr><td style='padding:5px;'>NIP</td><td style='padding:5px;'>{$content.nip}</td></tr>
<tr><td style='padding:5px;'>WWW</td><td style='padding:5px;'>{$content.www}</td></tr>
</table>
</td>
<td style='vertical-align:top;'>
<a href='zamowienia.php' style='margin-left:15px;'>[ Zamknij ]</a>
</td>
</tr>
</table>

{if $content.uwagi neq ''}
<div style='margin-top:10px;font-size:13px;'>Uwagi do zamówienia:<br/>
{$content.uwagi}
</div>
{/if}
</td>
</tr>

<tr>
<td colspan='3'>
{$summary}
</td>
</tr>

</table>