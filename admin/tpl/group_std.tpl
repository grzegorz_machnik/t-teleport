<div style="width: %WIDTH%; display: block;" class="dialog_simple" >
	<table  class="dialog_header" style="width:100%">
		<tbody>
			<tr >
				<td>
					<table style="width:100%">
						<tbody>
							<tr>
								<td  class="dialog_nw">
									<div class="dialog_nw">
									</div>
								</td>
								<td class="dialog_n" valign="middle">
									<div  class="dialog_title">
										%TITLE%
									</div>
								</td>
								<td class="dialog_ne">
									<div class="dialog_ne">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr >
				<td>
					<table style="width:100%">
						<tbody>
							<tr>
								<td class="dialog_w">
									<div class="dialog_w">
									</div>
								</td>
								<td class="dialog_content">
									<div class="dialog_content_simple">
									%CONTENT%
									</div>
								</td>
								<td class="dialog_e">
									<div class="dialog_e">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr >
				<td style="border-bottom:1px solid gray;">
				</td>
			</tr>
		</tbody>
	</table>
</div>
