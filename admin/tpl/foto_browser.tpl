<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-2">
  <title>%TITLE%</title>
	<script type="text/javascript" src="js/js.js"></script>
	<script type="text/javascript" src="js/advajax.js"></script>
	<script type="text/javascript" src="js/ss_files.js"></script>
	<script type="text/javascript" src="js/jmain_funct.js"></script>
	<LINK REL="stylesheet" HREF="css/easytable.css" TYPE="text/css">
	<LINK REL="stylesheet" HREF="css/main.css" TYPE="text/css">
	<style>
	* {font-size:10px; font-family:verdana;}
	a {text-decoration:none; }
	a.topmenu {color:white; text-decoration:none;}
	a.topmenu:hover {background-color:#808080; color:white;}
	#uploader_frm {display:none;}
	</style>
  </head>
  <body leftmargin=0 rightmargin=0 topmargin=0 bottommargin=0 marginwidth=0 marginheight=0 %ADDBODY%>

<center>
<div id="header">
	<div id="mainmenu">
<form class="pureform">
Katalog: %COMBO_DIRS%
</form>
	</div>
</div>
<div id="content_fb">
%CONTENT%
</div>
<div id="footer_static">
	<div id="footer_operation">
	%FOOTER_OPERATION%
	</div>
</div>
</center>

<div id="ajaxwaitmodule" style="position:absolute; right:1px; top:1px; float:none; padding:5px; border:1px solid black; background-color:rgb(220,220,220); color:red; font-weight:bold; display:none; z-index:2000; width:100px; height:50px; text-align:center;"><img src="gfx/indicator.gif"><br>Czekaj</div>
<iframe id="uploader_frm" name="uploader_frm"></iframe>
  </body>
</html>
