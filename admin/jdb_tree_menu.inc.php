<?php
function zakl_menu($menu, $active, $width=500) {

	if ($menu!==false && count($menu)>0) {
		$tb=new easytable();
		$tb->setsimple();
		$tb->opentable("border=0 cellspacing=0 cellpadding=0 background=\"gfx/zakl_spacer.gif\" width={$width}");
		$tb->row();
		foreach ($menu as $k=>$v) {
			$w=strlen($v)*9;
			if ($k==$active) {
				$tb->cell("<img src=\"gfx/zakl_act_left.gif\" border=0>",'width=10');
				$tb->cell("<a href=\"{$k}\"><b>{$v}</b></a>","background=\"gfx/zakl_act_bg.gif\" width={$w} align=center");
				$tb->cell("<img src=\"gfx/zakl_act_right.gif\" border=0>",'width=10');
			} else {
				$tb->cell("<img src=\"gfx/zakl_nact_left.gif\" border=0>",'width=10');
				$tb->cell("<a href=\"{$k}\">{$v}</a>","background=\"gfx/zakl_nact_bg.gif\" width={$w} align=center");
				$tb->cell("<img src=\"gfx/zakl_nact_right.gif\" border=0>",'width=10');
			}
		}
		$tb->cell("<img src=\"gfx/zakl_spacer.gif\" border=0>");
		return $tb->show();
	}

}

?>
