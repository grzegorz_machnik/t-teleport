<?php

include(_INCLUDE_PATH.'jdbet_tree.class.php');

$desc=array(
'title'=>'Nazwa',
'id_parent'=>'Kategoria nadrzędna',
'description'=>'Opis',
'pokaz_tresc'=>'Pokaż powyższą treść',
);

$a = new jdbet_tree($sqlconn, $table_section);

$a->set_deep($deep);

$a->set_fielddescriptions($desc);

$tb=new easytable();
$tb->setsimple();
$tb->opentable('width=650');
$tb->row();
$tb->cell($a->operation($f),'align=center');
$tu=$tb->show();

?>
