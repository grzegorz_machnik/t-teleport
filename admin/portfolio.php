<?php

include('head.inc.php');

$sid='portfolio';

function showfunct($row) {	
	$row['_name']=$row['name'];	
	return $row;
}

function content() {
	global $sqlconn;
	
	$a=new jdbet($sqlconn, _DB_PREFIX.'_portfolio');
	$fielddesc['_name']='Nazwa';
	$fielddesc['value']='Tekst';
  $a->set_fielddescriptions($fielddesc);
//	$a->set_fieldtype('value','htmlarea');
	//$a->set_fieldtype('sid', 'hidden');
	$a->set_fieldtype('url', 'hidden');
	$a->set_showfunct('showfunct');
	$a->set_filebrowse('foto','filebrowse.php');
	$a->set_toshow('nazwa, typ');
	
	$a->set_artforeignkey('typ', array('small'=>'Mały','big'=>'Duży'));
 	  $a->set_fieldtype('typ','combo');
	
  //$a->unset_operation('del');

	return $a->operation($_REQUEST);
}

$content=content();

include('foot.inc.php');

?>
