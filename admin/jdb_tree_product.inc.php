<?php
include(_INCLUDE_PATH.'jdbet_tree.class.php');

$fd=array(
'title'=>'Nazwa',
'id_parent'=>'Kategoria nadrzędna',
'id_section'=>'Kategoria',
'description'=>'Opis',
'description_full'=>'Pełny opis',
);

$f=$_REQUEST;

if ($f['setparent']=='on') {
	$_SESSION['id_parent']=$f['id_parent'];
}
// pre($_SESSION);
// if (!isset($_SESSION['id_parent'])) $_SESSION['id_parent']='0';
/*
if ($sid!='') {
	$tmp_data=db_getsqltable($sqlconn, "select * from {$table_section} where id_site='{$sid}' order by jdb_orderkey");
	$data_tree=get_section_tree($tmp_data!==false?$tmp_data:array());
} else {
*/
	$tmp_data=db_getsqltable($sqlconn, "select * from {$table_section} order by jdb_orderkey");
	$data_tree=get_section_tree($tmp_data!==false?$tmp_data:array());
// }

$combo_tree=get_combo_tree4prod($data_tree);
if ($combo_tree==false) $combo_tree=array();
$combo_treeall=get_combo_tree4prodall($data_tree);
if ($combo_treeall===false) $combo_treeall=array();

$combo_tree=array(intval(0)=>' - kategoria główna - ')+$combo_tree;
$combo_treeall=array(intval(0)=>' - kategoria główna - ')+$combo_treeall;

function show_section_combo() {
	global $sqlconn, $combo_tree;

	$ct=array(''=>' -- wszystkie kategorie -- ')+$combo_tree;
	$tu=jform_open($_SERVER['PHP_SELF']);
// echo "::{$_SESSION['id_parent']}::";
	$tu.=jform_combo('id_parent', $ct, $_SESSION['id_parent']);
	$tu.=jform_hidden('setparent','on');
	$tu.=jform_submit('ok','pokaż');
	$tu.=jform_close();	
	return $tu;
}

function showfunct($row) {
	global $combo_treeall;
	$row['sekcja']=$combo_treeall[$row['id_section']];
	$row['foto']=($row['foto_fn']!=''?"<img src=\"mphoto.php?img={$row['foto_fn']}&max=150\">":'');
	return $row;
}

function get_subsect($data, $id) {
	global $sqlconn;
	$cnt=count($data);
	$tmp=array();
	for ($i=0; $i<$cnt; $i++) {

		
		if ($data[$i]['id_parent']==$id) {
			$tmp[]=$data[$i]['id'];
			if (count($data[$i]['jdb_tree_data'])>0) {
				$tmp=array_merge($tmp,get_subsect($data[$i]['jdb_tree_data'], $data[$i]['id']));
			}
			
		} else {
			if (count($data[$i]['jdb_tree_data'])>0) {
				$tmp=array_merge($tmp,get_subsect($data[$i]['jdb_tree_data'], $id));
			}
		}
	}

	return $tmp;	

}

$a = new jdbet($sqlconn, $table_product);

$tmpwhere=array();

if ("{$_SESSION['id_parent']}"!='' && "{$_SESSION['id_parent']}"!='0') {
	$subsect = get_subsect($data_tree, $_SESSION['id_parent']);
	$subsect[] = $_SESSION['id_parent'];
	$tmpwhere[]="id_section in (".@join(',',$subsect).")";
	$a->set_fieldvalue('id_section',$_SESSION['id_parent']);
} elseif ("{$_SESSION['id_parent']}"=='0') {
	$subsect[] = "{$_SESSION['id_parent']}";
	$tmpwhere[]="id_section in (".@join(',',$subsect).")";
	$a->set_fieldvalue('id_section',"{$_SESSION['id_parent']}");
}
/*
if ($sid!=='') {
	$tmpwhere[]=" id_site='{$sid}' ";
	$a->set_fieldtype('id_site','hidden');
	$a->set_fieldvalue('id_site',$sid);
}
*/
if (count($tmpwhere)>0) {
	$a->set_where( join(' and ',$tmpwhere));
}


// $galeria_dzialy = array('0'=>'-brak-') + db_getbyindex($sqlconn, "select id, title from "._DB_PREFIX."_galcontent where jdb_active='y' order by jdb_orderkey");

$a->set_toshow('title,foto,sekcja');
$a->set_showfunct('showfunct');
//$a->set_toshowsize('300,150');
// $a->set_artforeignkey('id_galeria', $galeria_dzialy);
$a->set_cryptlink();
$a->set_fielddescriptions($fd);
$a->set_filebrowse('foto_fn','filebrowse.php');
// $a->set_fieldtype('opis','htmlarea');
$a->set_fieldtype('description','htmlarea');
$a->set_artforeignkey('id_section',$combo_tree);



$tb=new easytable();
$tb->setsimple();
$tb->opentable('width=650');

$tb->row();
$tb->cell(show_section_combo().$a->operation($f),'align=center');
$tu.=$tb->show();


?>
