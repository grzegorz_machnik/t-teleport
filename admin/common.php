<?php

function getFilenameWithoutExtension($filename) {
  $pos = strripos($filename, '.');
  if($pos === false){
    return $filename;
  }else{
    return substr($filename, 0, $pos);
  }
}

function getFileExtension($filename) { 
 $filename = strtolower($filename) ; 
 $exts = split("[/\\.]", $filename) ; 
 $n = count($exts)-1; 
 $exts = $exts[$n]; 
 return $exts; 
} 

function FriendlyUrl($sString) {
  $asCharacters = Array(
  //WIN
  "\xb9" => "a", "\xa5" => "A", "\xe6" => "c", "\xc6" => "C",
  "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
  "\xf3" => "o", "\xd3" => "O", "\x9c" => "s", "\x8c" => "S",
  "\x9f" => "z", "\xaf" => "Z", "\xbf" => "z", "\xac" => "Z",
  "\xf1" => "n", "\xd1" => "N",
  //UTF
  "\xc4\x85" => "a", "\xc4\x84" => "A", "\xc4\x87" => "c", "\xc4\x86" => "C",
  "\xc4\x99" => "e", "\xc4\x98" => "E", "\xc5\x82" => "l", "\xc5\x81" => "L",
  "\xc3\xb3" => "o", "\xc3\x93" => "O", "\xc5\x9b" => "s", "\xc5\x9a" => "S",
  "\xc5\xbc" => "z", "\xc5\xbb" => "Z", "\xc5\xba" => "z", "\xc5\xb9" => "Z",
  "\xc5\x84" => "n", "\xc5\x83" => "N",
  //ISO
  "\xb1" => "a", "\xa1" => "A", "\xe6" => "c", "\xc6" => "C",
  "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
  "\xf3" => "o", "\xd3" => "O", "\xb6" => "s", "\xa6" => "S",
  "\xbc" => "z", "\xac" => "Z", "\xbf" => "z", "\xaf" => "Z",
  "\xf1" => "n", "\xd1" => "N");       
  $sString = strtr($sString, $asCharacters);
  $sString = strtr($sString, 'ˇ¦¬±¶Ľ','ASZasz');
  $sString = preg_replace("'[[:punct:][:space:]]'",'-',$sString);
  $sString = strtolower($sString);
  $sChar = '-';
  $nRepeats = 1;
	$sString = preg_replace_callback('#(['.$sChar.'])\1{'.$nRepeats.',}#', create_function('$a', 'return substr($a[0], 0,'.$nRepeats.');'), $sString);

  return $sString;
}

function MakeFriendlyUrl($id_content=false, $table_name, $sString) {
  global $sqlconn;

  $sString = FriendlyUrl($sString);

  $ex = db_getsinglevalue($sqlconn, "SELECT id FROM {$table_name} WHERE url='{$sString}'");
  if ($ex!=false) {
    if ($id_content!=false) {
      if ($ex==$id_content) return $sString;  
      else return $sString.$id_content;
    }
    return $sString.$ex; 
  }
  return $sString;
}

function ApplyFriendlyUrl($table_name) {
  global $sqlconn;
	$f=$_REQUEST;
    
  $record = db_getsqltable($sqlconn, "SELECT * FROM {$table_name} WHERE url=''");
  if ($record==false) return;

  if ($f['op']=='url') {
    foreach ($record as $k=>$v) {
      $url = MakeFriendlyUrl($v['id'], $table_name, $v['nazwa']);
      $sqlconn->query("UPDATE {$table_name} SET url='{$url}' WHERE id='{$v['id']}'");  
    }
  } 
  else return msg_warning("Nie wszystkie pozycje posiadają przyjazne linki <a href='?op=url' style='font-size:10px;'>UTWÓRZ BRAKUJĄCE LINKI</a>").'<br />';
}

?>