<?php

$lng_cfg=array(
	'gfx_dir'=>'gfx/pl',
	'edit'=>'Edytuj',
	'del'=>'Kasuj',
	'add'=>'Dodaj',
	'confirm'=>'Jeste� pewien?',
	'up'=>'Przenie� w g�r�',
	'down'=>'Przenie� w d�',
	'insert'=>'Zatwierd�',
	'cancel'=>'Anuluj',
	'refresh'=>'Od�wierz',
	'active'=>'Aktywny - kliknij aby zmieni�',
	'deactive'=>'Nie aktywny - kliknij aby zmieni�',
	'user_pass_notnull'=>'Musisz ustawi� has�o dla u�ytkownika',
	'user_exists'=>'U�ytkownik o takim lognie istnieje...',
	'field_login'=>'Nazwa u�ytkownika',
	'field_pass'=>'Has�o',
	'login_btn'=>'Zaloguj',
	'logout_btn'=>'Wyloguj',
	'login_txt'=>'Zalogowany',
	'user_pass_fail'=>'Nieprawid�owy login lub has�o!',
	'sitetitle_admins'=>'Administratorzy',
	'sitetitle_login'=>'Panel administracyjny - zaloguj si�!',
	'field_valid_notnull'=>'Pole <b>%FIELD%</b> nie mo�e by� puste.',
	'field_valid_bad'=>'Niew�a�ciwy format %FIELD%',
	'del_selected'=>'Kasuj zaznaczone',
	'upload'=>'Kopiuj na serwer',
	'cancel'=>'Anuluj',
);

?>
