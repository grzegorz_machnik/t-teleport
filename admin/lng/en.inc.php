<?php

$lng_cfg=array(
	'gfx_dir'=>'gfx/en',
	'edit'=>'Edit',
	'del'=>'Delete',
	'add'=>'Add',
	'confirm'=>'Are you sure?',
	'up'=>'Up',
	'down'=>'Down',
	'insert'=>'OK',
	'cancel'=>'Cancel',
	'refresh'=>'Refresh',
	'active'=>'Aktywny - kliknij aby zmieni�',
	'deactive'=>'Nie aktywny - kliknij aby zmieni�',
	'user_pass_notnull'=>'Musisz ustawi� has�o dla u�ytkownika',
	'user_exists'=>'U�ytkownik o takim lognie istnieje...';
	'field_login'=>'User name',
	'field_pass'=>'Password',
	'sitetitle_admins'=>'Admins',
	'field_valid_notnull'=>'Pole <b>%FIELD%</b> nie mo�e by� puste.',
	'field_valid_bad'=>'Niew�a�ciwy format %FIELD%',
	'del_selected'=>'Kasuj zaznaczone',
	'upload'=>'Kopiuj na serwer',
	'cancel'=>'Cancel',
);


?>
