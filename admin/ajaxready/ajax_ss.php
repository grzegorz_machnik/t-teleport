<?php;
session_start();

$f=$_REQUEST;
$session_folder='itemid';
$getfolder='itemid';
if ($f['ss']!='') $session_folder=$f['ss'];
if ($f['gs']!='') $get_folder=$f['gs'];

header('Content-Type: text/xml');

if ($f[$get_folder]!='') {
	if (isset($_SESSION[$session_folder][$f[$get_folder]])) unset($_SESSION[$session_folder][$f[$get_folder]]);
	else $_SESSION[$session_folder][$f[$get_folder]]=$f[$get_folder];
}

echo '<?xml version="1.0" ?>';

echo '<items>';

if (count($_SESSION[$session_folder])>0) {
	foreach ($_SESSION[$session_folder] as $v) {
		echo "<{$get_folder}>{$v}</{$get_folder}>";	
	}
}
echo '</items>';
?>
