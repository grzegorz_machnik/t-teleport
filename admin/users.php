<?php

include('head.inc.php');

$sid='wspolnoty';

$wspolnoty = db_getbyindex($sqlconn, "SELECT id, nazwa FROM "._DB_PREFIX."_wspolnoty WHERE jdb_active='y' ORDER BY jdb_orderkey");

if($_GET['op']=='activeuser'){
    $sql = "Select * from "._DB_PREFIX."_users where id = '{$_GET['id']}'";
    $user = db_getsinglerow($sqlconn, $sql);
    
    $sql = "Select value from "._DB_PREFIX."_config where sid = 'email_nadawca' limit 1";
    $nadawca = db_getsinglevalue($sqlconn, $sql);
    
    $sql = "Select value from "._DB_PREFIX."_text where sid = 'email_aktywacja_konta' limit 1";
    $tresc = db_getsinglevalue($sqlconn, $sql);
    
    $sql = "Update "._DB_PREFIX."_users set jdb_active = 'y' where id = '{$_GET['id']}' limit 1";
    mysql_query($sql);
    
    
    
    jmail_html($user['email'], 'Aktywacja konta', $tresc, $nadawca);
    header('location:users.php');
}

function sort_panel() {
	
	$sortbyarr = array(
		'imie'=>'Imię',
		'nazwisko'=>'Nazwisko',
		'id_wspolnoty'=>'Wspólnota',
	);
	
	$visarr = array(
		'a'=>'Wszyscy',
		'y'=>'Potwierdzeni',
		'n'=>'Niepotwierdzeni',
	);
	
	$descarr = array(
		'desc'=>'Malejąca',
		'asc'=>'Rosnąca',
	);

	$f=$_REQUEST;
	
	if ( isset($f['sortby']) ) $_SESSION['admin']['users']['sortby']=$f['sortby'];
	if (!isset($_SESSION['admin']['users']['sortby']) ) $_SESSION['admin']['users']['sortby']='nazwisko';

	if ( isset($f['where_cmb']) ) $_SESSION['admin']['users']['where']=$f['where_cmb'];
	if ( !isset($_SESSION['admin']['users']['where']) ) $_SESSION['admin']['users']['where']='a';

	if ( isset($f['desc']) ) $_SESSION['admin']['users']['desc']=$f['desc'];
	if ( !isset($_SESSION['admin']['users']['desc']) ) $_SESSION['admin']['users']['desc']='desc';
	
	$tu.=jform_open($_SERVER['PHP_SELF']);
	$tu.='<table><tr><td>Pokazuj:</td><td>'.jform_combo('where_cmb',$visarr,$_SESSION['admin']['users']['where'] ,'onchange="submit();" style="width:120px;"').'</td></tr>';
	$tu.=jform_close();
	$tu.="<br/>";	
	$tu.=jform_open($_SERVER['PHP_SELF']);
	$tu.='<tr><td>Sortuj po:</td><td>'.jform_combo('sortby',$sortbyarr, $_SESSION['admin']['users']['sortby'] ,'onchange="submit();" style="width:120px;"').'</td></tr>';
	$tu.=jform_close();
	$tu.="<br/>";	
	$tu.=jform_open($_SERVER['PHP_SELF']);
	$tu.='<tr><td>Kolejność:</td><td>'.jform_combo('desc',$descarr, $_SESSION['admin']['users']['desc'] ,' style="width:120px;" onchange="submit();" style="width:120px;"').'</td></tr></table><br/>';
	$tu.=jform_close();

	return $tu;
}

$sortpanel = sort_panel();
$addleft.=group_left('Sortowanie', $sortpanel);

function _show($row){
   $row['operacje'] = '';
   if($row['jdb_active']=='n'){
       $row['operacje'] .= '
           <a  href="#" onClick="if(confirm(\'Czy chcesz aktywować konto i poinformować o tym użytkownika mailowo?\')) location.href=\'users.php?op=activeuser&id='.$row['id'].'\'" style="color:black">Aktywuj konto</a>
';
   }
   return $row; 
}

function content() {
	global $sqlconn, $wspolnoty;
	
	$a=new jdbet($sqlconn, _DB_PREFIX."_users");	
	$fielddesc['id_wspolnoty']='Wspólnota';
	$fielddesc['login']='Login';
	$fielddesc['haslo']='Hasło';
	$fielddesc['imie']='Imię';
	$fielddesc['nazwisko']='Nazwisko';
	$fielddesc['email']='Email';
	$fielddesc['telefon']='Telefon';
	$fielddesc['udzial']='Udział';
	$fielddesc['nr_lokalu']='Numer lokalu';
	$fielddesc['nr_komorki_lokatorskiej']='Numer komórki lokatorskiej';
	$fielddesc['nr_miejsca_postojowego']='Numer miejsca postojowego';
	$fielddesc['budynek']='Budynek';
	$fielddesc['pietro']='Piętro';
	$fielddesc['klatka']='Klatka';
	$fielddesc['nr_ksiegi_wieczystej']='Numer księgi wieczystej mieszkania';
	$fielddesc['koresp_imie']='Imię (koresp)';
	$fielddesc['koresp_nazwisko']='Nazwisko (koresp)';
	$fielddesc['koresp_ulica']='Ulica (koresp)';
	$fielddesc['koresp_nr_domu']='Nr domu (koresp)';
	$fielddesc['koresp_nr_mieszkania']='Nr mieszkania (koresp)';
	$fielddesc['koresp_miasto']='Miasto (koresp)';
	$fielddesc['koresp_email']='Email (koresp)';
	$fielddesc['koresp_telefon']='Telefon (koresp)';
	$fielddesc['data_rejestracji']='Data rejestracji';
        $fielddesc['udzial_w_garazu']='Udział w garażu';
        $fielddesc['zaliczka']='Zaliczka na opłaty eksploatacyjne';
        $fielddesc['powierzchnia_mieszkania']='Powierzchnia mieszkania';
        $fielddesc['nr_ksiegi_wieczystej_postoj']='Numer księgi wieczystej miejsca postojowego';
        $fielddesc['nr_konta_bankowego']='Numer konta bankowego';
  $fielddesc['message']='Wiadomość pojawiające się<br> na stronie wejściowej strefy mieszkańca';        
  $a->set_fielddescriptions($fielddesc);	
	$a->set_toshow('id_wspolnoty,imie,nazwisko,email,telefon, operacje');
	$a->set_fieldtype('data_rejestracji','hidden');
	$a->set_fieldtype('message','htmlarea');
	
 	$a->set_artforeignkey('id_wspolnoty', $wspolnoty);
 	$a->set_showfunct('_show');
	$a->set_fieldtype('haslo','passwordmd5');
        
        if(!isset($_GET['op']))
             $a->set_fieldtype('jdb_active', 'hidden');
        
	if ($_SESSION['admin']['users']['sortby']!='') {
		$a->set_orderby($_SESSION['admin']['users']['sortby']);
	}
	if ($_SESSION['admin']['users']['where']!='') {
		if ($_SESSION['admin']['users']['where']!='a') {
		  $a->set_where(" jdb_active='{$_SESSION['admin']['users']['where']}' ");
		}
	}
	if ($_SESSION['admin']['users']['desc']!='') $a->set_desc( $_SESSION['admin']['users']['desc'] );
        
	return $a->operation($_REQUEST);
}
	
$content=content();
	
include('foot.inc.php');

?>