<?php

include('head.inc.php');
include('images.php');

$sid='banners';

function show_($row) {  
  $row['image'] = "<a href='/{$row['image']}'>".get_image($row['image'], 60, 60)."</a>";
  return $row;
}
	
function content() {
	global $sqlconn, $bs_cids;

	$a=new jdbet($sqlconn, _DB_PREFIX."_banners_main_left");
	$desc['title']='Tytuł';
	$desc['image']='Obraz';
  $a->set_fielddescriptions($desc);
  $a->set_filebrowse('image','filebrowse.php'); 
	$a->set_orderby('jdb_orderkey');
	$a->set_showfunct('show_');
	$a->set_toshow('image, title');

  return $a->operation($_REQUEST);
}

$content=content();
                                                  
include('foot.inc.php');