var xmlhttp;

function GetXmlHttpObject()
{
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    return new XMLHttpRequest();
  }
  if (window.ActiveXObject) {
    // code for IE6, IE5
    return new ActiveXObject("Microsoft.XMLHTTP");
  }
  return null;
}

function SwitchOnMain(id_content, value)
{
  xmlhttp=GetXmlHttpObject();
  if (xmlhttp==null) { alert ("Browser does not support HTTP Request"); return; }

  var url="ajax_admin_operation.php";
  url=url+"?id_content="+id_content+"&value="+value+"&op=show_on_main";

  xmlhttp.onreadystatechange = function(){changedSwitchOnMain(id_content);};
  xmlhttp.open("GET",url,true);
  xmlhttp.send(null);
}

function changedSwitchOnMain(id_content)
{
  if (xmlhttp.readyState==4) {
    document.getElementById("main_switch"+id_content).innerHTML=xmlhttp.responseText;
  }
}

function SwitchOnMainMovie(id_movie, value)
{
  xmlhttp=GetXmlHttpObject();
  if (xmlhttp==null) { alert ("Browser does not support HTTP Request"); return; }

  var url="ajax_admin_operation.php";
  url=url+"?id_movie="+id_movie+"&value="+value+"&op=show_on_main_movie";

  xmlhttp.onreadystatechange = function(){changedSwitchOnMainMovie(id_movie);};
  xmlhttp.open("GET",url,true);
  xmlhttp.send(null);
}

function changedSwitchOnMainMovie(id_movie)
{
  if (xmlhttp.readyState==4) {
    document.getElementById("main_switch"+id_movie).innerHTML=xmlhttp.responseText;
  }
}

function GetXmlHttpObject()
{
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    return new XMLHttpRequest();
  }
  if (window.ActiveXObject) {
    // code for IE6, IE5
    return new ActiveXObject("Microsoft.XMLHTTP");
  }
  return null;
}

function ajax_deletecomment_admin(id, table_name) {
		
    advAJAX.get({
        url : "ajax_admin_operation.php?id="+id+"&table_name="+table_name+"&op=delete_comment",
        onInitialization : function() {
        },
        onComplete: function () {
        },
        onError : function() {
        	alert('Wystąpił błąd!');
        },
        onSuccess : function(obj) {
        	switch (obj.responseText) {
        		case 'ok':
							window.location.reload();
        			break;
        		case 'error':
        			alert('Nie udało się skasować komentarza');
        			break;
        		case 'null':
        		default:
        			break;
        	}
        }
    });
}

function deleteComment_admin(id, table_name) {
	if (confirm('Czy na pewno chcesz usunąć ten komentarz?')) ajax_deletecomment_admin(id, table_name);
}

function ajax_movie_thumbnail(id) {
		
    advAJAX.get({
        url : "ajax_admin_operation.php?id="+id+"&op=movie_thumbnail",
        onInitialization : function() {
        },
        onComplete: function () {
        },
        onError : function() {
        	alert('Wystąpił błąd!');
        },
        onSuccess : function(obj) {
        	switch (obj.responseText) {
        		case 'ok':
							window.location.reload();
        			break;
        		case 'error':
        			alert('Nie udało się utowrzyć okładki dla filmu');
        			break;
        		case 'null':
        		default:
        			break;
        	}
        }
    });    
}

function MovieThumbnail(id) {
	element = document.getElementById('thumb'+id);
	element.innerHTML = "<img src='../grafika/loading1.gif' />";
  ajax_movie_thumbnail(id);
}
