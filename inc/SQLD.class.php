<?php

class SQLD { 
    
    static $con;
    
    private function __construct() {
       
    }

    static function connect() {
        
        if (self::$con = mysql_connect(_DBHOST, _DBUSER, _DBPASS,TRUE)) { 
             mysql_query("SET NAMES latin2",self::$con);
                 
            if (!mysql_select_db(_DBNAME, self::$con)) {
                echo 'Nie można połączyć się z bazą '._DBNAME;
                return false;
            } else {
                
                return true;
            }
        } else{
            echo 'Błąd połączenia z bazą';
            return false;
            }
    }

    static function close() {
        if (!@mysql_close()) {
            return false;
        } else
            return true;
    }

    static function query($query) {
        $f = $_REQUEST;
        if (isset($f['show_query'])) {
            $_SESSION['show_query'] = $f['show_query'];
        }
      


        if (preg_match('/\?\:(\w+)/', $query, $m)) {
            $query = str_replace("?:", _DBPREFIX . '', $query);
        }
        mysql_query("SET NAMES 'latin2'",self::$con);
        
        $res = mysql_query($query,self::$con);
        

        if (!$res) {
             echo mysql_error();
            return false;
        } else {
            return $res;
        }
    }

    static function numRows($res) {

        $numrows = @mysql_num_rows($res);
        if ($numrows < 0) {
            return false;
        } else {
            return $numrows;
        }
    }

    static function fetchRow($res) {

        $tu = @mysql_fetch_row($res);
        if ($tu !== false)
            return $tu;
        else
            return false;
    }

    static function fetchRowassocc($res) {

        $tu = @mysql_fetch_array($res, MYSQL_ASSOC);
        if ($tu !== false)
            return $tu;
        else
            return false;
    }

    static function affectedRows() {

        $tu = @mysql_affected_rows();
        if ($tu !== false)
            return $tu;
        else
            return false;
    }

    static function lastID() {

        $tu = @mysql_insert_id();
        if ($tu !== false) {
            return $tu;
        } else {
            return false;
        }
    }

    static function fields($tname) {
        $row = array();
        if (preg_match('/\?\:(\w+)/', $tname, $m)) {
            $tname = str_replace("?:", _DBPREFIX . '', $tname);
        }
        //echo " SHOW FULL FIELDS FROM {$tname}";
        $res = @mysql_query(" SHOW FULL FIELDS FROM {$tname}");
        if ($res !== false) {
            $cnt = mysql_num_rows($res);
            for ($i = 0; $i < $cnt; $i++) {
                $r = @mysql_fetch_assoc($res);
                if ($r === false)
                    return false;
                preg_match_all('/([^( ]+)(\((.+)\))?/', $r['Type'], $m);
                $row[$r['Field']] = $r;
                $row[$r['Field']]['Type'] = $m[1][0];
                $row[$r['Field']]['len'] = $m[3][0];
            }

            return $row;
        } else
            return false;
    }

    static function getbyindex($sql) {

        $res = SQLD::query($sql);
        if ($res) {
            $tmp = array();
            while ($row = SQLD::fetchRow($res)) {
                list($k, $v) = $row;
                $tmp[$k] = $v;
            }
            return count($tmp) > 0 ? $tmp : false;
        } else
            return false;
    }

    static function getsqltable($sql) {

        $res = SQLD::query($sql);

        if ($res) {
            $tmp = array();
            while ($row = SQLD::fetchRowassocc($res)) {
                $tmp[] = $row;
            }
            return $tmp;
        } else {
            return false;
        }
    }

    static function getsinglerow($sql) {

        $res = SQLD::query($sql);
        if ($res) {

            $row = SQLD::fetchRowassocc($res);
            if ($row)
                return $row;
            else
                return false;
        } else
            return false;
    }

    static function getsinglevalue($sql) {

        $res = SQLD::query($sql);

        if ($res) {

            $row = SQLD::fetchRow($res);

            if ($row) {

                return $row[0];
            } else
                return false;
        }
    }

}

?>
