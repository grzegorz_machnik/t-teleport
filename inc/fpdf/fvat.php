<?php
  session_start();

require ("admin/config.php");
require_once("admin/inc/phpmailer/class.phpmailer.php");
require($SITE_MODULES_PATH.'site.mod');
require $TPL_SMARTY_PATH;
//error_reporting(E_ALL);

require ('admin/inc/classTshop.php');
require ('admin/inc/classTshopVat.php');

if (empty($_GET['id']) || !isset($_SESSION[SESSION_USER_ID_VAR])) header("Location: index.php");

//////////////////////////////////////////////////////////////////////
// db
require $DB_PEAR_PATH;

$dbH =& DB::connect($DB_DSN, $DB_OPTIONS);
if (PEAR::isError($dbH)) 
{
	echo $dbH->getMessage();
	echo $dbH->getDebugInfo();
	exit;
}

$DB = mysql_connect($DB_DSN['hostspec'],$DB_DSN['username'],$DB_DSN['password']);
if (!$DB)
   die('Not connected : ' . mysql_error());
$db_selected = mysql_select_db($DB_DSN['database'], $DB);
if (!$db_selected) 
   die ('Can\'t use foo : ' . mysql_error());
// / db
//////////////////////////////////////////////////////////////////////

$SHOP = new Tshop($dbH,array('categories_table' => $DB_SHOP_CATEGORIES_TABLE, 
                            'categories_content_table' => $DB_SHOP_CATEGORIES_CONTENT_TABLE,
                            'categories_features_table' => $DB_SHOP_CATEGORIES_FEATURES_TABLE,
                            'categories_features_content_table' => $DB_SHOP_CATEGORIES_FEATURES_CONTENT_TABLE,
                            
                            'products_table' => $DB_SHOP_PRODUCTS_TABLE,
                            'products_content_table' => $DB_SHOP_PRODUCTS_CONTENT_TABLE,
                            'products_opinions_table' => $DB_SHOP_PRODUCTS_OPINIONS_TABLE,
                            'products_ratings_table' => $DB_SHOP_PRODUCTS_RATINGS_TABLE,
                            'products_repository_table' => $DB_SHOP_PRODUCTS_REPOSITORY_TABLE,
                            'products_photos_table' => $DB_SHOP_PRODUCTS_PHOTOS_TABLE,
                            'products_files_table' => $DB_SHOP_PRODUCTS_FILES_TABLE,
                            'producers_table' => $DB_SHOP_PRODUCERS_TABLE,
                            'clients_table' => $DB_SHOP_CLIENTS_TABLE,
                            'orders_table' => $DB_SHOP_ORDERS_TABLE,
                            'ordered_products_table' => $DB_SHOP_ORDERED_PRODUCTS_TABLE,
                            
                            'delivery_table' => $DB_SHOP_DELIVERY_TABLE,
                            'orders_addresses_table' => $DB_SHOP_ORDERS_ADDRESSES_TABLE,
                            'clients_store_table' => $DB_SHOP_CLIENTS_STORE_TABLE,
                            'products_comments_table' => $DB_SHOP_PRODUCTS_COMMENTS_TABLE,
                            'rebates_table' => $DB_SHOP_REBATES_TABLE,
                            
                            'vat_table' => $DB_SHOP_VAT_TABLE,
                            'vat_products_table' => $DB_SHOP_VAT_PRODUCTS_TABLE                            
                            )
);

$VAT = new TshopVat($dbH,array('vat_table' => $DB_SHOP_VAT_TABLE,
                              'vat_products_table' => $DB_SHOP_VAT_PRODUCTS_TABLE,
                              'vat_clients_table' => $DB_SHOP_CLIENTS_TABLE,
                              'vat_numbers_table' => $DB_SHOP_VAT_NUMBERS_TABLE,
                              'vat_products_correction_table' => $DB_SHOP_VAT_PRODUCTS_CORRECTION_TABLE
                              ));

$SMARTY = new Smarty();
$SMARTY->template_dir = $TPL_SITE_FILES_PATH.'templates';
$SMARTY->compile_dir = $TPL_SITE_FILES_PATH.'templates_c';
$SMARTY->cache_dir = $TPL_SITE_FILES_PATH.'cache';
$SMARTY->config_dir = $TPL_SITE_FILES_PATH.'configs';

function sumValQuantity($pTab, $pCol) {
  $res = 0;

  foreach ($pTab as $val)
    $res += $val['quantity'] * $val[$pCol];

  return sprintf("%.2f", $res);
}

function policz($l,$t1,$t2,$t3) {
  $j = array("", "jeden ", "dwa ", "trzy ", "cztery ", "pi�� ", "sze�� ",
    "siedem ", "osiem ", "dziewi�� ", "dziesi�� ", "jedena�cie ", 
    "dwana�cie ", "trzyna�cie ", "czterna�cie ", "pi�tna�cie ", 
    "szesna�cie ", "siedemna�cie ", "osiemna�cie ", "dziewi�tna�cie ");
  $d = array("", "", "dwadzie�cia ", "trzydzie�ci ", "czterdzie�ci ",
    "pi��dziesi�t ", "sze��dziesi�t ", "siedemdziesi�t ", 
    "osiemdziesi�t ", "dziewi��dziesi�t ");
  $s = array("","sto ", "dwie�cie ", "trzysta ", "czterysta ", "pi��set ",
    "sze��set ", "siedemset ", "osiemset ", "dziewi��set ");

  $txt = $s[0+substr($l,0,1)];
  if (substr($l,1,2)<20) $txt .= $j[0+substr($l,1,2)]; 
  else $txt .= $d[0+substr($l, 1,1)].$j[0+substr($l, 2,1)];
  if ($l<>0) if ($l==1) $txt .= "$t1 "; else {
    if ((substr($l,2,1)==2 or substr($l,2,1)==3 or substr($l,2,1)==4) 
    and (substr($l,1,2)>20 or substr($l,1,2)<10)) 
    $txt .= "$t2 "; else $txt .= "$t3 ";
  }
  return $txt;
}


function slownie($liczba) {
  $liczba = str_replace(",", ".", $liczba);
  $liczba = number_format($liczba, 2, ",", "");
  $kwota=explode(",", $liczba);
  $kwotazl=sprintf("%012d",$kwota[0]);
  $kwotagr=sprintf("%03d",$kwota[1]);
  $txt = '';
  $txt .= policz(substr($kwotazl, 0,3),"miliard","miliardy","miliard�w");
  $txt .= policz(substr($kwotazl, 3,3),"milion","miliony","milion�w");
  $txt .= policz(substr($kwotazl, 6,3),"tysi�c","tysi�ce","tysi�cy");
  $txt .= policz(substr($kwotazl, 9,3),"z�oty","z�ote","z�otych");
  if ($kwotazl==0) $txt .= "zero z�otych ";
  $txt .= "i ";
  $txt .= policz($kwotagr,"grosz","grosze","groszy");
  if ($kwotagr==0) $txt .= "zero groszy";
  return $txt;
}

function dataAdd($datadd)
{
$dataw = strtotime($datadd);
$datau = $dataw + (7 * 24 * 60 * 60);
return strftime("%Y-%m-%d", $datau);
}

// zamowienie
  $query = "select o.*, c.name as c_name, c.surname as c_surname, c.street as c_street, c.postcode as c_postcode,
    c.city as c_city, c.company as c_company, c.nip as c_nip
    from ".$DB_SHOP_ORDERS_TABLE." o, ".$DB_SHOP_CLIENTS_TABLE." c
    where o.id = '".$_GET['id']."' and o.client_id = '".$_SESSION[SESSION_USER_ID_VAR]."' and o.client_id = c.id";
  $result = $dbH->query($query);
  $record = $result->fetchRow(DB_FETCHMODE_ASSOC);

  foreach ($record as $key => $value)
    $row[$key] = stripslashes($value);

  $tOrder = $row;
  $sale_date = $tOrder['added'];
  if ($tOrder['delivery_id'] == '1')
  {
    $payment = 'P�atno�� z g�ry';
    $delivery = 'Przesy�ka krurierem UPS - p�atno�� z g�ry';
    $delivery_cost = '16.50';
  }
  else if ($tOrder['delivery_id'] == '2')  
  {
    $payment = 'P�atno�� za pobraniem';
    $delivery = 'Przesy�ka krurierem UPS - p�atno�� za pobraniem';
    $delivery_cost = '23';
  }
// / zamowienie

// pozycje zamowienia  
  $sumVat = 0;

  $query = "select * from ".$DB_SHOP_ORDERED_PRODUCTS_TABLE." where id_zamowienia = '".$_GET['id']."'";
  $result = $dbH->query($query);

  $row = array();
  $tItems = array();

  while ($record = $result->fetchRow(DB_FETCHMODE_ASSOC))
  {
    foreach ($record as $key => $value) {
      $row[$key] = stripslashes($value);
    }

    $row['price_netto'] = $row['price'] * (100/(100 + $row['vat'])); // oryginalna netto uwzgl. ewentualny rabat
    $row['price'] = $row['price'] * ((100 - $tOrder['rebate_percent'])/100);
    $row['price_netto_rebate'] = $row['price']*(100/(100 + $row['vat'])); // netto po rabacie
    $row['price_vat'] = $row['quantity'] * (($row['vat']/100) * $row['price_netto_rebate']); // wartosc vat
    $row['value_netto'] = $row['quantity'] * $row['price_netto']; // wartosc netto
    $row['value_netto_rebate'] = $row['quantity'] * $row['price_netto_rebate']; // wartosc netto z uwzgl. rabatu
    $row['value_brutto'] = $row['quantity'] * $row['price'];

    $sumVat += $row['price_vat'];
    $tItems[] = $row;
  }


//dodawanie faktury do tabeli
  $dane_firmy="Firma Cukiernicza \"Solidarno��\" Sp. z o.o.
ul. Gospodarcza 25
20-211 Lublin
KRS 0000018470,
NIP: 946-22-15-432,
REGON: 431133859
";
  $terminPlatnosci = '7 dni';
  $data['data'] = date('Y-m-d');
  $data['sprzedajacy'] = addslashes($dane_firmy);
  $tOrder['c_company'] != '' ? $data['kupujacy'] = $tOrder['c_company']."\n" : $data['kupujacy'] = $tOrder['c_name']." ".$tOrder['c_surname']."\n";
  $data['kupujacy'] .= $tOrder['c_postcode']." ".$tOrder['c_city'].", ";
  $data['kupujacy'] .= $tOrder['c_street']."\n";
  $tOrder['nip'] != '' ? $data['kupujacy'] .= "NIP: ".$tOrder['c_nip'].", " : '';
      
  $i = count($products) ? count($products) : 0;
  $query = "SELECT count(*) as ile FROM ".$DB_SHOP_VAT_TABLE." WHERE order_id = '".$_GET['id']."'";
  $result = $dbH->query($query);
  $record = $result->fetchRow(DB_FETCHMODE_ASSOC);
  if (!$record['ile']>0)
    {

      // wyci�gni�cie id uzytkownika
      $query = "SELECT client_id FROM ".$DB_SHOP_ORDERS_TABLE." WHERE id = '".$_GET['id']."'";
      $result = $dbH->query($query);
      $client_id = $result->fetchRow(DB_FETCHMODE_ASSOC);
      $client_id = $client_id['client_id'];
      // /wyciagniecie id uzytkownika    
      $order_id = $_GET['id'];
      $date = $data['data'];
      $seller = $data['sprzedajacy'];
      $buyer = $data['kupujacy'];
      $pay_to = $terminPlatnosci;
      $price_brutto = sprintf("%.2f", sumValQuantity($tItems, 'price') + $tOrder['delivery_cost']);
      $price_spell = slownie(sumValQuantity($tItems, 'price') + $tOrder['delivery_cost']);
      $query = "INSERT INTO ".$DB_SHOP_VAT_TABLE." (`client_id`, `order_id`, `date`, `sale_date`, `seller`, `buyer`, `pay_to`, `price_brutto`, `price_spell`) VALUES ('$client_id', '$order_id', '$date', '$sale_date', '$seller', '$buyer', '$pay_to', '$price_brutto', '$price_spell')";
      $res = $dbH->query($query);
      $lastId = mysql_insert_id();
      $query = "INSERT INTO ".$DB_SHOP_VAT_NUMBERS_TABLE." (`vat_id`) VALUES ('$lastId')";
      $res = $dbH->query($query);
    }

    // dodawanie danych do tabeli produkt�w dla danej faktury
    $query = "SELECT id FROM ".$DB_SHOP_VAT_TABLE." WHERE order_id = '".$_GET['id']."'";
    $result = $dbH->query($query);
    $id = $result->fetchRow(DB_FETCHMODE_ASSOC);
    $query = "SELECT count(*) as ile FROM ".$DB_SHOP_VAT_PRODUCTS_TABLE." WHERE vat_id = '".$id['id']."'";
    $result = $dbH->query($query);
    $record = $result->fetchRow(DB_FETCHMODE_ASSOC);
    if (!($record['ile']>=$i) || ($record['ile']==0))
    { 
     foreach ($tItems as $val) 
    { 
      $vat_id = $id['id'];
      $product_name = $val['product_name'];
      $quantity = $val['quantity'];
      $price_netto = sprintf("%.2f", $val['price_netto']);
      $value_netto = sprintf("%.2f", $val['value_netto']);
      $price_vat = sprintf("%.2f", $val['price_vat']);
      $value_brutto = sprintf("%.2f", $val['value_brutto']);
      $vat = $val['vat'];
      $query = "INSERT INTO ".$DB_SHOP_VAT_PRODUCTS_TABLE." (`vat_id`, `product_name`, `quantity`, `price_netto`, `value_netto`, `price_vat`, `value_brutto`, `vat`) VALUES ('$vat_id', '$product_name', '$quantity', '$price_netto', '$value_netto', '$price_vat', '$value_brutto', '$vat')";
      $result = $dbH->query($query);
    }
    }
  // /dodawanie danych do tabeli produkt�w dla danej faktury


// /dodawanie faktury do tabeli

  //dane faktury
      $fvat = $VAT->getVatDetails($_GET['id']);
      $fvat = $fvat[0];
      $vat_number = $VAT->getVatNumber($fvat['id']);
      $products = $VAT->getVatProducts($fvat['id']);
      $delivery_vat = (22/100)*$tOrder['delivery_cost'];
      $suma_brutto = sprintf("%.2f", sumValQuantity($tItems, 'price') + $tOrder['delivery_cost']).' PLN';
      $suma_vat = sprintf("%.2f", $sumVat + (22/100) * $tOrder['delivery_cost']);
      $slownie = slownie(sumValQuantity($tItems, 'price') + $tOrder['delivery_cost']);
      $date = date('Y-m-d');
      $pay_date = dataAdd($date);
      $bank = "PKO BP S.A. I O/Lublin";
      $nr_konta = "85 1020 3147 0000 8702 0046 6011";
  // /dane faktury



$SMARTY->assign('place', 'Lublin');
$SMARTY->assign('date', $fvat['sale_date']);
$SMARTY->assign('sale_date', $fvat['sale_date']);
$SMARTY->assign('buyer', $fvat['buyer']);
$SMARTY->assign('seller', $fvat['seller']);
$SMARTY->assign('payment', $payment);
$SMARTY->assign('delivery', $delivery);
$SMARTY->assign('delivery_cost', $delivery_cost);
$SMARTY->assign('delivery_vat', $delivery_vat);
$SMARTY->assign('delivery_netto', $delivery_cost - $delivery_vat);
$SMARTY->assign('vat_number', $vat_number);
$SMARTY->assign('products', $products);
$SMARTY->assign('bank', $bank);
$SMARTY->assign('nr_konta', $nr_konta);
$SMARTY->assign('suma_brutto', $suma_brutto);
$SMARTY->assign('suma_vat', $suma_vat);
$SMARTY->assign('suma_netto', $suma_brutto - $suma_vat);
$SMARTY->assign('slownie', $slownie);
$SMARTY->assign('pay_date', $pay_date);
$SMARTY->assign('gdzie',$_SERVER['REQUEST_URI']);

if (!isset($_GET['pdf'])){

$SMARTY->display('vat.tpl');
                   }
else{
$output=$SMARTY->fetch('vat.tpl');
require_once("files/fpdf/html2fpdf.php");
ob_start();
echo $output;
$htmlbuffer = ob_get_contents();
ob_end_clean();
$pdf = new HTML2FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->UseCSS(true);
$pdf->WriteHTML($htmlbuffer);
$pdf->Output();
}
?>
