<?
class web{
  	var $query;
  	var $sqlconn;
  	var $wynik=array();
  	var $lng; 
	  function web()
   {
    $this->sqlconn = $sqlconn;
    $this->lng = $_SESSION['lng'];
   }

 
 function Miesiac($miesiac)//własna funkcja, gdzie argumentem jest zmienna $miesiac
    {  
    switch ($miesiac)//dalej cyfry zamieniamy na nazwe miesiaca
    	{
    	case '01':
    		return 'styczeń';
    		break;
    	case '02':
    		return 'luty';
    		break;
    	case '03':
    		return 'marzec';
    		break;
    	case '04':
    		return 'kwietnia';
    		break;
    	case '05':
    		return 'maja';
    		break;
    	case '06':
    		return 'czerwca';
    		break;
    	case '07':
    		return 'lipca';
    		break;
    	case '08':
    		return 'sierpnia';
    		break;
    	case '09':
    		return 'wrzesień';
    		break;
    	case '10':
    		return 'pazdziernik';
    		break;
    	case '11':
    		return 'listopad';
    		break;
    	case '12':
    		return 'grudzień';
    		break;
    	default:
    		return 'Nieznana nazwa miesiąca';
    		break;
    	}
     }
   
   function miesiac_text($data)
      {
        $cut = substr($data, 5, -3);
         //dumpvar($cut);  
        $miesiac = $this->Miesiac($cut);        
        return $miesiac;
      }
   
   function toAscii($str, $replace=array(), $delimiter='-')
      {
          setlocale(LC_ALL, 'pl_PL.UTF8');
          if( !empty($replace) )
          {$str = str_replace((array)$replace, ' ', $str);}
          $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
          $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
          $clean = strtolower(trim($clean, '-'));
          $clean = preg_replace("/[\/_|+ -]+/",
          $delimiter, $clean);return $clean;
      }

///////////////////////////////////////////////////////////////////////PRODUKTY//////////////////////////////////////////////////////////////////	 
 
      function pobierzMenuAll() 
      {
        $query = "select id,id_parent,title_{$this->lng} as title, link from "._DB_PREFIX."_sections where id_parent = '1' and jdb_active = 'y' "; 
        // echo($query);
        $wynik=db_getsqltable($this->sqlconn, $query); 
        
        foreach ($wynik as $k=>$v)
        {
         
        $wynik[$k]['titlelink'] = $this->toAscii($v['title']); 
         
        // $query = "select id,id_parent,title_{$this->lng} as title from "._DB_PREFIX."_sections where id_parent = '".$v['id']."' and jdb_active = 'y' ";
        // $wynik[$k]['submenu']  = db_getsqltable($this->sqlconn, $query);
         //foreach ($wynik[$k]['submenu'] as $ki=>$vi)
        //  {
        //   $wynik[$k]['submenu'][$ki]['titlelink'] = $this->toAscii($vi['title']); 
        //  } 
         
         
       }                 
        return $wynik;
      }
      
      function policzAktualnosci() 
      {
        //dumpvar($special);  
        $query = "select count(id) from "._DB_PREFIX."_news where jdb_active = 'y' ";                 
        $wynik=db_getsinglevalue($this->sqlconn, $query);
 
        return $wynik;
      }
      
      
      function pobierzAktualnosci($id=null, $start=null, $limit=null, $prasowe, $targi, $konferencje) 
      {
        //dumpvar($special);  
        $query = "select id, id_type, informacje_prasowe, targi_konferencje,udzial_projekty, title_{$this->lng} as title, description_{$this->lng} as description, content_{$this->lng} as content, date, file_{$this->lng} as file, foto_{$this->lng} as foto, tagi from "._DB_PREFIX."_news where jdb_active = 'y' ";                 
        // echo($query);
        
        if ($prasowe==y)        
         {$query.=" and informacje_prasowe='y' ";} 
        elseif ($targi==y) 
         {$query.=" and targi_konferencje='y' ";}            
        elseif ($konferencje==y)         
         {$query.=" and udzial_projekty='y' ";}  
  
                   
        $id!=null?$query.="and id='$id' order by date desc":$query.="order by date desc limit $start,$limit";
        //echo($query);
        $wynik=db_getsqltable($this->sqlconn, $query);
         
        foreach ($wynik as $k=>$v)
          {
          $wynik[$k]['rok'] = substr($v['date'], 0, -6);   
          $wynik[$k]['dzien'] = substr($v['date'], -2);
          $wynik[$k]['miesiac_slownie'] = substr($this->miesiac_text($v['date']), 0, 3); 
          $wynik[$k]['titlelink'] = $this->toAscii($v['title']); 
          }
          
        return $wynik;
      }
      
      
      function getSite($id)
       {
        $query = "select id,title_{$this->lng} as title, description_{$this->lng} as description, content_{$this->lng} as content, tags from "._DB_PREFIX."_sections where id='$id' order by id desc ";
         // echo($query);
        $wynik=db_getsqltable($this->sqlconn, $query);
      
        return $wynik;
       }
      
      function getCatalogues($new=null)
       {
        $query = "select id,title_{$this->lng} as title, file_{$this->lng} as file,foto_{$this->lng} as foto from "._DB_PREFIX."_catalogues where jdb_active='y'";
        $new!=null? $query.=" and is_new='y' " : $query.=" " ; 
        $query.=" order by id desc";
          // echo($query);
        $wynik=db_getsqltable($this->sqlconn, $query);
        foreach ($wynik as $k=>$v)
          {
           $rozmiar = filesize($v['file']);      
           $wynik[$k]['rozmiar']  = round( $rozmiar / 1024 / 1024, 2).'MB';             
          }
          
      // dumpvar($wynik);
        return $wynik;
       }
       
       function getPublications()
       { 
        $query = "select id,title_{$this->lng} as title,name_{$this->lng} as name, file_{$this->lng} as file, scan_{$this->lng} as scan, date, link from "._DB_PREFIX."_publications where jdb_active='y' order by id desc ";
         // echo($query);
        $wynik=db_getsqltable($this->sqlconn, $query);
        foreach ($wynik as $k=>$v)
          {
           $wynik[$k]['rok'] = substr($v['date'], 0, -6);   
           $wynik[$k]['dzien'] = substr($v['date'], -2);
           $wynik[$k]['miesiac_slownie'] = $this->miesiac_text($v['date']);       
          }
          
      // dumpvar($wynik);
        return $wynik;
       }
       
       function getFaq()
       { 
        $query = "select id,title_{$this->lng} as title, file_{$this->lng} as file, description_{$this->lng} as description, date from "._DB_PREFIX."_faq where jdb_active='y' order by id desc ";
         // echo($query);
        $wynik=db_getsqltable($this->sqlconn, $query);
        foreach ($wynik as $k=>$v)
          {
           $wynik[$k]['rok'] = substr($v['date'], 0, -6);   
           $wynik[$k]['dzien'] = substr($v['date'], -2);
           $wynik[$k]['miesiac_slownie'] = $this->miesiac_text($v['date']);       
          } 
          
      // dumpvar($wynik);
        return $wynik;
       }

    function ustawpoloczenie($sqlconn)
    {     
     $this->sqlconn = $sqlconn;
    }
    
    function ustawjezyk($lng)
    {     
     $this->lng = $lng;
    }
	}
?>
