<?
class forum extends web{
  	var $query;
  	var $sqlconn;
  	var $wynik=array();
  	var $lng; 
  	public $errorMessage;
  	 public $shopMessage;
	  function forum()
     {
      $this->sqlconn = $sqlconn;
      $this->Uid = $_SESSION['uDataC']['id'];
      $this->lng = $_SESSION['lng'];
      $this->getCat = $_GET['id_category'];
      $this->postNick= $_POST['nick'];
      $this->postPassword= $_POST['password'];
      $this->postNewPassword = $_POST['newpassword'];
      $this->postEmail = $_POST['email'];
      $this->postCamera = $_POST['camera'];
      $this->postEmailvisible = $_POST['emailvisible'];
      $this->postAcceptemail = $_POST['acceptemail'];
      $this->postSignature = $_POST['signature'];
      $this->dateNow = date("Y-m-d H:i:s");
      
      $this->error_messages = array(
      1 => 'Pole nick jest nieprawidłowe',
      2 => 'Pole tytuł jest nieprawidłowe',
      3 => 'Pole treść tematu jest nieprawidłowe',
      4 => 'Podany nick jest już zajęty',
      5 => 'Podany adres email jest już zajęty',
      6 => 'Podany adres email ma nieprawidłowy format',
      7 => 'Podaj hasło (min. 3 znaki)',
      8 => 'Podany token jest nie prawidłowy lub został juz wykorzystany!',
      9 => 'Podany token został juz wykorzystany!',
      10 => 'Pole nick musi zawierać przynajmniej 3 znaki.',
      11 => 'Kod z obrazka nie jest prawidłowy',
      12 => 'Login/Hasło nie prawidłowe',); 
      $this->messages = array(1 => 'Dziękujemy post został dodany, po akceptacji moderatora zostanie wyświetlony na forum',
      2 => 'Dodano',
      3 => 'Dziękujemy, rejestracja zakończona!<br>Odbierz emaila w celu zakończenia rejestracji',
      4 => 'Dziękujemy, twoje konto zostało aktywowane, możesz korzystać z pełnych funkcjonalności forum.<br>Zaraz nastąpi przekierowanie do strony logowania',
      5 => 'Dziękujemy temat został dodany do forum, po akceptacji moderatora zostanie wyświetlony na stronie.<br>Za chwilę nastąpi przekierowanie na forum',
      6 => 'Dane użytkownika zostały zmienione');
     }

 function change_titles($name)
 {
    $z =   array(' ',',','ó','?','!','ł','ń','ź','ż','ć','ś','ą','ę','.','Ł','%','–','„','”','"');
    $na =  array('-','','o','','','l','n','z','z','c','s','a','e','','l','','','','','');
    $name = strtolower(str_replace($z,$na,$name));
    return $name;
 }

///////////////////////////////////////////////////////////////////////PRODUKTY//////////////////////////////////////////////////////////////////	 
      function getErrorMessage($id)
      {
        $error= "<div align=\"center\"><div class=\"error_window_small\"></span>".$this->error_messages[$id]."</div></div>";                
         return $error;
      }
      function getMessage($id)
      {
        $info= "<div align=\"center\"><div class=\"message_window_small\"></span>".$this->messages[$id]."</div></div>";                
         return $info;
      }
      function pobierzNazweKategorii($id ,$table)
      {
       $query = "select id, title_pl from "._DB_PREFIX."$table where jdb_active = 'y' and id='$id' ";
       $wynik=db_getsinglerow($this->sqlconn, $query);
       return $wynik;
      }
      
       public function Check($title=null,$content=null,$nick=null) {
       global $errorMessage,$err;
       
       if(eregi(NAME_REGEXP,$nick)==false){$err["'.$nick.'"]=1;}
       
       if($title!=null){ if(eregi(LONG_NAME_REGEXP,$title)==false){$err["'.$title.'"]=1;}}
       
       if(eregi(DESCRIPTION_REGEXP,$content)==false){$err["'.$content.'"]=1;}
       
       if (isset($err["'.$nick.'"])){$errorMessage = $this->getErrorMessage(1);} 
       
       if($title!=null){ if (isset($err["'.$title.'"])){$errorMessage = $this->getErrorMessage(2);} }
       
       if (isset($err["'.$content.'"])){$errorMessage = $this->getErrorMessage(3);}
       //dumpvar($wynik);
       return $wynik;
       }
      
      function pobierzKategorie()
      {
        $query = "select id, title_pl, alias, nick, last_post_date, last_post_nick, last_post_id,view_count,content,date from "._DB_PREFIX."_forum_categories where jdb_active = 'y' order by jdb_orderkey desc "; 
        $wynik=db_getsqltable($this->sqlconn, $query);
        foreach ($wynik as $k=>$v)
         {
          $query = "select id, title_pl, alias, nick, last_post_date, last_post_nick, last_post_id,view_count,content,date from "._DB_PREFIX."_forum_subject where id_section='".$v['id']."' and jdb_active = 'y' order by jdb_orderkey asc limit 1 "; 
          $wynik[$k]['lastSubject']=db_getsinglerow($this->sqlconn, $query);
         }
        return $wynik;
      }
      
      function pobierzTematy($id=null, $start=null, $limit=null, $separated=null)
      {
        $query = "select id, title_pl, alias, nick, last_post_date, last_post_nick, last_post_id,view_count,content,date,count_posts,is_closed,is_separated from "._DB_PREFIX."_forum_subject where id_section='".$this->getCat."' and jdb_active = 'y'"; 
        $separated!=null? $query .=" and is_separated='y' " : $query .=" and is_separated='n' "; 
        $id!=null?$query.="and id='$id' order by jdb_orderkey":$query.="order by jdb_orderkey desc limit $start,$limit";
          
        //echo($query);
        $wynik=db_getsqltable($this->sqlconn, $query);
         foreach ($wynik as $k=>$v)
         {      
          $wynik[$k]['titlelink']=$this->change_titles($v['title_pl']);
          $wynik[$k]['godzina']=substr($v['last_post_date'], 10, -3);
         }
        return $wynik;
      }
      
      function aktualizujTematy($id)
      {
        $query = "select * from "._DB_PREFIX."_forum_posts where id_temat='$id' and jdb_active = 'y' order by date desc limit 1";
        $wynik=db_getsinglerow($this->sqlconn, $query);
        
           $query = "select count(id) from "._DB_PREFIX."_forum_posts where id_temat='$id' and jdb_active = 'y' order by date desc";
           $wynikAll=db_getsinglevalue($this->sqlconn, $query);
        
        $query = "Update "._DB_PREFIX."_forum_subject SET last_post_id='".$wynik['id']."', last_post_date='".$wynik['date']."', last_post_nick='".$wynik['nick']."', count_posts='".$wynikAll."'  where id='".$wynik['id_temat']."' "; 
        mysql_query($query);
           
           $query = "Select view_count from "._DB_PREFIX."_forum_subject where id='".$_GET['id_temat']."' "; 
           $ile=db_getsinglevalue($this->sqlconn, $query);
           
           $count = $ile+1;
           $query = "Update "._DB_PREFIX."_forum_subject SET view_count='$count' where id='".$_GET['id_temat']."' "; 
           mysql_query($query); 
           

 
                     
        return $wynik;
      }
      
      
      
      function pobierzPosty($id, $start, $limit)
      {
        $query = "select id,id_user, id_temat, title_pl, content, nick, date from "._DB_PREFIX."_forum_posts where id_temat='$id' and jdb_active = 'y' order by date desc limit $start,$limit "; 
        //echo($query);
        $wynik=db_getsqltable($this->sqlconn, $query);
        foreach ($wynik as $k=>$v)
         {
          $query = "select id, email, emailvisible from "._DB_PREFIX."_forum_users where id='".$v['id_user']."' ";
          //echo($query);
          $wynik[$k]['userthis']=db_getsinglerow($this->sqlconn, $query);
          
          $wynik[$k]['godzina']=substr($v['date'], 10, -3);
         }
        // dumpvar($wynik);
        return $wynik;
      }
      
      function dodajPost($content,$nick,$id_temat, $id_category,$idU) 
      {
        global $errorMessage ,$err, $shopMessage;
        $check = $this->Check($title=null,$content,$nick);
        if ($err["'.$nick.'"]){$errorMessage = $this->getErrorMessage(1); return $errorMessage; }
        elseif ($err["'.$content.'"]){$errorMessage = $this->getErrorMessage(3); return $errorMessage; }
        else
        {
          $alias = $this->change_titles($title);
          $query = "Insert into "._DB_PREFIX."_forum_posts SET content='$content', alias='$alias',nick='$nick', id_temat='$id_temat',id_category='$id_category', id_user='$idU', date=NOW(), jdb_active = 'n'"; 
          mysql_query($query);
          $shopMessage = $this->getMessage(1);
           
        }      
      }
      
      function dodajTemat($title,$content,$nick,$idCat,$idU)
      {
        global $errorMessage ,$err ,$shopMessage; 
        $check = $this->Check($title,$content,$nick);
        if ($err["'.$nick.'"]){$errorMessage = $this->getErrorMessage(1); return $errorMessage; }
        elseif ($err["'.$title.'"]){$errorMessage = $this->getErrorMessage(2); return $errorMessage; }
        elseif ($err["'.$content.'"]){$errorMessage = $this->getErrorMessage(3); return $errorMessage; }
        else
        {
          $alias = $this->change_titles($title);
          $query = "Insert into "._DB_PREFIX."_forum_subject SET title_pl='$title', content='$content', alias='$alias',nick='$nick',id_section='$idCat', date=NOW(),id_user='$idU', jdb_active = 'n'"; 
          //echo($query);
          mysql_query($query);
          $shopMessage = $this->getMessage(5);
          header("Refresh: 5; URL=http://preview.sni.pl/counturHd/forum.html");
         // header('Location: http://preview.sni.pl/counturHd/temat-dodany,forum.html'); 
        }      
      }
      
      function policzTematy()
      {
        $query = "select count(id) from "._DB_PREFIX."_forum_subject where id_section='".$this->getCat."' and jdb_active = 'y'"; 
        $wynik=db_getsinglevalue($this->sqlconn, $query);
        //dumpvar($wynik);
        return $wynik;
      }
      
      public function prawdzDostepnosc($nick,$email,$idU=null) {
       global $errorMessage, $shopMessage;
       
       $sql = "Select nick from "._DB_PREFIX."_forum_users where nick='$nick'";
       $idU!=''?$sql.=" and id!='$idU' ":$sql.="";
       $wynik=db_getsqltable($this->sqlconn, $sql);       
       if ($wynik){$errorMessage = $this->getErrorMessage(4);}
       else{
       $sql = "Select email from "._DB_PREFIX."_forum_users where email='$email'";
       $idU!=''?$sql.=" and id!='$idU' ":$sql.="";
       $wynik=db_getsqltable($this->sqlconn, $sql);      
       if ($wynik){$errorMessage = $this->getErrorMessage(5);}
       }
       //dumpvar($wynik);
       return $wynik;
      }
      
      
      function zalogujUzytkownika()
      {
        global $shopMessage, $errorMessage;
        $query = "select * from "._DB_PREFIX."_forum_users where nick='".$this->postNick."' and password='".md5($this->postPassword)."' and jdb_active = 'y' and banned='n' "; 
        //echo($query);
        $wynik=db_getsinglerow($this->sqlconn, $query);
        //dumpvar($wynik);
        if(!$wynik){$errorMessage = $this->getErrorMessage(12);} 
        return $wynik;
      }
      function pobierzOstatniegoUzytkownika($id=null)
      {
        $query = "select * from "._DB_PREFIX."_forum_users "; 
        $id!=''?$query.=" where id='$id' ":$query.="";
        $query.=" order by id desc limit 1";
       // echo($query);
        $wynik=db_getsinglerow($this->sqlconn, $query);
        //dumpvar($wynik);
        return $wynik;
      }
      
      function sprawdzToken($id,$token)
      {
        global $shopMessage, $errorMessage;
        $query = "select id,token from "._DB_PREFIX."_forum_users where id='".$id."' and token='".$token."' "; 
        //echo($query);
        $user = db_getsinglerow($this->sqlconn, $query);
        if ($user)
        {
        $query = "select id,token from "._DB_PREFIX."_forum_users where id='".$user['id']."' and token='".$token."' and token_a_date='' "; 
        $usert = db_getsinglerow($this->sqlconn, $query);
        if ($usert)
        {
          $query = "UPDATE "._DB_PREFIX."_forum_users SET jdb_active='y',token_a_date='".$this->dateNow."' where id='".$id."' and token='".$token."' "; 
          //echo($query);
          $wynik = mysql_query($query);
          $shopMessage = $this->getMessage(4);
          header("Refresh: 5; URL=http://preview.sni.pl/counturHd/logowanie,forum.html");
        }
        else
        {
         $errorMessage = $this->getErrorMessage(9);
         header("Refresh: 5; URL=http://preview.sni.pl/counturHd/forum.html");
        }
        }
        else
        {
         $errorMessage = $this->getErrorMessage(8);
         header("Refresh: 5; URL=http://preview.sni.pl/counturHd/forum.html");
        }
        return $wynik;
      }
      
 
      function rejestracjaNowegoKlienta($token)
      {
       global $shopMessage, $errorMessage;
        $check = $this->prawdzDostepnosc($this->postNick,$this->postEmail);
        $kod = md5(md5($_POST['obrazek']));
        if (!$check)
        {
        if(eregi(EMAIL_REGEXP,$this->postEmail)==false)
        {$errorMessage = $this->getErrorMessage(6);}
        elseif(eregi(USER_PASSWORD_REGEXP,$this->postPassword)==false)
        {$errorMessage = $this->getErrorMessage(7);}        
        elseif(eregi(NAME_REGEXP,$this->postNick)==false)
        {$errorMessage = $this->getErrorMessage(10);}        
        elseif ($kod!=$_SESSION['sprawdz'])
        {$errorMessage = $this->getErrorMessage(11);}
        else
        {
        $query = "INSERT INTO "._DB_PREFIX."_forum_users SET 
         nick='".$this->postNick."' , 
         email='".$this->postEmail."' , 
         password='".md5($this->postPassword)."' , 
         camera='".$this->postCamera."' ,
         emailvisible='".$this->postEmailvisible."' , 
         acceptemail='".$this->postAcceptemail."' , 
         date='".$this->dateNow."' , 
         signature='".$this->postSignature."' , 
         token='$token' , 
         jdb_active='n' "; 
         //echo($query);
         $wynik=mysql_query($query);
 
          if($wynik){$shopMessage = $this->getMessage(3);}        
          return $wynik;
         }
         
          }
            else
          {
            return $errorMessage;
          }
      }
     function zmianDaneKlienta()
      {
       global $shopMessage, $errorMessage;
        $check = $this->prawdzDostepnosc($this->postNick,$this->postEmail,$this->Uid);
        $kod = md5(md5($_POST['obrazek']));
        if (!$check)
        {
        //Jezeli jest nowe hasło
        if ($this->postNewPassword!=''){
        if(eregi(USER_PASSWORD_REGEXP,$this->postNewPassword)==false)
        {$errorMessage = $this->getErrorMessage(7);}  
        }
        if(eregi(EMAIL_REGEXP,$this->postEmail)==false)
        {$errorMessage = $this->getErrorMessage(6);}      
        elseif(eregi(NAME_REGEXP,$this->postNick)==false)
        {$errorMessage = $this->getErrorMessage(10);}        
        elseif ($kod!=$_SESSION['sprawdz'])
        {$errorMessage = $this->getErrorMessage(11);}
        else
        { 
        $query = "Update "._DB_PREFIX."_forum_users SET 
         nick='".$this->postNick."' , 
         email='".$this->postEmail."' ";
         if ($this->postNewPassword!='')
         {
          $query.=" ,password='".md5($this->postNewPassword)."' ";
         }
         $query.="
         ,camera='".$this->postCamera."' ,
         emailvisible='".$this->postEmailvisible."' , 
         acceptemail='".$this->postAcceptemail."' , 
         signature='".$this->postSignature."' where id='".$this->Uid."' "; 
         //echo($query);
         $wynik=mysql_query($query);
 
          if($wynik){$shopMessage = $this->getMessage(6);}        
          return $wynik;
         }
         
          }
            else
          {
            return $errorMessage;
          }
      } 
      
 
    function ustawpoloczenie($sqlconn)
    {     
     $this->sqlconn = $sqlconn;
    }
    
    function ustawjezyk($lng)
    {     
     $this->lng = $lng;
    }
	}
?>
